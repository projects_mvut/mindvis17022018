@extends('layouts.pages')

@section('content')

<h1>Edit the section</h1>

{!!Form::model($section,
	[  	'url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug ,
	   	'method'=>'patch'])!!}
@include('partials.forms.section')

@stop