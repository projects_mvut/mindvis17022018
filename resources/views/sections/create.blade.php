@extends('layouts.pages')

@section('content')

<h1>Add a new Section</h1>

{!!Form::open(['url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section'])!!}
@include('partials.forms.section')

@stop