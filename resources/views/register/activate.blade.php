@extends ('layouts.pages')

@section('content')

<p>Your Email is yet to be verified.</p>
<p>Kindly login to your email account and verify your email to login.</p>

<a href="#" class="btn btn-primary">Resend Activation Email</a>

@stop