@extends('layouts.section')

@section('sectioncontent')

<div class="quiz-show">
<h1>{{$quiz->title}}</h1>
<p>{{$quiz->preview}}</p>

@if($question)
<a href="/quiz/{{$quiz->slug}}/start" class="btn btn-primary"><i class="fa fa-play"></i> Start Quiz</a>
@endif
		
</div>
@if(Auth::check())
@if(Auth::user()->is_teacher or Auth::user()->is_admin)
<div class="row">
	<div class="col col-md-3 quiz-sidebar">
		@if($quiz->drafted)
		<span class="label label-warning">Drafted</span>
		@else
		<span class="label label-success">Live</span>
		@endif
		<hr>
		<a href="/courses/{{$course->slug}}/quizzes/{{$quiz->slug}}/questions/create" class="btn btn-primary">Add a Question</a>
		<a href="#" data-toggle="modal" data-target="#QuizcsvModal" class="btn btn-primary">Upload Quiz Csv</a>
		<a href="/downloadcsv/{{$quiz->slug}}" class="btn btn-primary">Download Quiz Csv</a>
		<a href="/quiz/{{$quiz->slug}}/draft" class="btn btn-warning">Draft</a>
		<a href="/quiz/{{$quiz->slug}}/live" class="btn btn-success">Live</a>
		<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/{{$quiz->slug}}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit the Quiz</a>
		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$quiz->slug}}"><i class="fa fa-trash"></i> Quiz</button>	    
	</div>
	<div class="col col-md-9">
		<table class="table table-bordered">
			<tr><th>Question Number</th>
				<th>Question Title</th>
				<th>Action</th>
			</tr>
			@foreach($questions as $question)
			<tr>
				<td>{{$question->question_number}}</td>
				<td>{{$question->title}}</td>
				<td><a class="btn btn-primary" href="/courses/{{$course->slug}}/quizzes/{{$quiz->slug}}/questions/{{$question->question_number}}">
				 <i class="fa fa-eye"></i> </a>
				 <a class="btn btn-danger" href="/courses/{{$course->slug}}/quizzes/{{$quiz->slug}}/questions/{{$question->question_number}}/delete"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
<div id="delete-{{$quiz->slug}}" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-body">
    <h4>Confirm Delete for Quiz {{$quiz->title}}</h4>
    <a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/{{$quiz->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  </div>
</div>
</div>
</div>

@include('partials.forms.quizcsv')

@endif
@endif

@stop