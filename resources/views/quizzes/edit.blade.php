@extends('layouts.pages')

@section('content')

<h1>Edit the Quiz</h1>

{!!Form::model($quiz,
	[  	'url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/quiz/'.$quiz->slug ,
	   	'method'=>'patch'])!!}
@include('partials.forms.quiz')

@stop
