@extends ('layouts.pages')

@section('content')

<h1>Add a new Quiz</h1>

{!!Form::open(['url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/quiz'])!!}
@include('partials.forms.quiz')

@stop
