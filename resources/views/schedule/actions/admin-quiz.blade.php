@if(Auth::user()->is_admin)
<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/sub/3" class="btn btn-danger">
		<i class="fa fa-minus"> 3</i>
	</a>
	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/sub/1" class="btn btn-danger">
		<i class="fa fa-minus"> 1</i>
	</a>

	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/today" class="btn btn-default">
		Today
	</a>

	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/add/1" class="btn btn-primary">
		<i class="fa fa-plus"> 1</i>
	</a>
	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/add/3" class="btn btn-primary">
		<i class="fa fa-plus"> 3</i>
	</a>
	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/add/5" class="btn btn-primary">
		<i class="fa fa-plus"> 5</i>
	</a>
	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/add/10" class="btn btn-primary">
		<i class="fa fa-plus"> 10</i>
	</a>
	@if($lipo)
	<a href="/dashboard/schedule/user/{{$user->username}}/quiz/{{$quiz->slug}}/unassign" class="btn btn-warning">Unassign</a>
	@endif
@endif