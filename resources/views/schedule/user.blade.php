@extends ('layouts.pages')

@section('content')
<h1>{{$course->title}} | Schedule</h1>
<a href="/updateSchedule/{{$course->slug}}/{{$user->username}}" class="btn btn-success">Update Schedule Request</a>

	
@foreach($course->subjects as $subject)
	<h2>{{$subject->title}}</h2>

	@foreach($subject->sections as $section)
	<div class="row">
	<div class="col col-md-4">
		<h4>{{$section->title}}</h4>
	</div>
	<div class="col col-md-8">
		<table class="table">
			<thead>
				<tr>
					<th>Lecture / Quiz</th>
					<th>Completed</th>
					<th>Deadline</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($section->lectures as $lecture)
				<tr>
					<td><a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$lecture->slug}}"><i class="fa fa-play-circle-o"></i> {{$lecture->title}}</a></td>
					<?php $lipo=$user->lectures->find($lecture->id); ?>

					@if($lipo)
					<?php 
						$deadline= \Carbon\Carbon::createFromTimeStamp(strtotime($lipo->pivot->deadline))
							->format('jS  F');
						$completed=$lipo->pivot->completed;
						$class="warning";
						if($completed=='Yes'){
							$class="success";
						}
					?>
					<td class="{{$class}}">{{$completed}}</td>
					<td>{{$deadline}}</td>
					@else
					<td>No</td>
					<td>Not Assigned</td>
					@endif
					<td>
						@include('schedule.actions.user-lecture')
					</td>
				</tr>
				@endforeach
				@foreach($section->quizzes as $quiz)
				<tr>
					<td><a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/{{$quiz->slug}}"><i class="fa fa-question-circle"></i> {{$quiz->title}}</a></td>
					<?php $lipo=$user->quizzes->find($quiz->id); ?>

					@if($lipo)
					<?php 
						$deadline= \Carbon\Carbon::createFromTimeStamp(strtotime($lipo->pivot->deadline))
							->format('jS  F');
						$completed=$lipo->pivot->completed;
						$class="warning";
						if($completed=='Yes'){
							$class="success";
						}
					?>
					<td class="{{$class}}">{{$completed}}</td>
					<td>{{$deadline}}</td>
					@else
					<td>No</td>
					<td>Not Assigned</td>
					@endif
					<td>
						@include('schedule.actions.user-quiz')
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	</div>
@endforeach

@endforeach

@stop