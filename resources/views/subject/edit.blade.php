@extends('layouts.pages')

@section('content')

<h1>Edit the Subject</h1>

{!!Form::model($subject,
	[  	'url'=>'courses/'.$course->slug.'/subject/'.$subject->slug ,
	   	'method'=>'patch'])!!}
@include('partials.forms.subject')

@stop