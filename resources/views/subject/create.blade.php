@extends('layouts.pages')

@section('content')

<h1>Add a new Subject</h1>

{!!Form::open(['url'=>'courses/'.$course->slug.'/subject'])!!}
@include('partials.forms.subject')

@stop