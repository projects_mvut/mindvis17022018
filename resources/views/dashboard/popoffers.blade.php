@extends('layouts.dashboard')
@section('dashcon')
<form method="post" action="{{url('/dashboard/popoffers')}}">
  <div class="form-group">
        {{csrf_field()}}

    <label for="txt">Text</label>
    <input type="text" class="form-control" name="poptext" id="txt">

  </div>
   <div class="form-group">
    <label for="txt">Link To</label>
    <input type="text" placeholder="https://" class="form-control" name="calltoaction" id="txt">
  </div>
  <div class="form-group">
    <label for="bgc">Background Color</label>
 <select name="popbgcolor">
<option value="#0375B4" style="background-color:#0375B4; color:#fff;">Starry Night</option>
<option value="#007849" style="background-color:#007849; color:#fff;">IRISES</option>
<option value="#813772" style="background-color:#813772; color:#fff;">POSY</option>
<option value="#B82601" style="background-color:#B82601; color:#fff;">EMBERS</option>
<option value="#565656" style="background-color:#565656; color:#fff;">BLACKBOARD</option>
<option value="#49274A" style="background-color:#49274A; color:#fff;">EGGPLANT</option>
<option value="#1A2930" style="background-color:#1A2930; color:#fff;">DENIM</option>
<option value="#01ABAA" style="background-color:#01ABAA; color:#fff;">AZURE</option>
</select>

   </div>
  <div class="form-group">
    <label for="tc">Text Color</label>
   <select name="poptextcolor">
<option value="#fff">White</option>
<option value="#000">Black</option>
</select>
  </div>





 
<select name="courseid">

@foreach($courses as $course)
  <option value="{{$course['id']}}">{{$course['title']}}</option>
@endforeach   
</select>




                      <button type="submit" class="btn btn-default">Submit</button>
</form>


<hr>

<div class="table-responsive">
<table  class="table-striped table-bordered">
<tr>
<td>OFFER</td>
<td>LINK TO</td>
<td>STYLE</td>
<td>VISIBLE IN</td>
<td>CREATED ON</td>
<td>ACTION</td>
</tr>

@foreach($pops as $pop)
<tr>
<td>{{$pop['poptext']}}</td>
<td><a href="{{$pop['calltoaction']}}" target="_blank">click here</a></td>
<td style="background-color:{{$pop['popbgcolor']}}; color:{{$pop['poptextcolor']}}">{{$pop['poptext']}}</td>
<td>{{$pop['courseid']}}</td>
<td>{{$pop['created_at']}}</td>
<td>
<button onclick="location.href='{{action('PopController@edit', $pop['id'])}}'" type="button"><i class="fa fa-pencil
-square-o" aria-hidden="true"></i></button>
<form action="{{action('PopController@destroy', $pop['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form></td>
</tr>
@endforeach   
</table>
</div>
@stop
