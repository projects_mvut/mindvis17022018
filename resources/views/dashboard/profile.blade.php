@extends('layouts.dashboard')

@section('dashcon')

<div class="row">
	<div class="col col-md-5">
		<div class="profile">
			@if($user->url)
			<img src="{{$user->url}}"  class="profileImage">
			@else
			<img src="/images/default.png"  class="profileImage">
			@endif
		<p>First Name : <b>{{$user->first_name}}</b></p>
		<p>Last Name : <b>{{$user->last_name}}</b></p>
		<p>Userame : <b>{{$user->username}}</b></p>
		<p>Email : <b>{{$user->email}}</b></p>
		<p>Mobile : <b>{{$user->mobile}}</b></p>
		<p>Registered : <b>{{$user->created_at->diffForHumans()}}</b></p>
		<p>Last Profile Update : <b>{{$user->updated_at->diffForHumans()}}</b></p>
		<div><p>Address:</p>
			<p>{{$user->address1}}</p>
			<p>{{$user->address2}}</p>
			<p>{{$user->city}} - {{$user->pin}} , {{$user->state}}</p>
		</div>
		<a href="#" class="btn btn-primary" id="editProfile">Edit Profile</a>
		<a href="#" class="btn btn-primary" id="chpass">Change Password</a>
	@if(Auth::user()->is_admin)
			@include('partials.forms.schedule')
	@endif
		</div>
	</div>
	<div class="col col-md-5">
		<div id="editProfileForm">
			<h1>Edit Profile</h1>
			@include('partials.forms.profile')
			<a href="#" class="btn btn-warning pull-right canceledit">Cancel</a>
		</div>
		<div id="chpassForm">
			<h1>Change Password</h1>
			@include('partials.forms.changePassword')
			<a href="#" class="btn btn-warning pull-right canceledit">Cancel</a>
		</div>
	</div>
</div>
@include('partials.errors')
@stop