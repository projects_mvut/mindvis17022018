@extends('layouts.dashboard')

@section('dashcon')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css">
  <a href="/dashboard/users/date/{{$prev->toDateString()}}" class="btn btn-primary">Previous Day</a>
  <a href="/dashboard/users/date/{{$next->toDateString()}}" class="btn btn-primary">Next Day</a>
<h3>Select Date: </h3>
<input type="text" id="datepicker" name="date" class="form-control datepicker">
<button class="btn btn-primary date_submit">Submit</button>
<hr>
@if(!$users->isEmpty())
<h2>Users registered on {{$today->toFormattedDateString()}}</h2>
<table class="table table-striped">
	<tr>
		<th>Profile Image</th>
		<th>Name</th>
		<th><p>Username</p><p>Email</p><p>Mobile</p></th>
		<th>Course Interested</th>
		<th>Action</th>
	</tr>
	@foreach($users as $user)
	<tr>
		<td><a href="/user/{{$user->username}}"><img src="{{$user->url}}" width="100px" height="100px"></a></td>
		<td><a href="/user/{{$user->username}}">{{$user->first_name}} {{$user->last_name}}</td></a>
		<td><p>{{$user->username}}</p><p>{{$user->email}}</p><p>{{$user->mobile}}</p></td>
		<td>
			{{$user->course_interested}}
			{!!Form::open(['url'=>'dashboard/users/mail/'.$user->username])!!}
			<div class="form-group">
				{!! Form::label('Mail Template') !!}
				{!! Form::select('email_id',$emails,null , ['class'=>'form-control']) !!}
			</div>
			{!! Form::submit('Mail',['class'=>'btn btn-danger','name'=>"submit"]) !!}
			{!! Form::submit('Test',['class'=>'btn btn-success','name'=>"submit"]) !!}
			{!! Form::close() !!}
		</td>
		<td>
			<a href="/dashboard/users/{{$user->username}}/courses" class="btn btn-primary btn-dashuser">Courses</a><br>
			<a href="/dashboard/author/{{$user->username}}" class="btn btn-default btn-dashuser"><i class="fa fa-thumbs-up"></i> Assign</a><br>
			<button type="button" class="btn btn-danger btn-dashuser" data-toggle="modal" data-target="#delete-{{$user->username}}"><i class="fa fa-trash"></i> Delete</button>
		</td>
	</tr>
	<div id="delete-{{$user->username}}" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-body">
	        <h4>Confirm Delete for User {{$user->first_name}} {{$user->last_name}}</h4>
	        <a href="/dashboard/delete/{{$user->username}}" class="btn btn-danger"><i class="fa fa-trash"></i> Confrim Delete</a>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
	@endforeach
	</table>


@else
<h5>No User Registered on {{$today->toFormattedDateString()}}</h5>
@endif


<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script>
  $(function() {
    $( "#datepicker" ).datepicker({
    	dateFormat:"yy-m-d"
    });
    $('.date_submit').click(function(){
	   window.location.href='/dashboard/users/date/'+$('#datepicker').val();
	})
  });
  </script>
@stop