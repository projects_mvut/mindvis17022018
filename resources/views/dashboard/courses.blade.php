@extends('layouts.dashboard')

@section('dashcon')

<h1 class="other-page-heading">Courses</h1><a href="/courses/create" class="btn btn-primary">Create a new Course.</a>
<p></p>
@if(!$courses->isEmpty())
<table class="table table-hover table-fit table-bordered">
		<tr>
			<th>Title</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	@foreach($courses as $course)
	
		<tr>
			<td><a href="/courses/{{$course->slug}}">{{$course->title}}</a></td>
			@if($course->is_published)
			<td align="center"><i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color:#8bc34a"></i>
</td>
			@else
			<td align="center"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true" style="color:#ffc107"></i>
</td>
			@endif
			<td>
 <div class="btn-toolbar">
        <div class="btn-group">
          <button class="dropdown dropdown-toggle btn reg-btn btn-block" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>
 <b class="caret"></b></button>
          <ul class="dropdown-menu">
          			@if(Auth::user()->is_admin)

            <li><a href="/dashboard/courses/{{$course->slug}}/subscribed">Subscribed</i></a></li>
            <li><a href="/dashboard/courses/{{$course->slug}}/unsubscribed">Unsubscribed</a></li>
            <li><a href="/dashboard/courses/{{$course->slug}}/teachers">
					<span class="count-teacher pull-right">{{$course->users()->teacher()->count()}}</span> Teachers</a></li>
									@endif

            <li><a href="/dashboard/courses/{{$course->slug}}/users">
					<span class="count-user pull-right">{{$course->users()->student()->count()}}</span> Users</a></li>
            <li class="divider"></li>
            <li><a href="/courses/{{$course->slug}}/edit">Edit</a></li>
            @if(Auth::user()->is_admin)
                        <li><a href="#" data-toggle="modal" data-target="#delete-{{$course->slug}}"> Delete</a></li>
                        				<div id="delete-{{$course->slug}}" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-body">
				        <h4>Confirm Delete for Course {{$course->title}}</h4>
				        <a href="/dashboard/courses/{{$course->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				      </div>
				    </div>
				  </div>
				</div>
				@endif

          </ul>
        </div>
     </div> 

<!-- 
			@if(Auth::user()->is_admin)
				<a href="/dashboard/courses/{{$course->slug}}/subscribed" class="btn btn-primary"><i class="fa fa-check"></i></a>
				<a href="/dashboard/courses/{{$course->slug}}/unsubscribed" class="btn btn-info"> <i class="fa fa-times"></i></a>
				<a href="/dashboard/courses/{{$course->slug}}/teachers" class="btn btn-default">
					<span class="count-teacher">{{$course->users()->teacher()->count()}}</span> <i class="fa fa-user"></i></a>
				@endif
				<a href="/dashboard/courses/{{$course->slug}}/users" class="btn btn-primary">
					<span class="count-user">{{$course->users()->student()->count()}}</span> <i class="fa fa-users"></i></a>
				<a href="/courses/{{$course->slug}}/edit" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></a>
				@if(Auth::user()->is_admin)
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$course->slug}}"><i class="fa fa-trash"></i></button>
				<div id="delete-{{$course->slug}}" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-body">
				        <h4>Confirm Delete for Course {{$course->title}}</h4>
				        <a href="/dashboard/courses/{{$course->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				      </div>
				    </div>
				  </div>
				</div>
				@endif -->
			</td>
		</tr>
	@endforeach
	</table>

@else
<h5>No Course Registered.</h5>
@endif
@stop