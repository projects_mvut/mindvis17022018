@extends('layouts.dashboard')

@section('dashcon')
<h1>{{$user->first_name}} {{$user->last_name}}</h1>
<div class="row">
<div class="col col-md-6">
<h2>Courses subscribed</h2>

@if(!$courses->isEmpty())
<table class="table">
@foreach($courses as $course)
<tr>
	<td><a href="/courses/{{$course->slug}}">{{$course->title}}</a></td>
	<td>
		<a href="/dashboard/schedule/user/{{$user->username}}/course/{{$course->slug}}" class="btn btn-default">Manage Schedule</a>
		<a href="/dashboard/assignments/user/{{$user->username}}/course/{{$course->slug}}" class="btn btn-warning">Assignments</a>
		<a href="/dashboard/unassign/{{$user->username}}/{{$course->slug}}" class="btn btn-danger"><i class="fa fa-user-times"></i> Unsubscribe</a>
	</td>
</tr>
@endforeach
</table>
@else
<p>No courses subscribed.</p>
@endif
</div>
<div class="col col-md-6">
	<h2>Unsubscribed Courses</h2>
	@if(!$courses_un->isEmpty())
<table class="table">
@foreach($courses_un as $course)
<tr>
	<td><a href="/courses/{{$course->slug}}">{{$course->title}}</a></td>
	<td><a href="/dashboard/assign/{{$user->username}}/{{$course->slug}}" class="btn btn-primary">Subscribe
	</a></td>
</tr>
@endforeach
</table>
@else
<p>All courses subscribed.</p>
@endif
</div>


</div>
@stop