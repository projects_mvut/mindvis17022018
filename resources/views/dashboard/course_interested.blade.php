@extends('layouts.dashboard')

@section('dashcon')

@if(Auth::check())

{!!Form::model(Auth::user(),['url'=>'dashboard/course_interested',])!!}
<div class="form-group">
<select name="course_interested" class="form-control">
  <option value=""><b>Select a Course</b></option>
	@foreach($courses as $course)
  <option value="{{$course}}">{{$course}}</option>
  @endforeach
</select>
</div>
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>

{!!Form::close()!!}
@endif


@include('partials.errors')
@stop