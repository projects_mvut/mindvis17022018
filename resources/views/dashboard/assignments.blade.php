@extends('layouts.dashboard')

@section('dashcon')

<h3>Assignments for Course 	<a href="/courses/{{$course->slug}}">{{$course->title}}</a>
</h3><h3> {{$user->first_name}} {{$user->last_name}}</h3>

@if(!$assignments->isEmpty())

<ol>
	@foreach($assignments as $assignment)
	<li>
		<a href="{{$assignment->slug}}" target="_blank">{{$assignment->title}}</a>
		<h5>Submitted: 	<a href="{{$assignment->link($user->id)}}" target="_blank">{{$assignment->link($user->id)}}</a></h5>
	</li>
	@endforeach
</ol>


@else
<h5>No Assignments Available.</h5>
@endif
@stop