@extends('layouts.dashboard')

@section('dashcon')

<h1 class="other-page-heading">Teachers</h1>

@if(!$teachers->isEmpty())
<table class="table table-hover table-fit table-bordered">
		<tr>
			<th>Profile Image</th>
			<th>Name</th>
			<th>Username <br>Email <br>Mobile</th>
			<th>Action</th>
		</tr>
	@foreach($teachers as $teacher)
	
		<tr>
			<td><a href="/user/{{$teacher->slug}}"><img src="{{$teacher->url}}"  style="max-width:55px; max-height:55px; object-fit:cover;"></a></td>
			<td><a href="/user/{{$teacher->slug}}">{{$teacher->first_name}} {{$teacher->last_name}}</a></td>
			<td>{{$teacher->username}} <br>
			{{$teacher->email}} <br>
			{{$teacher->mobile}}</td>
			<td>


				<div class="btn-toolbar">
				       <div class="btn-group">
				         <button class="dropdown dropdown-toggle btn reg-btn btn-block" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>
				<b class="caret"></b></button>
				         <ul class="dropdown-menu">
				     <li><a href="/dashboard/teachers/{{$teacher->slug}}/courses">Courses</a></li>
				     <li><a href="/dashboard/suspend/{{$teacher->slug}}">Suspend</a></li>
				     <li><a href="#" data-toggle="modal" data-target="#delete-{{$teacher->username}}">Delete</a></li>
				         </ul>
				       </div>
				    </div> 
				    			</td>
		</tr>
		<div id="delete-{{$teacher->username}}" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-body">
		        <h4>Confirm Delete for Teacher {{$teacher->first_name}} {{$teacher->last_name}}</h4>
		        <a href="/dashboard/delete/{{$teacher->slug}}" class="btn btn-danger"><i class="fa fa-trash"></i> Confrim Delete</a>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>
	@endforeach
	</table>

@else
<h5>No Teacher Registered.</h5>
@endif
@stop