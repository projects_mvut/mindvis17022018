@extends('layouts.dashboard')

@section('dashcon')


<div class="row"> 
	<div class="col col-md-4 col-md-offset-4">
	<h1>Add User</h1>
		{!!Form::open(['url'=>'dashboard/user'])!!}

		<div class="form-group">
		{!! Form::text('first_name',null,['placeholder'=>'First Name','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		{!! Form::text('last_name',null,['placeholder'=>'Last Name','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		{!! Form::text('username',null,['placeholder'=>'Username','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		{!! Form::password('password',['placeholder'=>'Password','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		        <button type="submit" class="btn btn-primary form-control">Create User</button>
		    </div>
		</form>
	</div>
</div>
@include('partials.errors')
@stop