@extends('layouts.dashboard')

@section('dashcon')

<h1 class="other-page-heading">Users</h1>

@if(!$users->isEmpty())
<div class="table-responsive">
<table class="table userlist table-hover table-fit table-bordered">
	<thead>
		<tr>
			<th>Name</th>
			<th>Username</th>

			<th>Email</th>
			<th>Mobile</th>
			<th>University</th>



			<th>Course Interested</th>
			<th>Action</th>
		</tr>
	</thead>
	@foreach($users as $user)
	<tbody>
		<tr>
		


			<td><a href="/user/{{$user->username}}">{{$user->first_name}} {{$user->last_name}}</a></td>
			<td>{{$user->username}}</td>
			<td>{{$user->email}}</td>
			<td>{{$user->mobile}}</td>
			<td>{{$user->address2}}</td>

			<td>{{$user->course_interested}}</td>
			<td>



				 <div class="btn-toolbar">
				        <div class="btn-group">
				          <button class="dropdown dropdown-toggle btn reg-btn btn-block" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>
				 <b class="caret"></b></button>
				          <ul class="dropdown-menu">
				      <li><a href="/dashboard/users/{{$user->username}}/courses">Courses</a></li>
				      <li><a href="/dashboard/author/{{$user->username}}">Assign</a></li>
				      <li><a href="#" data-toggle="modal" data-target="#delete-{{$user->username}}">Delete</a></li>
				          </ul>
				        </div>
				     </div> 











<!-- 
				<a href="/dashboard/users/{{$user->username}}/courses" class="btn btn-primary btn-dashuser">Courses</a><br>
				<a href="/dashboard/author/{{$user->username}}" class="btn btn-default btn-dashuser"><i class="fa fa-thumbs-up"></i> Assign</a><br>
				<button type="button" class="btn btn-danger btn-dashuser" data-toggle="modal" data-target="#delete-{{$user->username}}"><i class="fa fa-trash"></i> Delete</button> -->
			</td>
		</tr>
	</tbody>
		<div id="delete-{{$user->username}}" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-body">
		        <h4>Confirm Delete for User {{$user->first_name}} {{$user->last_name}}</h4>
		        <a href="/dashboard/delete/{{$user->username}}" class="btn btn-danger"><i class="fa fa-trash"></i> Confrim Delete</a>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>
	@endforeach
	</table>
</div>
@else
<h5>No User Registered.</h5>
@endif
@stop
