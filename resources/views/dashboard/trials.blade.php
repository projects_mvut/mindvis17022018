@extends('layouts.dashboard')

@section('footinclude')
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
$(function() {
	$( "#datepicker" ).datepicker({
		dateFormat:"yy-m-d"
	});
	$('.date_submit').click(function(){
		window.location.href='/dashboard/trials/date/'+$('#datepicker').val();
	})
});
</script>
@stop

@section('dashcon')
<!-- <a href="/dashboard/trials/download" class="btn btn-primary">Download Csv</a> -->

@if(isset($next))
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css">
<a href="/dashboard/trials/date/{{$prev->toDateString()}}" class="btn btn-primary">Previous Day</a>
<a href="/dashboard/trials/date/{{$next->toDateString()}}" class="btn btn-primary">Next Day</a>
<h3>Select Date: </h3>
<input type="text" id="datepicker" name="date" class="form-control datepicker">
<button class="btn btn-primary date_submit">Submit</button>
<hr>
@endif
<!-- <div class="row">	
	<div class="col col-sm-4">Contact</div>
	<div class="col col-sm-4">Course</div>
	<div class="col col-sm-4">Created</div>
</div> -->


<div class="row">
<div class="table-responsive">
<table class="table table-fit table-bordered" >
	<thead>
	<tr>
		<th>Contact</th>
		<th>Course</th>
		<th>Created</th>
	</tr>	
</thead>

	@foreach($trials as $e)
	<div class="
	@if($e->trackers()->count()==$mailCount)
	successBar
	@endif
	">
	<tbody id="trials-table">

<tr>
<td style="white-space:normal">
	{{$e->name}} <br>
	{{$e->email}} <br>
	{{$e->phone}} <br>
</td>
<td style="white-space:normal">
	{{$e->subject}}
</td>
<td style="white-space:normal">
	{{$e->created_at->diffForHumans()}}
</td>
</tr>	
<tr>
<td colspan="2" style="white-space:normal">

	{!!Form::open(['url'=>'dashboard/trials/'.$e->id,'method'=>'put'])!!}
	<div class="input-group">

	{!! Form::text('detail',$e->detail,['placeholder'=>'Detail','class'=>'form-control',"rows"=>"3"]) !!}
	<span class="input-group-addon">-</span>

	{!! Form::text('action',$e->action,['placeholder'=>'Action','class'=>'form-control',"rows"=>"3"]) !!}
</div>
<br>
<div class=" text-center">

	<button type="submit" class="btn btn-info">Update</button>
	<a href="/dashboard/trials/{{$e->id}}/delete" class="btn btn-danger">Delete</a>
</div>
	{!! Form::close() !!}
</td>
<td style="white-space:normal">
	{!!Form::open(['url'=>'dashboard/trials/mail/'.$e->id])!!}
	<div class="form-group">
<!-- 		{!! Form::label('Mail Template') !!}
		 -->		{!! Form::select('email_id',$emails,null , ['class'=>'form-control']) !!}
	</div>
	{!! Form::submit('Mail',['class'=>'btn btn-danger','name'=>"submit"]) !!}
	{!! Form::submit('Test',['class'=>'btn btn-success','name'=>"submit"]) !!}
	{!! Form::submit('Force',['class'=>'btn btn-info','name'=>"submit"]) !!}
	{!! Form::close() !!}
</td>
</tr>
</tbody>
</div>
@endforeach

</table>
</div>
</div>



@include('partials.errors')
@stop