@extends('layouts.dashboard')

@section('dashcon')
<a href="/dashboard/newsletter/download" class="btn btn-primary">Download Csv</a>
<table class="table table-hover table-fit table-bordered">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Course</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>
	@foreach($newsletters as $e)
		<tr>
			<td>{{$e->name}}</td>
			<td>{{$e->email}}</td>
			<td>{{$e->course}}</td>
			<td>{{$e->created_at->diffForHumans()}}</td>
		</tr>
	@endforeach
	</tbody>
</table>


@include('partials.errors')
@stop