@extends('layouts.dashboard')
@section('pageTitle', 'GATE & IBPS PO Preparation Tips and Articles')
@section('dashcon')

<h1>Articles</h1>

<a href="/articles/create" class="btn btn-primary">Create a new Article.</a>
<p></p>
@if(!$blogs->isEmpty())
<table class="table table-striped userlist table-hover table-fit table-bordered">
	<thead>
		<tr>
			<th>Title</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($blogs as $blog)
	
		<tr>
			<td><a href="/articles/{{$blog->slug}}">{{$blog->title}}</a></td>
			@if($blog->is_published)
						<td align="center"><i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color:#8bc34a"></i>
			</td>
						@else			
									<td align="center"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true" style="color:#ffc107"></i>
						</td>
									@endif
			<td>
			

				<div class="btn-toolbar">
				       <div class="btn-group">
				         <button class="dropdown dropdown-toggle btn reg-btn btn-block" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>
				<b class="caret"></b></button>
				         <ul class="dropdown-menu">
				     <li><a href="/articles/{{$blog->slug}}/edit" >Edit</a></li>
				     <li><a href="#" data-toggle="modal" data-target="#delete-{{$blog->slug}}">Delete</a></li>
				     <div id="delete-{{$blog->slug}}" class="modal fade" role="dialog">
				       <div class="modal-dialog">
				         <div class="modal-content">
				           <div class="modal-body">
				             <h4>Confirm Delete for Article {{$blog->title}}</h4>
				             <a href="/dashboard/articles/{{$blog->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
				             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				           </div>
				         </div>
				       </div>
				     </div>
				         </ul>
				       </div>
				    </div> 


			
				
			







			</td>
		</tr>
	@endforeach
</tbody>
	</table>

@else
<h5>No Article Added.</h5>
@endif
@stop
