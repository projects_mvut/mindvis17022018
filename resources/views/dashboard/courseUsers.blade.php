@extends('layouts.dashboard')

@section('dashcon')

<h1>Course {{$course->title}}</h1>
@if(Auth::user()->is_admin)
<div class="row">
	<div class="col col-md-6">
		@endif
		<h2>Users Subscribed</h2>
		@if(!$users->isEmpty())
		<table class="table">
			@foreach($users as $user)
			@if($user->pivot->is_active)
			<tr class="success">
				@else
				<tr class="danger">
					@endif
					<td>{{$user->first_name}}</td>
					<td>{{$user->last_name}}</td>
				</tr>
				<tr>
					<td>
						<a href="/dashboard/schedule/user/{{$user->username}}/course/{{$course->slug}}" class="btn btn-primary">Schedule</a>
						<a href="/dashboard/assignments/user/{{$user->username}}/course/{{$course->slug}}" class="btn btn-warning">Assignments</a>
						<a href="/quiz-report/user/{{$user->username}}" class="btn btn-success">Quiz Report</a>
						@if(Auth::user()->is_admin)
						<a href="/dashboard/unassign/{{$user->username}}/{{$course->slug}}" class="btn btn-danger"><i class="fa fa-user-times"></i> Unsubscribe</a>
						@endif
						<td>
							<a href="/dashboard/userActivate/{{$user->username}}/{{$course->slug}}"><i class="fa fa-check"></i></a>
							<a href="/dashboard/userDeactivate/{{$user->username}}/{{$course->slug}}"><i class="fa fa-times"></i></a>
						</td>
					</td>
				</tr>
				@endforeach
			</table>
			@else<p>No user subscribed.</p>
			@endif

		</div>
		@if(Auth::user()->is_admin)
		<div class="col col-md-6">
			<h2>Users Unsubscribed</h2>
			@if(!$users_un->isEmpty())
			<table class="table">
				@foreach($users_un as $user)
				<tr>
					<td>{{$user->first_name}}</td>
					<td>{{$user->last_name}}</td>
					<td>
						<a href="/dashboard/assign/{{$user->username}}/{{$course->slug}}" class="btn btn-primary">Subscribe</a>
					</td>
				</tr>
				@endforeach
			</table>
			@else<p>All Users Subscribed.</p>
			@endif
		</div>
	</div>
	@endif

	@stop