@extends('layouts.dashboard')

@section('dashcon')

<h1 class="other-page-heading">Admins</h1>

@if(!$admins->isEmpty())
<table class="table table-hover table-hover table-fit table-bordered">
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Username</th>
			<th>Email</th>
		</tr>
	@foreach($admins as $admin)
	
		<tr>
			<td>{{$admin->first_name}}</td>
			<td>{{$admin->last_name}}</td>
			<td>{{$admin->username}}</td>
			<td>{{$admin->email}}</td>
		</tr>
	@endforeach
	</table>

@else
<h5>No Admin Registered.</h5>
@endif
<a href="/dashboard/userList" class="btn btn-info">Download User List</a>
<!-- <a href="/dashboard/downloaddb" class="btn btn-warning">Download DB</a> -->
@stop