@extends('layouts.dashboard')

@section('dashcon')


<div class="row"> 
	<div class="col col-md-8 col-md-offset-2">
<h1>Edit Home Seo Settings</h1>
		{!!Form::model($home,['url'=>'dashboard/home_seo'])!!}

		<div class="form-group">
		{!! Form::text('keywords',null,['placeholder'=>'Keywords Here','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		{!! Form::text('description',null,['placeholder'=>'Description','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
		        <button type="submit" class="btn btn-primary">Update</button>
		    </div>
		</form>
	</div>
</div>
@include('partials.errors')
@stop