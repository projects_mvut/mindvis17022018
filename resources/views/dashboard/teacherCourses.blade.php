@extends('layouts.dashboard')

@section('dashcon')
<h1>{{$teacher->first_name}} {{$teacher->last_name}}</h1>
<div class="row">
<div class="col col-md-6">
<h2>Courses assigned</h2>
@if(!$courses->isEmpty())
<table class="table">
@foreach($courses as $course)
<tr>
	<td><a href="/courses/{{$course->slug}}">{{$course->title}}</a></td>
	<td><a href="/dashboard/unassign/{{$teacher->slug}}/{{$course->slug}}" class="btn btn-warning"><i class="fa fa-user-times"></i> Unassign
	</a></td>
</tr>
@endforeach
</table>
@else
<p>No course assigned.</p>
@endif
</div>
<div class="col col-md-6">
	<h2>Unassigned Courses</h2>
	@if(!$courses_un->isEmpty())
<table class="table">
@foreach($courses_un as $course)
<tr>
	<td><a href="/courses/{{$course->slug}}">{{$course->title}}</a></td>
	<td><a href="/dashboard/assign/{{$teacher->slug}}/{{$course->slug}}" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Assign
	</a></td>
</tr>
@endforeach
</table>
@else
<p>All courses assigned.</p>
@endif
</div>


</div>
@stop