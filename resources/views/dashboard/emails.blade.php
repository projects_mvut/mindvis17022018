@extends('layouts.dashboard')

@section('dashcon')
<h1>Emails</h1>

<a href="/dashboard/emails/create" class="btn btn-primary">Create</a>

@foreach($emails as $email)
<div class="email">
	<h3>{{$email->subject}}</h3>
	<div>
		{{$email->content}}
	</div>
</div>
@endforeach


@include('partials.errors')
@stop