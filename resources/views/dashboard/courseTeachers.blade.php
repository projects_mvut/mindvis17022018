@extends('layouts.dashboard')

@section('dashcon')

<h1>Course {{$course->title}}</h1>
<div class="row">
<div class="col col-md-6">
<h2>Teachers assigned</h2>
@if(!$teachers->isEmpty())
<table class="table">
@foreach($teachers as $teacher)
<tr>
	<td>{{$teacher->first_name}}</td>
	<td>{{$teacher->last_name}}</td>
	<td>
		<a href="/dashboard/unassign/{{$teacher->slug}}/{{$course->slug}}" class="btn btn-warning"><i class="fa fa-user-times"></i> Unassign</a>
	</td>
</tr>
@endforeach
</table>
@else<p>No teacher assigned.</p>
@endif
</div>

<div class="col col-md-6">
<h2>Teachers Unassigned</h2>
@if(!$teachers_un->isEmpty())
<table class="table">
@foreach($teachers_un as $teacher)
<tr>
	<td>{{$teacher->first_name}}</td>
	<td>{{$teacher->last_name}}</td>
	<td>
		<a href="/dashboard/assign/{{$teacher->slug}}/{{$course->slug}}" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Assign</a>
	</td>
</tr>
@endforeach
</table>
@else<p>All teachers assigned.</p>
@endif
</div>
</div>

@stop