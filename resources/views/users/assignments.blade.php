@extends('layouts.dashboard')

@section('dashcon')

<h3>Assignments for Course 	<a href="/courses/{{$course->slug}}">{{$course->title}}</a></h3>

@if(!$assignments->isEmpty())

<ol>
	@foreach($assignments as $assignment)
	<li>
		<a href="{{$assignment->link}}" target="_blank">{{$assignment->title}}</a>
		<h5>Submitted: 	<a href="{{$assignment->link($uid)}}" target="_blank">{{$assignment->link($uid)}}</a></h5>
		{!!Form::open(['url'=>'assignments/'.$course->slug.'/submit/'.$assignment->id])!!}
		<div class="form-group">
			<input type="text" name="link" placeholder="Your Assignment Link" class="form-control">
		</div>
		<div class="form-group">
			{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
	</li>
	@endforeach
</ol>


@else
<h5>No Assignments Available.</h5>
@endif
@stop