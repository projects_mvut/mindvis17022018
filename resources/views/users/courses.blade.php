@extends('layouts.dashboard')

@section('dashcon')

<h1>Courses Subscribed</h1>

@if(!$courses->isEmpty())

@foreach($courses as $course)
<h2><a href="/courses/{{$course->slug}}">{{$course->title}}</a></h2>
<a href="/schedule/{{$course->slug}}" class="btn btn-success">Schedule</a>
<a href="/assignments/{{$course->slug}}" class="btn btn-warning">Assignments</a>
@endforeach
<a href="/quiz-report" class="btn btn-info">Quiz Report</a>


@else
<h5>No Course Subscribed.</h5>
@endif
@stop