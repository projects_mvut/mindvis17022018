@extends ('layouts.pages')

@section('content')

<h1>Add a new Email Template</h1>

{!!Form::open(['url'=>'dashboard/emails'])!!}

@include('partials.forms.mailer')

@stop