@extends ('layouts.pages')

@section('content')

<h1>Edit the Email</h1>

{!!Form::model($email,['url'=>'/dashboard/emails/'.$email->id,'method'=>'patch'])!!}

@include('partials.forms.mailer')

@stop