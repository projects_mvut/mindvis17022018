@extends ('layouts.pages')

@section('content')
<section class="main white-bg">
<div class="container">
<div class="row">
<div class="art-show">
	<div class="art-author">
			@if(Auth::user())
			@if(Auth::user()->is_admin)
			<a href="/gks/{{$gk->slug}}/edit" class="btn btn-primary">Edit</a>
			
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$gk->slug}}"><i class="fa fa-trash"></i> Delete</button>
			<div id="delete-{{$gk->slug}}" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body">
			        <h4>Confirm Delete for News {{$gk->title}}</h4>
			        <a href="/gks/{{$gk->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			      </div>
			    </div>
			  </div>
			</div>
			@endif
			@endif
		</div>	
	<div class="art-content">
		<h4>{{$gk->title}}</h4>
		<p>Created : {{$gk->created_at->diffForHumans()}}</p>
		<p>{{$gk->preview}}</p>
		<div class="all-content">{!! $gk->content !!}</div>
	</div>
</div>
</div>
</div>
</section>
@stop