@extends ('layouts.pages')
@section('pageTitle', 'News')
@section('content')	
<section class="main white-bg">
<div class="container">
<div class="row">
		
	@foreach($recg as $gk)
<div class="row arti">
	<div class="col col-md-4 col-md-offset-1">
		<a href="/gks/{{$gk->slug}}"><img src="{{$gk->url}}" width="300px" height="150px"><p></p></a>
	</div>
	<div class="col col-md-6">
		<a href="/gks/{{$gk->slug}}"><h4>{{$gk->title}}</h4></a>
		<p>Created : {{$gk->created_at->diffForHumans()}}</p>
		<p>{!! $gk->preview !!}</p>
	</div>
</div>
	@endforeach	

<div class="art-butt">
		@if(Auth::user())
		@if(Auth::user()->is_admin)
			<a href="/gks/create" class="btn btn-primary">Add News</a>
		@endif
		@endif
		</div>

		
	</div>
	</div>
	
	</section>
@stop
