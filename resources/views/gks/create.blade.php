@extends ('layouts.pages')

@section('content')

<h1>Add News</h1>

{!!Form::open(['url'=>'gks','files' => true])!!}

@include('partials.forms.gks')

@stop