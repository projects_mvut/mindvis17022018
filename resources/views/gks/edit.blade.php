@extends ('layouts.pages')

@section('content')

<h1>Edit the News</h1>

{!!Form::model($gk,['url'=>'gks/'.$gk->slug,'method'=>'patch','files' => true])!!}

@include('partials.forms.gks')

@stop