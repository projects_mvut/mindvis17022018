@extends ('layouts.pages')

@section('content')
<section class="main other white-bg">
<div class="container">
<div class="row">
<h1 class="other-page-heading">Edit the Course</h1>

{!!Form::model($course,['url'=>'courses/'.$course->slug,'method'=>'patch','files' => true])!!}

@include('partials.forms.course')
</div>
</div>
</section>
@stop