<div class="reviews">
	<h3>Reviews</h3>
	<div class="row course-review">
	@foreach($reviews as $review)
	<?php $us=\App\User::where('id',$review->user_id)->first() ?>
	<div class="col col-md-4">
		<div>
			<img src="{{$us->url}}" class="img-rounded" width="50px" height="50px">
			<p class="revname">{{$us->first_name}} {{$us->last_name}}</p>
		</div>
		<div>
		<p class="raty" data-score="{{$review->rating}}"></p>
		<div class="review-content">
			{{$review->content}}
			@if(Auth::check())
			@if((Auth::user()->is_admin) or ((Auth::user()->id) == ($us->id)))
			<a href="/courses/{{$course->slug}}/reviews/{{$review->id}}/delete" class="btn btn-danger reviewdel"><i class="fa fa-trash-o"></i></a>
			@endif
			@endif
		</div>
		</div>
	</div>
	@endforeach
</div>
</div>

<div class="revform">
	{!!Form::open(['url'=>'courses/'.$course->slug.'/review'])!!}
	@include('partials.forms.review')
</div>