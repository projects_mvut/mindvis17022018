<h3>Reviews</h3>

<div id="reviewCarousel" class="carousel slide reviewCarousel" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		@foreach($reviews as $review)
		<?php 
		$us=\App\User::where('id',$review->user_id)->first();
		$freview=\App\Review::where('course_id',$course->id)->first();
		?>
		<div class="item @if($review->id ==$freview->id) active @endif">
			<div class="review-cx">
				<div>
					<img src="{{$us->url}}" class="img-rounded" width="50px" height="50px">
					<p class="revname">{{$us->first_name}} {{$us->last_name}}</p>
				</div>
				<div>
					<p class="raty" data-score="{{$review->rating}}"></p>
					<div class="review-content">
						{{$review->content}}
						@if(Auth::check())
						@if((Auth::user()->is_admin) or ((Auth::user()->id) == ($us->id)))
						<a href="/courses/{{$course->slug}}/reviews/{{$review->id}}/delete" class="btn btn-danger reviewdel"><i class="fa fa-trash-o"></i></a>
						@endif
						@endif
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>


	<a class="left carol" href="#reviewCarousel" role="button" data-slide="prev">
		<span class="sr-only">Previous</span>
		<i class="fa fa-chevron-circle-left carol-left"></i>
	</a>
	<a class="right carol" href="#reviewCarousel" role="button" data-slide="next">
		<span class="sr-only">Next</span>
		<i class="fa fa-chevron-circle-right carol-right"></i>
	</a>
</div>

<div class="revform">
	{!!Form::open(['url'=>'courses/'.$course->slug.'/review'])!!}
	@include('partials.forms.review')
</div>