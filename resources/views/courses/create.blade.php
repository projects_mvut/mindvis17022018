@extends ('layouts.pages')

@section('content')
<section class="main other white-bg">
<div class="container">
<div class="row">
<h1 class="other-page-heading">Add a new Course</h1>

{!!Form::open(['url'=>'courses','files' => true])!!}

@include('partials.forms.course')
</div>
</div>
</section>
@stop

