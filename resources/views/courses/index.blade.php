@extends ('layouts.course-index')
@section('pageTitle', 'Online Courses for GATE Mechanical Engineering')
@section('content')
@include('partials.newsletter')
	@if(!$courses->isEmpty())
	<div class="row">
		<ul id="da-thumbs" class="da-thumbs">
			@foreach($courses as $course)

				<li class="col courses-index">

					<a href="/courses/{{$course->slug}}">
						<img src="{{$course->url}}" alt="{{$course->title}}" />
							<div class="course-text">
								<span>
									{{$course->title}}
								</span>
							</div>
					</a>
	 			</li>
			@endforeach
		</ul>
	</div>
	@else
	<p>No courses available.</p>
	@endif
@stop
