@extends('layouts.main')
@section('pagecontent')





<?php 
$instructor=$course->users()->where('is_teacher',true)->first();
$rating=\App\Review::where('course_id',$course->id)->avg('rating');
if(!$rating){
	$rating=3.5;
}
?>

@include('partials.elements.nav')







@if($show_banner)
<img src="{{$course->banner}}" class="banner">
@if($show_trial)
@include('partials.elements.requestTrial')
@endif
@endif
<section class="main white-bg">
<div class="container">

	@include('partials.flash')
	<div class="row" style="margin-top: 30px;">
		<div class="col col-md-4"><img src="{{$course->url}}" width="320px" height="170px"></div>
		<div class="col col-md-8">
			<div class="course-title">{{$course->title}}</div>
			<p>About Course : {{$course->preview}}</p>
			<div class="row sinfo">
				<div class="col col-sm-4">
					Rating: <span class="raty" data-score="{{$rating}}"></span>
				</div>
				<div class="col col-sm-4">
					<i class="fa fa-users"></i>
					{{$course->base_users+$course->users()->where('is_teacher',false)->get()->count()}}  Learners
				</div>
				<div class="col col-sm-4">
					<div class="course-social">
						<a href="https://www.facebook.com/mindvis" style="color:#43609C"><i class="fa fa-facebook-square"></i></a>
						<a href="https://twitter.com/mindvisindia" style="color:#28A9E0"><i class="fa fa-twitter-square"></i></a>
						<a href="mailto:info@mindvis.in"><i class="fa fa-envelope"></i></a>
						<a href="https://www.youtube.com/channel/UCcZ9l294MgHN3d4P3_f45Jw" style="color:#E62117"><i class="fa fa-youtube-play"></i></a>
					</div>
				</div>
			</div>        
			<a href="/courses/{{$course->slug}}/curriculum" class="btn btn-primary btn-curriculum">Curriculum</a>
			@if(Auth::check())
			<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#assignments-{{$course->slug}}">Assignments</button>
			@include('partials.elements.assignments')
			@else
			<a href="/auth/login" class="btn btn-success">Login to Start Learning</a>
			@endif
			
			@if(Auth::check() and Auth::user()->is_teacher)
			@if(Auth::user()->is_admin)
			<a href="/courses/{{$course->slug}}/downloadCurriculum" class="btn btn-info">Download Curriculum</a>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#uploadCurriculum-{{$course->slug}}">Upload Curriculum</button>
			@include('partials.uploadCurriculum')
			@endif
			<a href="/courses/{{$course->slug}}/emailstudents" class="btn btn-default" data-toggle="modal" data-target=".emailstudents">Email Students</a>
			<a href="/courses/{{$course->slug}}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$course->slug}}"><i class="fa fa-trash"></i></button>
			<div id="delete-{{$course->slug}}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h4>Confirm Delete for Course {{$course->title}}</h4>
							<a href="/courses/{{$course->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>
			@include('partials.elements.emailModal')
			@endif
		</div>
	</div>

	<div class="row vid-in">
		<div class="col col-sm-6">
			<iframe width="100%" height="300px" src="{{$course->demo_video}}" frameborder="0" allowfullscreen></iframe>
		</div>
		
		<div class="col col-sm-3">
			@if($course->price)
			@if($course->price_self)
			<div class="course-details">
				<span class="btn btn-curriculum" id="course-type-show-assisted">Assisted</span>
				<span class="btn btn-primary" id="course-type-show-self">Self</span>
			</div>
			@endif
			@endif
			<ul class="list-group course-details" id="course-type-assisted">
				<li class="list-group-item">No. of Videos : <b>{{$course->no_of_video}}</b> </li>
				<li class="list-group-item">No. of Sectional Test : <b>{{$course->no_of_quiz}}</b> </li>
				<li class="list-group-item">Notes Provided : <b>{{$course->notes}}</b> </li>
				<li class="list-group-item">No. of Live Sessions : <b>{{$course->no_of_live_session}}</b> </li>
				<li class="list-group-item">No. of Mock Tests : <b>{{$course->no_of_mocks}}</b> </li>
				@if($course->price)
				<li class="list-group-item">DVDs available : <b>{{$course->dvds}}</b> </li>
				@endif
				<li class="list-group-item">Mentor Support : <b>Yes</b></li>
				<li class="list-group-item">Price : <b>@if($course->price) <i class="fa fa-inr"></i> {{$course->price}}
					@else   Free     @endif</b>
				</li>
				@if(Auth::check() and !(Auth::user()->courses->contains($course->id)))
				@if($course->instamojo_button)
				<li class="list-group-item">
					<a href="https://www.instamojo.com/mindvis/{{ $course->instamojo_button }}?data_name={{Auth::user()->first_name}}+{{Auth::user()->last_name}}&data_email={{Auth::user()->email}}" rel="im-checkout" data-behavior="remote" data-style="light" data-text="Subscribe" data-token="b382915691d8922110e34a2cb2f97aa8"></a><script src="https://d2xwmjc4uy2hr5.cloudfront.net/im-embed/im-embed.min.js"></script>
				</li>
				@endif
				@endif
			</ul>
			@if($course->price_self)
			<ul class="list-group course-details" id="course-type-self">
				<li class="list-group-item">No. of Videos : <b>{{$course->no_of_video}}</b> </li>
				<li class="list-group-item">No. of Sectional Test : <b>{{$course->no_of_quiz}}</b> </li>
				<li class="list-group-item">Notes Provided : <b>{{$course->notes}}</b> </li>
				<li class="list-group-item">Live Sessions : <b>Not Provided</b> </li>
				<li class="list-group-item">No. of Mock Tests : <b>{{$course->no_of_mocks}}</b> </li>
				<li class="list-group-item">DVDs available : <b>{{$course->dvds}}</b> </li>
				<li class="list-group-item">Mentor Support : <b>No</b></li>
				<li class="list-group-item">Price : 
					<b><i class="fa fa-inr"></i> {{$course->price_self}}</b>
				</li>
				@if(Auth::check() and !(Auth::user()->courses->contains($course->id)))
				@if($course->instamojo_self)
				<li class="list-group-item">
					<a href="https://www.instamojo.com/mindvis/{{ $course->instamojo_self }}?data_name={{Auth::user()->first_name}}+{{Auth::user()->last_name}}&data_email={{Auth::user()->email}}" rel="im-checkout" data-behavior="remote" data-style="light" data-text="Subscribe" data-token="b382915691d8922110e34a2cb2f97aa8"></a><script src="https://d2xwmjc4uy2hr5.cloudfront.net/im-embed/im-embed.min.js"></script>
				</li>
				@endif
				@endif
			</ul>
			@endif
		</div>
		<div class="col col-md-3 insco">
			<div class="course-instructor">
				@if($instructor)
				<h4>Instructor</h4>
				<img class="img-circle" src="{{$instructor->url}}" alt="insim" height="80px" width="80px">
				<h5><i>{{$instructor->first_name}} {{$instructor->last_name}}</i></h5>
				@endif
			</div>
		</div>
	</div>

	<div class="course-main">
		<div class="all-content">
			{!! $course->content !!}
		</div>
	</div>


@foreach($pops as $pop)
@if($pop['courseid'] == $course->id)
  <div id="my" class="modal fade" role="dialog">
@else

  <div id="my1" class="modal fade" role="dialog">
				@endif
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content"  style="background:url('http://www.batuira.com/wp-content/uploads/2016/03/abstract-background-blue-25pct-640x300.png') {{$pop['popbgcolor']}};color:{{$pop['poptextcolor']}}; background-size:contain;">
<div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                <h4 class="modal-title text-center" style="color:#fff; font-weight:bolder;letter-spacing:1px; word-spacing:1px;">Here is the Special Offer for You!</h4>
            </div>
      <div class="modal-body text-center">
        <p style="font-size:16px;  line-height:32px;">{{$pop['poptext']}}			
</p>
      </div>
    <div class="modal-footer text-center">
    <div class="text-center">
                    <a href="{{$pop['calltoaction']}}" class="btn btn-default" target="_blank" style="display:block; color:{{$pop['popbgcolor']}}; margin:0;">Click here to avail the offer</a>
                    
                     <i><p style="color:#fff; font-weight:light;">and get started!</p></i>
    </div>
                </div>
    </div>

  </div>
</div>

    @endforeach   


	@if($course->price)
	@else
	@include('partials.elements.ad')
	@endif

	@include('courses.reviewcarousel')    
	@include('partials.elements.searchmodal')
</div>
 



</div>
</div>
</section>		

@include('partials.newsletter')

@include('partials.elements.footer')


@stop
