@extends ('forums.base')

<section class="main other white-bg">
<div class="container">
<div class="row">
@section('forum')

<h2>{{$forum->title}}</h2>
<p>{{$forum->content}}</p>

<p>Posted By : <b>{{$user->first_name}} {{$user->last_name}}</b></p>
<hr>

<h4>Answers:</h4>

@if(!$answers->isEmpty())
@foreach($answers as $answer)
<p id="autlink"><i class="fa fa-comments"></i> {{$answer->content}}</p>
<?php $user= \App\User::where('id',$answer->user_id)->first(); ?>
<p>Posted By : <b>{{$user->first_name}} {{$user->last_name}}</b></p>
<hr class="answers">
@endforeach
@else
<p>No answer posted.</p>
@endif

@if(Auth::check())

{!!Form::open()!!}
<div class="form-group">
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Add an answer','class'=>'form-control', 'rows'=>'2']) !!}
</div>
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
</div>
{!! Form::close() !!}

@if(Auth::user()->id == $user->id)

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$forum->slug}}"><i class="fa fa-trash"></i> Delete</button>
<div id="delete-{{$forum->slug}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Confirm Delete for Forum {{$forum->title}}</h4>
        <a href="/forums/{{$forum->slug}}/delete" class="btn btn-danger">Delete this forum</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</section>
@endif
@endif
@stop

