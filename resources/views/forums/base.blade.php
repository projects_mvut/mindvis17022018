@extends ('layouts.pages')

@section('content')

<div class="row">
	<div class="col col-md-4">
		<h1>Forums</h1>
		@if(!$forums->isEmpty())
		@foreach($forums as $forum)
		<a href="/forums/{{$forum->slug}}"><i class="fa fa-commenting-o"></i> {{$forum->title}}</a>
		<p>{{$forum->content}}</p>
		<p>Posted By : <b>{{\App\User::where('id',$forum->user_id)->first()->first_name}} {{\App\User::where('id',$forum->user_id)->first()->last_name}}</b></p>
		@endforeach
		@else
		<p>No Forum posted.</p>
		@endif
	</div>

	<div class="col col-md-8">
		@yield('forum')		
	</div>
</div>
@stop

