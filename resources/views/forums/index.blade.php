@extends ('forums.base')
<section class="main other white-bg">
<div class="container">
<div class="row">
@section('pageTitle', 'Forums')
@section('forum')
@include('partials.forms.forum')
</div>
</div>
</section>
@stop

