<div class="reviews">
	<h3>Comments</h3>
	@foreach($lecture->lcomments as $lcomment)
	<div class="review row">
		<div class="rimg col col-md-3">
			<img src="{{$lcomment->user->url}}" width="50px" height="50px">
			<p class="revname">{{$lcomment->user->first_name}} {{$lcomment->user->last_name}}</p>
		</div>
		<div class="col col-md-9">
		@if(Auth::check())
		@if((Auth::user()->is_admin) or ((Auth::user()->id) == ($lcomment->user->id)))
		<a href="/comment/{{$lcomment->id}}/delete" class="btn btn-danger pull-right"><i class="fa fa-trash-o"></i></a>
		@endif
		@endif
		<p>{{$lcomment->content}}</p>
		</div>
	</div>
	@endforeach
</div>

<div class="revform">
	{!!Form::open(['url'=>'lectures/'.$lecture->slug.'/comment'])!!}
	<div class="form-group">
	{!! Form::textarea('content',null,['placeholder'=>'Enter Your Comment','class'=>'form-control reviewcon']) !!}
	</div>

	@if(!$course->price)
	@include('partials.elements.ad')
	@endif

	@if(Auth::check())
	<div class="form-group">
		{!! Form::submit('Submit',['class'=>'btn btn-primary form-control','name'=>"submit"]) !!}
	</div>
	@else
	<a href="/auth/login" class="btn btn-success">Login To Comment</a>
	@endif
	{!!Form::close()!!}
	@include('partials.errors')
</div>