@extends ('layouts.pages')

@section('content')

<h1>Add a new Lecture</h1>

{!!Form::open(['url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/lecture','files' => true])!!}

@include('partials.forms.lecture')

@stop