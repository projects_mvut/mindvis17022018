@extends ('layouts.pages')

@section('content')

<h1>Edit the Lecture</h1>

{!!Form::model($lecture,['url'=>'courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/lecture/'.$lecture->slug,'method'=>'patch','files' => true])!!}

@include('partials.forms.lecture')

@stop