@extends ('layouts.pages')

@section('content')
<ul>
@foreach($lectures as $lecture)
@if($lecture->url)
<img src="{{$lecture->url}}" width="300px" height="150px"><p></p>
@endif
<li><h4><a href="/lectures/{{$lecture->slug}}">{{$lecture->title}}</a></h4></li>
@endforeach
</ul>

<a href="/lectures/create" class="btn btn-primary">Create a new Lecture.</a>

@stop