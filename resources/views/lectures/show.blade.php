@extends('layouts.section')

@section('sectioncontent')

		@if($lecture->url)
		<img src="{{$lecture->url}}" width="300px" height="150px"><p></p>
		@endif
	<h1>{{$lecture->title}}</h1>
	@if(Auth::user())
		@if(Auth::user()->is_teacher)
			<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$lecture->slug}}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$lecture->slug}}"><i class="fa fa-trash"></i></button>
		    <div id="delete-{{$lecture->slug}}" class="modal fade" role="dialog">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-body">
		            <h4>Confirm Delete for Lecture {{$lecture->title}}</h4>
		            <a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$lecture->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		          </div>
		        </div>
		      </div>
		    </div>
		@endif
	@endif
	<div class="all-content">
		{!! $lecture->content !!}
	</div>

	<div class="pene">
	@if($previousLecture)
	<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$previousLecture->slug}}" class="">Previous Lecture</a>
	@endif

	@if($nextLecture)
	<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$nextLecture->slug}}" class="pull-right">Next Lecture</a>
	@else
	<a href="/courses" class="pull-right">Finished</a>
	@endif
	</div>
	@include('lectures.comment')


@stop
