<!DOCTYPE html>
  <html lang="en">
  <head>
  	<meta charset="UTF-8">
    @if(isset($keywords))
    <meta name="keywords" content="{{$keywords}}" />
    @endif
    @if(isset($description))
<meta name="description" content="{{$description}}"/>
    @endif
    <link href="/logo.png" rel="icon" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('pageTitle') @if(isset($seo_title)){{$seo_title}} @endif </title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.3.0/katex.min.css">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    @yield('headinclude')

<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5656875881505c8622d9b4c9/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
  </head>
  <body style="padding-top:63px;background-color:#fbfbfb;">
    <div class="se-pre-con"></div>

      @yield('pagecontent')
<script src="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.3.0/katex.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<script src="{{ elixir('js/all.js') }}"></script>
<script type="text/javascript">

 $(window).load(function(){ 

   setTimeout(function() {
    $('#my').modal('show');
    }, 5000);
    }); 
    </script>
    <script type="text/javascript">
$(document).ready(function(){
    function alignModal(){
        var modalDialog = $(this).find("#my .modal-dialog");
        
        // Applying the top margin on modal dialog to align it vertically center
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $("#my.modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $("#my.modal:visible").each(alignModal);
    });   
});
</script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-61219916-1', 'auto');ga('send', 'pageview');</script>
    @yield('footinclude')

<script>
$("#autlink").each(function(){
   $(this).html( $(this).html().replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, '<a href="$1">$1</a> ') );
    });
</script>
<script type="text/javascript">

// change nav on scroll
$(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() >= 50) {
            $( "nav" ).addClass( "white-nav",{duration:500} );
        } else {
            $( "nav" ).removeClass( "white-nav",{duration:500} );
        }
    });
});

</script>
<script type="text/javascript">

//paste this code under the head tag or in a separate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
  });
  </script>
  </body>
</html>
