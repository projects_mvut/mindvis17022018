@extends('layouts.main')
@section('pagecontent')
<div id="wrap">
      @include('partials.elements.nav') <!-- Navigation -->
      @if($course->banner)
    		<img src="{{$course->banner}}" class="banner">
      @endif
      <div class="container" style="margin-bottom:40px;">
        @include('partials.flash')
        <div class="row"> 
          <div class="col col-md-4 course-sidebar">
            @yield('sidebar')
          </div>
        <div class="col col-md-8 course-main">
          <h1>{{$course->title}}</h1>   
          <div class="all-content">
            {!! $course->content !!}
          </div>
          @include('courses.review')    
        </div>
      </div>
        @include('partials.elements.searchmodal')
      </div>
      </div>
   @include('partials.elements.footer')

@stop
      