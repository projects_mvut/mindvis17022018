@extends('layouts.main')
@section('pagecontent')
<div id="wrap">
@include('partials.elements.nav') <!-- Navigation -->
<div class="container">
	@include('partials.flash')
	<div class="row">
		<div class="col col-md-4 coursebase-sidebar">
			<h1><a href="/courses">Courses</a></h1>
			<h5><a href="/courses/popular"><i class="fa fa-fire"></i> Popular</a></h5>
			@if(!$categories->isEmpty())
			<h3>Categories <i class="fa fa-object-ungroup"></i></h3>
				@foreach($categories as $category)
					<a href="/categories/{{$category->slug}}"><h5><i class="fa fa-th-list"></i> {{$category->title}}</h5></a>
				@endforeach
			@else
				<p>No Category Available.</p>
			@endif
			@if(Auth::user())
			@if(Auth::user()->is_teacher)
			<div class="form-group coursebase-button">
				<a href="/courses/create" class="btn btn-primary form-control"><i class="fa fa-plus"></i> Course</a>
				<a href="/categories/create" class="btn btn-primary form-control"><i class="fa fa-plus"></i> Category</a>
				@yield('catbutt')
			</div>
			@endif
			@endif
		</div>
		<div class="col col-md-8 coursebase-main">
			@yield('content')
		</div>
	</div>
	@include('partials.elements.searchmodal') <!-- Search Modal -->
</div>
</div>
 @include('partials.elements.footer')
@stop
