@extends('layouts.pages')

@section('content')
<section class="main other white-bg">
<div class="container">
<div class="rows">
	<div class="col col-md-3" style="padding-top:110px">
		<img src="{{Auth::user()->url}}" alt="" class="img-circle" width="200px" height="200px">
		<a class="btn btn-primary upic" href="#" data-toggle="modal" data-target="#PicModal">Upload Profile Pic</a>
		@if(Auth::user()->is_admin)
		<ul class="list-group">
			<li class="list-group-item list-group-item-info">
				<span class="badge"><i class="fa fa-user-secret"></i> {{\App\User::where('is_admin',true)->count()}}
				</span>
				<a href="/dashboard/admins">Admins</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<span class="badge"><i class="fa fa-university"></i> {{\App\User::teacher()->count()}}
				</span>
				<a href="/dashboard/teachers">Teachers</a>
			</li>		
			<li class="list-group-item list-group-item-info">
				<span class="badge"><i class="fa fa-users"></i> {{\App\User::student()->count()}}
				</span>
				<a href="/dashboard/users">Users</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/users/date/{{Carbon\Carbon::today()->toDateString()}}">Today's Users</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<span class="badge"><i class="fa fa-bell"></i> {{\App\Trial::latest()->count()}}
				</span>
				<a href="/dashboard/trials">Trials</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/trials/date/{{Carbon\Carbon::today()->toDateString()}}">Today's Trials</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<span class="badge"><i class="fa fa-envelope"></i> {{\App\Email::latest()->count()}}
				</span>
				<a href="/dashboard/emails">Emails</a>
			</li>


			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/user/create"><i class="fa fa-plus"></i> Add a User</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/home_seo">Home Seo</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/quiz-report">Quiz Report</a>
			</li>

			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/newsletter">Newsletter</a>
			</li>
			
		</ul>
		@endif
		<ul class="list-group">
			<li class="list-group-item list-group-item-info">
				<a href="/user/{{Auth::user()->slug}}">Profile</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/courses">Courses</a>
			</li>
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/mobile">Mobile</a>
			</li>
			@if(Auth::user()->is_admin or Auth::user()->is_teacher)
			<li class="list-group-item list-group-item-info">
				<a href="/dashboard/blogs">Blogs</a>
			</li>
			<li class="list-group-item list-group-item-info"><a href="/dashboard/images">Images</a>
			</li>
			<li class="list-group-item list-group-item-info"><a href="/dashboard/popoffers">Pop Offers</a>
			</li>
			@endif
		</ul>
	</div>
	@include('partials.elements.picmodal')
	<div class="col col-md-9">
		@yield('dashcon')
	</div>

</div>
</div>
</section>

@stop