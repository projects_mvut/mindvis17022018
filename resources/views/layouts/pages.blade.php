@extends('layouts.main')

@section('pagecontent')
<div id="fold">
      @include('partials.elements.nav')
     
        @include('partials.flash')
        @yield('content')
        @include('partials.elements.searchmodal')
      </div>
      </div>
      </section>
</div>
        @include('partials.elements.footer')
@stop