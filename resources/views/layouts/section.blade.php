@extends('layouts.pages')

@section('content')
<?php 
$subscriber=0;
if(Auth::check()){
	if(Auth::user()->courses->contains($course->id)){
		$subscriber=1;
	}
	if(Auth::user()->is_teacher or Auth::user()->is_admin){
		$subscriber=1;
	}
}
?>
<div class="rows">
	<div class="col col-md-4">
		<h1><a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}"><i class="fa fa-puzzle-piece"></i> {{$section->title}} </a></h1>
		<div class="secprev">{{$section->preview}}</div>
		<hr>
		@foreach($lectures as $electure)
		<?php 
		if($electure->is_public){
			$class="section-list-a";
		}
		if(!$electure->is_public){
			if($subscriber){
				$class="section-list-a";
			}
			else{
				$class="section-list-b";
			}
		}
		if($path==$electure->slug){
			$class="section-list-active";
		}
		?>
		<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/{{$electure->slug}}" class="section-list {{$class}}"><i class="fa fa-caret-square-o-right"></i> {{$electure->title}}</a>
		@endforeach

		@foreach($quizzes as $equiz)
		<?php 
		if($equiz->is_public){
			$class="section-list-a";
		}
		if(!$equiz->is_public){
			if($subscriber){
				$class="section-list-a";
			}
			else{
				$class="section-list-b";
			}
		}
		if($path==$equiz->slug){
			$class="section-list-active";
		}
		?>
		@if($equiz->drafted)
		@if(Auth::user()->is_teacher)
		<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/{{$equiz->slug}}" class="section-list {{$class}}"><i class="fa fa-question-circle"></i> {{$equiz->title}} - Under Progress</a>
		@else
		<span>{{$equiz->title}} - Under Progress</span>
		@endif
		@else
		<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/{{$equiz->slug}}" class="section-list {{$class}}"><i class="fa fa-question-circle"></i> {{$equiz->title}}</a>
		@endif
		@endforeach
		<hr>
		<a class="btn btn-default btn-sidebar" href="/courses/{{$course->slug}}/curriculum"><i class="fa fa-chevron-left"></i> Course</a>
		@if(Auth::check() and Auth::user()->is_teacher)
		<a class="btn btn-primary btn-sidebar" href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/lecture/create"><i class="fa fa-plus"></i> Lecture</a>
		<a class="btn btn-primary btn-sidebar" href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/quiz/create"><i class="fa fa-plus"></i> Quiz</a>
		<a class="btn btn-primary btn-sidebar" href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/edit"><i class="fa fa-pencil-square-o"></i> Section</a>
		
		<button type="button" class="btn btn-danger btn-sidebar" data-toggle="modal" data-target="#delete-{{$section->slug}}"><i class="fa fa-trash"></i> Section</button>
		<div id="delete-{{$section->slug}}" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<h4>Confirm Delete for Section {{$section->title}}</h4>
						<a class="btn btn-danger" href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}/delete"><i class="fa fa-trash"></i> Confirm Delete</a>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if(!$course->price)
		@include('partials.elements.ad')
		@endif
	</div>
	<div class="col col-md-8">
		@yield('sectioncontent')
	</div>

</div>
@stop