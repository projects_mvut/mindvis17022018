<p>Hi {{$name}},</p>
<p>Thanks for your interest in the "GATE Mechanical Engineering 2020 Exam Online Preparation Course" at MindVis.</p>
<p>Feel free to browse through the course page at this link, https://mindvis.in/courses/gate-2020-mechanical-engineering to understand the course pattern and deliverables.</p>
<p>To watch the demo videos from the course, please check the link, https://vimeopro.com/user34808304/mechanical-engineering and use the password: 4822 to access the videos on this link.</p>
<p>For Maths video, please click</p>
<p>https://www.youtube.com/watch?v=ZgymPDxOmjM<br />https://www.youtube.com/watch?v=T__VMebeh10</p>
<p>Some other course details are as follows:</p>
<p>1) Once you enrol for the course, you will be assigned a mentor who will talk to you and guide you how to study the course.</p>
<p>2) Upon successful enrollment and orientation with the mentor, you will be given a "Study Plan" as per which you will have to cover the assigned sections, attempt online quizzes and attend LIVE sessions.</p>
<p>3) In a nutshell, the course is designed to run in a manner which will give you enough time to work on each topic and ask doubts in that.</p>
<p>4) The video lectures are available 24X7 on the website. You can refer them as and when required. The LIVE sessions with the mentor will be done as per your study plan, but you can yourself call or email the mentor for some query anytime.</p>
<p>5) The course subscription fee is INR 10,000 (Assisted) and INR 8000 (Self-Paced), which you can pay by the following methods:<br />(a) Online Payment (Debit card/ Credit card)<br />(b) Direct Bank Deposit</p>
<p>We hope we have satisfied all your queries and doubts regarding the course. Hope to see you soon in a LIVE class!</p>
<p>MindVis India<br />F-452, Industrial Area,<br />Sector 91,<br />Mohali, Punjab (INDIA)</p>
<p>&nbsp;</p>
<img src="https://ci6.googleusercontent.com/proxy/WgKn9ixI5-g832uSRFDzKNssNJ7lz_C6vCQkFdVR5kLdDVqOnwIxsD74krzFEgkg7lF4v3gqdQ=s0-d-e1-ft#http://i60.tinypic.com/2br40n.jpg" width="96" height="96" class="m_8812908721295755988gmail-m_-4787938364094503609gmail-CToWUd m_8812908721295755988gmail-CToWUd CToWUd">
<p>www.mindvis.in<br />www.facebook.com/mindvis<br />www.twitter.com/mindvisindia</p>
<p>Contact Us: +91-8283-86-5488</p>
