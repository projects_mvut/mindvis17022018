<div class="head">
<img src="http://mindvis.in/images/logo.png" alt="Mindvis" width="120px"><div style="text-align:center;font-size:20px;font-weight:600;display:inline-block;position: relative;top:-22px;">Mindvis</div>
</div>
<h2>Verify Your Email Address</h2>

<div>
    Thanks for creating an account with Mindvis.
    Please follow the link below to verify your email address
    {{ URL::to('register/verify/' . $token) }}.<br/>
</div>