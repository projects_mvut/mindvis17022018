<body class="mcd np">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
<tbody><tr>
<td align="left" valign="top" id="bodyCell">
<!-- BEGIN TEMPLATE // -->
<!--[if gte mso 9]>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
<tr>
<td align="center" valign="top" width="600" style="width:600px;">
<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
<tbody><tr>
<td valign="top" id="templateHeader" mc:container="header_container" mccontainer="header_container" class="tpl-container dojoDndSource dojoDndTarget dojoDndContainer"><div class="mojoMcContainerEmptyMessage" style="display: none;"></div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_0" widgetid="mojo_neapolitan_preview_McBlock_0">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
<tbody class="mcnImageBlockOuter">
<tr>
<td valign="top" style="padding:9px" class="mcnImageBlockInner">
<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
<tbody><tr>
<td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


<img align="center" alt="" src="http://mindvis.in/images/logo.png" width="150" style="max-width:150px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage blockDropTarget" id="dijit__Templated_0" widgetid="dijit__Templated_0">


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_1" widgetid="mojo_neapolitan_preview_McBlock_1">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
<tbody class="mcnDividerBlockOuter">
<tr>
<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #000000;">
<tbody><tr>
<td>
<span></span>
</td>
</tr>
</tbody></table>
<!--            
<td class="mcnDividerBlockInner" style="padding: 18px;">
<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div></td>
</tr>
<tr>
<td valign="top" id="templateBody" mc:container="body_container" mccontainer="body_container" class="tpl-container dojoDndSource dojoDndTarget dojoDndContainerOver"><div class="mojoMcContainerEmptyMessage" style="display: none;"></div><div class="mojoMcBlock tpl-block dojoDndItem dojoDndItemOver" id="mojo_neapolitan_preview_McBlock_2" widgetid="mojo_neapolitan_preview_McBlock_2">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
<tbody class="mcnTextBlockOuter">
<tr>
<td valign="top" class="mcnTextBlockInner">

<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
<tbody><tr>

<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

<p style="line-height: 20.8px; text-align: justify;">Welcome to MindVis! We are excited to have you on board.<br>
<br>
<p>Please follow the link below to verify your email address
	<a href="https://mindvis.in/register/verify/{{$token}}">Verify</a>
<br>
Soak yourself in knowledge!<br>
Cheers,<br>
MindVis</p>

</td>
</tr>
</tbody></table>

</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_3" widgetid="mojo_neapolitan_preview_McBlock_3">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
<tbody class="mcnDividerBlockOuter">
<tr>
<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #000000;">
<tbody><tr>
<td>
<span></span>
</td>
</tr>
</tbody></table>
<!--            
<td class="mcnDividerBlockInner" style="padding: 18px;">
<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_4" widgetid="mojo_neapolitan_preview_McBlock_4">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
<tbody class="mcnFollowBlockOuter">
<tr>
<td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
<tbody><tr>
<td align="center" style="padding-left:9px;padding-right:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
<tbody><tr>
<td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td align="center" valign="top">
<!--[if mso]>
<table align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
<![endif]-->

<!--[if mso]>
<td align="center" valign="top">
<![endif]-->


<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
<tbody><tr>
<td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
<tbody><tr>
<td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
<table align="left" border="0" cellpadding="0" cellspacing="0" width="">
<tbody><tr>

<td align="center" valign="middle" width="24" class="mcnFollowIconContent">
<a href="http://www.facebook.com/mindvis" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
</td>


</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

<!--[if mso]>
</td>
<![endif]-->

<!--[if mso]>
<td align="center" valign="top">
<![endif]-->


<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
<tbody><tr>
<td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
<tbody><tr>
<td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
<table align="left" border="0" cellpadding="0" cellspacing="0" width="">
<tbody><tr>

<td align="center" valign="middle" width="24" class="mcnFollowIconContent">
<a href="http://www.twitter.com/mindvisindia" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>
</td>


</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

<!--[if mso]>
</td>
<![endif]-->

<!--[if mso]>
<td align="center" valign="top">
<![endif]-->


<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
<tbody><tr>
<td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
<tbody><tr>
<td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
<table align="left" border="0" cellpadding="0" cellspacing="0" width="">
<tbody><tr>

<td align="center" valign="middle" width="24" class="mcnFollowIconContent">
<a href="www.mindvis.in" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class=""></a>
</td>


</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

<!--[if mso]>
</td>
<![endif]-->

<!--[if mso]>
<td align="center" valign="top">
<![endif]-->


<table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
<tbody><tr>
<td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
<tbody><tr>
<td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
<table align="left" border="0" cellpadding="0" cellspacing="0" width="">
<tbody><tr>

<td align="center" valign="middle" width="24" class="mcnFollowIconContent">
<a href="https://www.youtube.com/channel/UCcZ9l294MgHN3d4P3_f45Jw" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-youtube-48.png" style="display:block;" height="24" width="24" class=""></a>
</td>


</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

<!--[if mso]>
</td>
<![endif]-->

<!--[if mso]>
</tr>
</table>
<![endif]-->
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_5" widgetid="mojo_neapolitan_preview_McBlock_5">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
<tbody class="mcnDividerBlockOuter">
<tr>
<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #000000;">
<tbody><tr>
<td>
<span></span>
</td>
</tr>
</tbody></table>
<!--            
<td class="mcnDividerBlockInner" style="padding: 18px;">
<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div></td>
</tr>
<tr>
<td valign="top" id="templateFooter" mc:container="footer_container" mccontainer="footer_container" class="tpl-container dojoDndContainer dojoDndSource dojoDndTarget"><div class="mojoMcContainerEmptyMessage" style="display: none;"></div><div class="mojoMcBlock tpl-block dojoDndItem" id="mojo_neapolitan_preview_McBlock_6" widgetid="mojo_neapolitan_preview_McBlock_6">
<div class="editedOverlay" style="display:none" data-dojo-attach-point="editedOverlay">
<span data-dojo-attach-point="editType"></span> <span class="userName" data-dojo-attach-point="editedBy"></span>
</div>

<!-- Content of the block will get inserted inside of this -->
<div data-dojo-attach-point="containerNode"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
<tbody class="mcnTextBlockOuter">
<tr>
<td valign="top" class="mcnTextBlockInner">

<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
<tbody><tr>

<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

<div style="text-align: center;"><em>Copyright © 2016MindVis, All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
<u><strong>MindVis</strong></u><br>
F-452, Industrial Area, Sector 91,<br>
Mohali, Punjab-160062,<br>
India.<br>
<u><strong>info@mindvis.in</strong></u></div>

</td>
</tr>
</tbody></table>

</td>
</tr>
</tbody>
</table></div>

<div class="tpl-block-controls">
<span title="Drag to Reorder" class="tpl-block-drag dojoDndHandle freddicon vellip-square"></span>
<a title="Edit Block" href="#" class="tpl-block-edit" data-dojo-attach-point="editBtn"><span class="freddicon edit"></span></a>
<a title="Duplicate Block" href="#" class="tpl-block-clone" data-dojo-attach-point="cloneBtn"><span class="freddicon duplicate"></span></a>
<a title="Delete Block" href="#" class="tpl-block-delete" data-dojo-attach-point="deleteBtn"><span class="freddicon trash"></span></a>
</div>
</div></td>
</tr>
</tbody></table>
<!--[if gte mso 9]>
</td>
</tr>
</table>
<![endif]-->
<!-- // END TEMPLATE -->
</td>
</tr>
</tbody></table>
</center>

</body>