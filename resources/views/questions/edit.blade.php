@extends('layouts.pages')

@section('content')
<section>
<div class="container">
<div class="row">
<h1>Edit the Question</h1>

{!!Form::model($question,
	[  	'url'=>'courses/'.$course->slug.'/quizzes/'.$quiz->slug.'/questions/'.$question->question_number ,
	   	'method'=>'patch'])!!}
@include('partials.forms.question')

@stop