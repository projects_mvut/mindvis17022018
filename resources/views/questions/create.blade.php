@extends ('layouts.pages')

@section('content')

<h1>Add a new Question</h1>

{!!Form::open(['url'=>'courses/'.$course->slug.'/quizzes/'.$quiz->slug.'/questions'])!!}
@include('partials.forms.question')

@stop
