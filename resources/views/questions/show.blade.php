@extends ('layouts.pages')

@section('content')
<section>
	<div class="container">
	<div class="row">
		<h1>{{$quiz->title}}</h1>
	<div class="col col-md-3 quiz-sidebar">
		<a class="btn btn-default" href="/courses/{{$course->slug}}/subject/{{$quiz->section->subject->slug}}/section/{{$quiz->section->slug}}/quiz/{{$quiz->slug}}">Back to Quiz</a>
		<a class="btn btn-primary" href="/courses/{{$course->slug}}/quizzes/{{$quiz->slug}}/questions/{{$question->question_number}}/edit">Edit</a>
		<a class="btn btn-danger" href="/courses/{{$course->slug}}/quizzes/{{$quiz->slug}}/questions/{{$question->question_number}}/delete">Delete</a>
	</div>
	<div class="col col-md-9" style="padding-left: 70px;">
		<table class="table table-bordered">
			<tr>
				<td class="tdhed">Question No.</td>
				<td>{{$question->question_number}}</td>
			</tr>
			<tr>
				<td class="tdhed">Title</td>
				<td>{{$question->title}}</td>
			</tr>
			<tr>
				<td class="tdhed">Content</td>
				<td>{!! $question->content !!}</td>
			</tr>
			@if($question->type=='mcq')
			<tr>
				<td class="tdhed">Type</td>
				<td>MCQ</td>
			</tr>
			<tr>
				<td class="tdhed">Option 1</td>
				<td>{{$question->option1}}</td>
			</tr>	<tr>
				<td class="tdhed">Option 2</td>
				<td>{{$question->option2}}</td>
			</tr>	<tr>
				<td class="tdhed">Option 3</td>
				<td>{{$question->option3}}</td>
			</tr>	<tr>
				<td class="tdhed">Option 4</td>
				<td>{{$question->option4}}</td>
			</tr>		
			@else
			<tr>
				<td class="tdhed">Type</td>
				<td>Fill ups</td>
			</tr>
			@endif
			<tr>
				<td class="tdhed">Correct Answer</td>
				<td>{{$question->correct_answer}}</td>
			</tr>
			<tr>
				<td class="tdhed">Positive Marks</td>
				<td>{{$question->positive_marks}}</td>
			</tr>
			<tr>
				<td class="tdhed">Negative Marks</td>
				<td>{{$question->negative_marks}}</td>
			</tr>
			<tr>
				<td class="tdhed">Solution</td>
				<td>{!! $question->solution !!}</td>
			</tr>
		</table>
	</div>

@stop
