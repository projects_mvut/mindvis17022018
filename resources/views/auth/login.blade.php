@extends ('layouts.pages')
@section('pageTitle', 'Login to MindVis')
@section('content')
<section class="main white-bg">
<div class="container">
<div class="row">
<div class="cred">
@include('auth.social')
@include('partials.forms.login')
</div>
</div>
</div> 
</section>

@stop
