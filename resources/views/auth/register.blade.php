@extends ('layouts.pages')

@section('pageTitle', 'Register for a MindVis account')


@section('headinclude')
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('content')
<div class="cred">
@include('auth.social')
@include('partials.forms.register')
</div>
@stop
