@extends ('layouts.pages')

@section('content')

<h2>Page Not Found</h2>

<a href="/courses" class="btn btn-primary">Browse Courses</a>

@stop