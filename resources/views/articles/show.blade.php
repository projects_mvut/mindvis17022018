@extends ('layouts.pages')

@section('content')
@include('partials.newsletter')
 <section class="">
      <div class="container-fluid">
      <div class="row">
<div class="art-show">
	<div class="art-img">
		@if($article->banner)
		<img src="{{$article->banner}}">
		@endif
		<div class="art-author">
			@if($author)
			<a href="/articles/{{$article->slug}}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit the Article</a>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$article->slug}}"><i class="fa fa-trash"></i> Delete this Article</button>
		    <div id="delete-{{$article->slug}}" class="modal fade" role="dialog">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-body">
		            <h4>Confirm Delete for Article {{$article->title}}</h4>
		            <a href="/articles/{{$article->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		          </div>
		        </div>
		      </div>
		    </div>
			@endif
		</div>
	</div>
	</div>
	</div>
	</div>
	</section>
	 <section class="main other white-bg">
      <div class="container">
      <div class="row">
	<div class="art-content">
		<h4>{{$article->title}}</h4>
		<div class="all-content">{!! $article->content !!}</div>
	</div>
	@include('partials.elements.ad')
	<div id="disqus_thread"></div>
	</div>
	</div>
	</section>
<script>
var disqus_config = function () {
this.page.url = 'http://mindvis.in/articles/{{$article->slug}}';
this.page.identifier = '{{$article->slug}}';
};

(function() {
var d = document, s = d.createElement('script');

s.src = '//mindvis.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
</div>

@stop