@extends ('layouts.pages')

@section('content')

<h1>Add a new Article</h1>

{!!Form::open(['url'=>'articles','files' => true])!!}

@include('partials.forms.article')

@stop