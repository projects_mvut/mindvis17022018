@extends ('layouts.pages')

@section('content')

<h1>Edit the Article</h1>

{!!Form::model($article,['url'=>'/articles/'.$article->slug,'method'=>'patch','files' => true])!!}

@include('partials.forms.article')

@stop