@extends ('layouts.pages')
@section('pageTitle', 'GATE & IBPS PO preparation tips and articles')
@section('content')	
@include('partials.newsletter')

<section class="main other white-bg">
<div class="container">
<div class="row">
	<div class="col col-sm-4">
		<h4>Categories <i class="fa fa-bars"></i></h4>
		<div class="bow"><a href="/articles/page/1"><i class="fa fa-play" aria-hidden="true"></i> All</a></div>
		@foreach($blogs as $bow)
		<div class="bow">
			<a href="/blog/{{$bow->slug}}"><i class="fa fa-play" aria-hidden="true"></i> {{$bow->title}}</a> 
			@if(Auth::check())
			@if(Auth::user()->is_admin)
			<a href="/blog/{{$bow->slug}}/delete"><span class="delo"><i class="fa fa-trash"></i></span></a>
			@endif
			@endif
		</div>
		@endforeach
		@if(Auth::check())
		@if(Auth::user()->is_admin)
		{!!Form::open(['url'=>'blog'])!!}
		{!! Form::text('title',null,['placeholder'=>'Enter the Title','class'=>'form-control']) !!}
		{!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
		{!!Form::close()!!}
		@endif
		@endif
		
	</div>
	<div class="col col-sm-8">
		@if($blog)
		<h3>Article for Category <i>{{$blog->title}}</i></h3>
		@endif
		@if(Auth::check())
		@if(Auth::user()->is_teacher or Auth::user()->is_admin)
		<div class="art-butt">
			<a href="/articles/create" class="btn btn-primary">Add Article</a>
		</div>
		@endif
		@endif
		@if(!$articles->isEmpty())		
		@foreach($articles as $articl)
		<div class="row arti">
			<h4><a href="/articles/{{$articl->slug}}">{{$articl->title}}</a></h4>
			<div><a href="/articles/{{$articl->slug}}"><img src="{{$articl->url}}" width="300px" height="150px"></a></div>
			<div>
				<p>{!! $articl->preview !!}</p>
			</div>
		</div>
		@endforeach
		<div class="pene">
			@if($prev)
			<a href="/articles/page/{{$page-1}}" class="">Previous...</a>
			@endif
			@if($next)
			<a href="/articles/page/{{$page+1}}" class="pull-right">Next...</a>
			@endif
		</div>
		@else
		<p>No articles added.</p>		
		@endif
	</div>
</div>
</div>
</section>

@stop
