@extends('layouts.main')

@section('pagecontent')
@include('partials.elements.nav')
@include('partials.flash')
<div class="row curriculum" style="background:{{$color}}">
	<div class="col col-md-4">
		<div class="curil" style="color:{{$fontcolor}}">
			<h1><a href="/courses/{{$course->slug}}"  style="color:{{$fontcolor}}">{{$course->title}}</a></h1>
			<p>{{$course->preview}}</p>
			@if(Auth::check() and Auth::user()->is_teacher)
			<a href="/courses/{{$course->slug}}/subject/create" class="btn btn-primary"><i class="fa fa-plus"></i> Subject</a>
			@endif
		</div>
	</div>
	<div class="col col-md-8 curira">
		@foreach($subjects as $subject)
		<div class="row curir">
			<div class="col col-sm-6 subject">
				<h2>{{$subject->title}}</h2>
				<div class="subprev">{{$subject->preview}}</div>
				@if(Auth::check() and Auth::user()->is_teacher)
				<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>

				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$subject->slug}}"><i class="fa fa-trash"></i></button>
				<div id="delete-{{$subject->slug}}" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<h4>Confirm Delete for Subject {{$subject->title}}</h4>
								<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
				@endif
			</div>
			<div class="col col-sm-6 sections">
				@if(Auth::check() and Auth::user()->is_teacher)
				<a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/create" class="btn btn-primary"><i class="fa fa-plus"></i> Section</a>
				@endif
				@foreach($subject->sections()->get() as $section)
				<h4><a href="/courses/{{$course->slug}}/subject/{{$subject->slug}}/section/{{$section->slug}}"><i class="fa fa-puzzle-piece"></i> {{$section->title}}</a></h4>
				@endforeach
			</div>
		</div>
		@endforeach
	</div>
</div>

@include('partials.elements.searchmodal')
@include('partials.elements.footer')

@stop