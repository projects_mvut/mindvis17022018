@extends ('layouts.pages')

@section('content')
<section>
<div class="container">
<div class="row">
<div class="quiztake">
    <div class="row">
        <div class="col col-md-4">
            <h1>{{$quiz->title}}</h1>
            <p>{{$quiz->preview}}</p>
            <a href="/courses/{{$course->slug}}" class="btn btn-default btn-block">Back To Course</a><br>
            <a href="/quiz/{{$quiz->slug}}/start" class="btn btn-primary btn-block">Retake Quiz</a><br>
            <a href="/quiz/{{$quiz->slug}}/showques" class="btn btn-success btn-block">Show All Questions</a>
        </div>
        <div class="col col-md-8">
            <table class="table">
                <thead>
                    <tr>
                        <th>Question Number</th>
                        <th>Correct Answer</th>
                        <th>Your Answer</th>
                        <th>Marks</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($questions as $question)
                    <?php 
                    $attempt=\App\Attempt::where('test_id',$test->id)->where('qno',$question->question_number)->first();
                    ?>
                    <tr>
                        <td>{{$question->question_number}}</td>
                        <td>{{$question->correct_answer}}</td>
                        @if($attempt)
                        <td>{{$attempt->answer}}</td>
                            @if($attempt->answer==$question->correct_answer)
                            <td>{{$question->positive_marks}}</td>
                            @else
                            <td>-{{$question->negative_marks}}</td>
                            @endif
                        @else
                            <td></td>
                            <td>0</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
		<h3>Finished</h3>
		<p>You've scored {{$marks}} out of {{$total_marks}}.</p>
		</div>
	</div>
</div>


@stop
