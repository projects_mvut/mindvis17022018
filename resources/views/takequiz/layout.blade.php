<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">    
	<meta name="description" content="Give your exam preparation a boost with online classes for GATE, BANK PO, SSC, GRE, GMAT by India's BEST teachers."/>
	<link href="/logo.png" rel="icon" type="image/x-icon" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MindVis</title>
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
	@yield('headinclude')
</head>
<body>
	<div class="quiznav">
		<a href="/"><img src="/logo.png" alt="MindVis" width="100px"></a>
	</div>
	<div class="container">
		@yield('quizcontent')  	
	</div>
	<div id="footer">
		<div class="container">
			<hr class="footer-divider">
			<p class="pull-right tp"><a href="#">Back to top</a></p>
			<p class="copy">&copy; 2017 MindVis.in </p>
		</div>
	</div>
	<script src="{{ elixir('js/all.js') }}"></script>
	<script src="/timer.js"></script>
	@yield('footinclude')
</body>
</html>