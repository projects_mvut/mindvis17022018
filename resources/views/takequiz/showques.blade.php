@extends ('layouts.pages')

@section('content')

<section>
<div class="container">
<div class="row">
<div class="quiztake">
    <div class="row">
        <div class="col col-md-4">
                    <a href="/courses/{{$course->slug}}" class="btn btn-default btn-block">Back To Course</a>


           
        </div>
        <div class="col col-md-8">


                     @foreach($questions as $question)
                      <?php 
                    $attempt=\App\Attempt::where('test_id',$test->id)->where('qno',$question->question_number)->first();
                    ?>
                   


                <div class="col-md-12">
                <div class="solved" style="float:left; width:100%; padding:20px 0 20px; border-bottom:solid 1px #e8e8e8;">
                <div class="question" style="float:left; width:100%; text-align:left">
                <span style="float:left; padding:10px; display:none;">{{$question->question_number}}.</span> <b>Question: {{$question->question_number}}</b> {!!$question->content!!}
                </div>
<div class="solution">
<b>Solution: </b>{!!$question->solution!!}
</div>
<div class="your-answer">
<b>Correct Answer: </b>{{$question->correct_answer}}
</div>
 @if($attempt)
<div class="your-answer">
<b>Your Answer: </b>{{$attempt->answer}}
</div>
 @else
 <div class="your-answer">
<b>Your Answer: </b><span style="color:red">Not Answered</span>
</div>
@endif
</div>
                   </div>
                    @endforeach



		</div>
	</div>
</div>


@stop
