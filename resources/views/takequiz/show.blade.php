@extends ('takequiz.layout')

@section('quizcontent')
<div id="fold">
    
<div class="quiztake">
    <div class="row">
        <div class="col col-sm-4">
            <div id="countdowntimer"><div id="timer"></div></div>
        <div id="quizscroll">
            @foreach($questions as $quest)
            <?php
            $ans=\App\Attempt::where('test_id',$test->id)->where('qno',$quest->question_number)->first();
            if($ans){
                if($ans->marked){
                    $color="warning";                    
                }
                else{
                    $color="success";                    
                }
            }
            else{
                $color="default";                  
            }
            if($quest->question_number==$question->question_number){
                $color="primary";
            }
            ?>
                <a href="/quiz/{{$quiz->slug}}/question/{{$quest->question_number}}" class="btn btn-{{$color}}">
                    {{$quest->question_number}}
                </a>
                @endforeach
            </ul>
        </div></div>
        <div class="col col-sm-8">
            <h3>Question No. {{$question->question_number}} </h3>
            <h3>{{$question->title}}</h3>
            <div class="all-content">{!! $question->content !!}</div>
{!!Form::open(['url'=>'quiz/'.$quiz->slug.'/question/'.$question->question_number,'id'=>'quizform'])!!}
@if($question->type=='mcq')
<div class="row">
    <div class="col col-md-6">
        <div class="radio">A. <br>
            <label> 
                {!! Form::radio('answer', '1',$answer==1,['class'=>'radio1']) !!} {{$question->option1}}
            </label>
        </div>
    </div>
    <div class="col col-md-6">
        <div class="radio">B. <br>
            <label> 
                {!! Form::radio('answer', '2',$answer==2,['class'=>'radio2']) !!} {{$question->option2}}
            </label>
        </div>
    </div>
    <div class="col col-md-6">
        <div class="radio">C. <br>
            <label> 
                {!! Form::radio('answer', '3',$answer==3,['class'=>'radio3']) !!} {{$question->option3}}
            </label>
        </div>
    </div>
    <div class="col col-md-6">
        <div class="radio">D. <br>
            <label> 
                {!! Form::radio('answer', '4',$answer==4,['class'=>'radio4']) !!} {{$question->option4}}
            </label>
        </div>
    </div>
</div>
@else
<div class="form-group">
<input name="answer" type="text" placeholder="Enter your Answer" class="form-control fillupform" value="{{$answer}}">
</div>
@endif
<div class="form-group quiztake-btn">
    {!! Form::submit('Submit &amp; Next',['class'=>'btn btn-success quizsub','name'=>"submit"]) !!}
    {!! Form::submit('Mark for Review &amp; Next',['class'=>'btn btn-warning quizsub','name'=>"submit"]) !!}
    <a href="/{{$nexturl}}" class="btn btn-default quizsub">Next</a>
    <a href="/quiz/{{$quiz->slug}}/finished" class="btn btn-danger quizsub">Finish Quiz</a>
</div>
{!!Form::close()!!}

</div>
</div>
</div>
@include('partials.errors')
@stop
</div>

@section('footinclude')
<script>
$(function(){
    $("#timer").countdowntimer({hours : {{$hours}},minutes : {{$minutes}},seconds : {{$seconds}},size : "lg",timeUp : timeisUp});
    function timeisUp(){
        window.location.href="/quiz/{{$quiz->slug}}/finished";
    }
    $(".radio{{$answer}}").prop("checked", true);
});
</script>
@stop