@extends ('layouts.pages')
@section('pageTitle', 'HelpDesk')
@section('content')


<div class="helpdesk">
<h1>Submit Your Query</h1>
{!!Form::open()!!}
@include('partials.forms.helpdesk')

@stop
