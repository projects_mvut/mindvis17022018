<!DOCTYPE html>
  <html lang="en">
  <head>
  	<meta charset="UTF-8">
    <link href="/logo.png" rel="icon" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GATE 2020 Preparation Tips</title>
    <meta name="keywords" content="gate 2020 preparation tips, gate 2020, gate mechanical engineering online coaching, gate mechanical online lectures, gate mechanical online classes, gate mechanical online coaching, gate 2020 mechanical engineering online coaching, gate 2020 mechanical
"/>
<meta name="description" content="Prepare for GATE Mechanical Engineering 2020 with the BEST Online Coaching. "/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Ubuntu:400,700');

    @media(max-width: 640px){
    	#main h1{
    		font-size: 30px !important;
    	}
    	#main h4{
    		font-size: 12px !important;
    	}
    	#title{
    		font-size: 20px !important;
    	}
    	.tile{
    		float: left;
    		width: 100%;
    		padding: 20px 0;	
    	}
    }
body{
	font-family: 'Ubuntu', sans-serif;
}
    	.form-control{
    		border-top: 0;	
    	}
   section#main{
   	  background: linear-gradient(rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55)),
                rgba(0,0,0,0.55) url('/images/laptop-2557572_1920.jpg') no-repeat center;
    background-size: cover;
    color: #fff;
    text-align: center;
    text-rendering: optimizeLegibility;
   }
    </style>
      </head>
  <body>
  	
<section id="main" style="height:100vh; padding:30px 0; position:relative;overflow:hidden">

<div style="   position: relative;
    top: 50%; 
    transform: translateY(-50%);
    -webkit-transform: translateY(-50%);">
<h1 style="color:#fff; font-weight:800; font-size:50px;">GATE 2020 Preparation Tips</h1>
<h4 style="color:#fff; font-weight:800; font-size:16px;">A Complete guide to help you achieve success in GATE 2020 exam [ FREE E-Book ]</h4>
<a href="#getitnow" class="btn btn-default" style="background:#fff; color:#000; margin-bottom:20px;">Get it now</a>
</div>
<!-- <img src="/images/book.png" width="365px" class="img-responsive" style="margin:0 auto" 	>
 -->
</section>

<section style="padding:30px 0;">
	<div class="container">

	<div class="row">

		<div class="col-md-12 text-center">

<h3 style="color:#000; font-weight:800; margin-bottom:60px	" id="title">What will you learn from this E-book ?

</h3>

<div class="col-md-4">
<div class="row text-center">
<div class="tile">
<img src="/images/checked.svg" width="65" style="margin-bottom:10px;">
<h5 style="padding:0px 20px"> All About GATE 2020 
</div>
</h5>
</div>
</div>
<div class="col-md-4">
<div class="row text-center">
<div class="tile">
<img src="/images/checked.svg" width="65" style="margin-bottom:10px;">
<h5 style="padding:0px 20px"> Study and prepration Tips and Guidelines 
</div>
</h5>
</div>
</div>
<div class="col-md-4">
<div class="row text-center">
<div class="tile">
<img src="/images/checked.svg" width="65" style="margin-bottom:10px;">
<h5 style="padding:0px 20px"> Eligible for 15% off on GATE 2020 online prep course subscription
</h5>
</div>
</div>
</div>


		</div>
	</div>
	</div>
</section>




<section style="padding:30px 0;background: #f6f6f6;" id="getitnow">
	<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<h3 style="color:#000; font-weight:800; margin-bottom:60px	" id="title">Get Your Secret of Success
</h3>
    <div class="col-sm-6 col-sm-offset-3">
    <div style="float:left; width:100%; background:#fff; padding:20px;     box-shadow:0 10px 6px -6px #7777773b; border: #b2b2b23b solid 1px; border-bottom: 0;border-radius: 5px;
    border-top: 5px solid #21bcb4;
">
{!!Form::open(['url'=>'gate-2020-preparation-tips-free-ebook', 'class' => ''	])!!}
		<div class="form-group">
		{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('city',null,['placeholder'=>'City','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('working_or_student',null,['placeholder'=>'Are you a Working Professional or a Student ?','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		<p> <b>Would you like us to contact you regarding the course?</b></p>
		<select name="like_us_to_contact" class="form-control" style="width: 100px;">
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
		</div>
		<div class="form-group">
			{!! Form::submit('Mail me',['class'=>'btn btn-primary btn-block	','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
</div>
</div>
</div>
</div>
</section>
  @include('partials.flash')
		@include('partials.errors')








      
<div class="ebook-gate hidden">
<div class="ebook1">
	<div class="container">
		<div class="ebook-header">
		<h2>A Complete guide to help you achieve success in GATE 2020 exam [ FREE E-Book ]</h2>
		</div>
<div class="row">
	<div class="col col-md-6">
		<img src="/images/qw.jpg" class="ebook-image">
	</div>
	<div class="col col-md-6">
		<div class="ebook-form">
		<div class="ebook-what">
			<h4><b>What will you learn from this E-book ?</b></h4>
			<p><i class="fa fa-star"></i> All About GATE 2020 <br><i class="fa fa-star"></i> Study and prepration Tips and Guidelines <br> <i class="fa fa-star"></i> Eligible for <span class="ebook-discount disca">15% off</span> on GATE 2020 online prep course subscription</p>
		</div>
		{!!Form::open(['url'=>'gate-2020-preparation-tips-free-ebook'])!!}
		<div class="form-group">
		{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('city',null,['placeholder'=>'City','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('working_or_student',null,['placeholder'=>'Are you a Working Professional or a Student ?','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		<p> <b>Would you like us to contact you regarding the course?</b></p>
		<select name="like_us_to_contact" class="form-control" style="width: 100px;">
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
		</div>
		<div class="form-group">
			{!! Form::submit('Mail me the Secret of Success',['class'=>'btn btn-danger','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
	</div></div>
</div>
</div>
</div>
</div>

<div class="ebook-foot"><div class="container"><div class="row">
	<div class="col col-md-6">
		<p>Address</p>
		<p>F-452, Industrial Area, Sector 91</p>
		<p>Mohali - Punjab (INDIA)</p>
		<p>Mobile: +91 9779434433 , Email: info@mindvis.in</p>
	</div>
	<div class="col col-md-6">
		Follow Us <p></p>
		<a href="http://www.facebook.com/mindvis"><i class="fa fa-facebook customfb"> </i></a>
		<a href="http://plus.google.com/mindvis"><i class="fa fa-google-plus"></i></a>
		<a href="https://twitter.com/mindvisindia"><i class="fa fa-twitter"></i></a>
	</div>
</div></div></div>
<script src="{{ elixir('js/all.js') }}"></script>
  </body>
</html>
