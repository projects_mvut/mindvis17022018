@extends ('layouts.main')
@section('pageTitle', 'Vision')
@section('pagecontent')
@include('partials.elements.nav')

<img src="/images/emp.jpg" style="width:100%">

<div class="ribbon">
<h1>Vision</h1>	
</div>
<section class="main other white-bg">
<div class="container">
<div class="row">
<p><em><strong>MindVis </strong></em><strong>is a step towards making learning empowered by </strong><span style="font-weight: 400;">Beyond Engineering. The</span><span style="font-weight: 400;"> technology behind MindVis is envisioned to:</span></p>
<ul>
<ul style="list-style-type: disc;">
<li style="font-weight: 400;"><em><span style="font-weight: 400;">Organize and present online courses material to enable students to learn at their own pace;</span></em></li>
<li style="font-weight: 400;"><em><span style="font-weight: 400;">Include interactivity, online laboratories, peer-to-peer and student-to-teacher communication;</span></em></li>
<li style="font-weight: 400;"><em><span style="font-weight: 400;">Create scalable software infrastructure in order to support continuous improvement in online course delivery and student engagement and make it readily available to other educational service providers.</span></em></li>
</ul>
</ul>
<h2><strong>Why Beyond Engineering created MindVis?</strong></h2>
<p><span style="font-weight: 400;">Two vitals drive the creation of&nbsp;MindVis.</span></p>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400;">We always strive to use or develop the best teaching and learning tools possible for our students. MindVis will offer new opportunities to study how people learn best online &ndash; whether those learners are university students anywhere in the world, or non-university learners and will foster virtual communities of learners.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">We aim to shatter barriers to education. </span></li>
</ol>
<h2><strong>The path ahead</strong></h2>
<p><strong>Community</strong><span style="font-weight: 400;">: To create a new kind of virtual learning community, for &ldquo;perpetual education.&rdquo;&nbsp;</span><em><span style="font-weight: 400;">MindVis</span></em><span style="font-weight: 400;">&nbsp;will offer educators/ teachers/ trainers new ways of teaching and interacting with students online, while also helping in communication among students. </span></p>
<p><strong>Certification</strong><span style="font-weight: 400;">: The creation of an online learning platform provides the opportunity to allow students not only to learn, but also to demonstrate that they have mastered the content. We plan to offer to students, an opportunity to demonstrate their skills and progress towards earning a certificate for better job prospects/ openings.</span></p>
<p><span style="font-weight: 400;">We are sure that MindVis will evolve over time in exciting ways that today we do not anticipate. The path ahead involves bold, experimental and futuristic risk-taking reflecting the best of our values and culture. </span></p>
<p>&nbsp;</p>
</div>
</div>
</section>

@include('partials.elements.searchmodal') <!-- Search Modal -->
@include('partials.elements.footer')
@stop
