@extends ('layouts.pages')

@section('content')
<h1>Search Results for <span style="text-decoration:underline;">{{$query}}</span></h1>

<h2 class="search-article-head">Courses</h2>
@if(!$courses->isEmpty())
	<div class="row">
	@foreach($courses as $result)
		<div class="col col-md-6 courses-index">
		<a href="/courses/{{$result->slug}}">
			<img src="{{$result->url}}" height="150px" width="300px"><p></p>
		{{$result->title}}</a>
		</div>
	@endforeach
	</div>
@else
<p>No courses found.</p>
@endif


<h2 class="search-article-head">Articles</h2 class="search-article-head">
@if(!$articles->isEmpty())
<div class="row">
	@foreach($articles as $result)
	<div class="col col-md-4 search-article">
		<img src="{{$result->url}}" height="150px" width="300px"><br>
		<a href="/articles/{{$result->slug}}">{{$result->title}}</a>
	</div>
	@endforeach
</div>
@else
<p>No articles found.</p>
@endif

@stop