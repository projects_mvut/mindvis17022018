@extends ('layouts.pages')

@section('pageTitle', 'Terms and Conditions')
@section('content')

<div class="ribbon">
<h1>Website usage terms and conditions</h1>
</div>
<section class="main other white-bg">
<div class="container">
<div class="row">

<p>Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern MindVis&rsquo; relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.</p>
<p>The term &lsquo;MindVis&rsquo; or &lsquo;us&rsquo; or &lsquo;we&rsquo; refers to the owner of the website whose registered office is 4822, Sector 68, Mohali, Punjab, India. The term &lsquo;you&rsquo; refers to the user or viewer of our website.</p>
<p>The use of this website is subject to the following terms of use:</p>
<ul>
<li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
<li>This website uses cookies to monitor browsing preferences.</li>
<li>It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
<li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
<li>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</li>
<li>Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>
<li>From time to time, this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
<li> Price change may occour before any prior notice</li>
<li>For cancellations and refunds please contact us at info@mindvis.in or 09779434433.<br>refunds : (applicable within the first 24 hours of purchase of the course) </li>
</ul>
</div>
</div>
</section>
@stop
