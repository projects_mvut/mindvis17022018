<!DOCTYPE html>
  <html lang="en">
  <head>
  	<meta charset="UTF-8">
    <link href="/logo.png" rel="icon" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>India's Best GATE 2018 Mechanical Engineering Online Preparation</title>
   <meta name="keywords" content="gate online exam, online gate exam, gate online coaching, online gate preparation, gate 2018 preparation online, gate 2018 online preparation, online gate 2018 exam preparation, Online GATE 2018 preparation, gate 2018 mechanical engineering online preparation"/>
<meta name="description" content="Prepare for GATE Mechanical Engineering 2018 with the BEST Online Preparation Course. "/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
      </head>
  <body>  	
        @include('partials.flash')
		@include('partials.errors')
<div class="rev">
<div class="rev1">
	<div class="container">
		<div class="rev-header">
		<h1>Don't just prepare, join the league of outstanding performers.</h1> <h3>Do you see yourself as one of them ? </h3>
		</div>
<div class="row">
	<div class="col col-md-6">
	<div class="row">
		<div class="col col-xs-4">
			<img src="http://mindvis.in/uploads/images/manjit-buttar-P5bD5aEa.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Manjit Buttar</h4>
			<h5>Gate 2014</h5>
			<h4>Marks : 58.17</h4>
		</div>
		<div class="col col-xs-4">
			<img src="/images/varinder.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Varinder Singh</h4>
			<h5>Gate 2014</h5>
			<h4>Marks : 48.67</h4>
		</div>
		<div class="col col-xs-4">
			<img src="/images/manpreet.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Manpreet Singh</h4>
			<h5>Gate 2015</h5>
			<h4>Marks : 36.23</h4>
		</div>
		<div class="col col-xs-4">
			<img src="/images/umang.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Umang Shankar</h4>
			<h5>Gate 2015</h5>
			<h4>Marks : 35.21</h4>
		</div>
		<div class="col col-xs-4">
			<img src="/images/rajat.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Rajat Grover</h4>
			<h5>Gate 2015</h5>
			<h4>Marks : 32.71</h4>
		</div>
		<div class="col col-xs-4">
			<img src="/images/adarsh.jpg" alt="" width="100px" height="100px" class="img-circle">
			<h4>Adarsh Shrivastav</h4>
			<h5>Gate 2015</h5>
			<h4>Marks : 38.71</h4>
		</div>
		<div>
		</div>
	</div>
			<h4>They wanted to stay ahead of their competition, and we made sure they did!</h4>
	</div>
	<div class="col col-md-6">
		<div class="seminar-form">
		{!!Form::open(['url'=>'gate-mechanical-engineering-2018'])!!}
		<div class="form-group">
		{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('city',null,['placeholder'=>'City','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('working_or_student',null,['placeholder'=>'Are you a Working Professional or a Student ?','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Take me Ahead',['class'=>'btn btn-danger','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
	</div></div>
</div>
<div class="row">
	<div class="col col-md-6">
		<img src="http://mindvis.in/uploads/images/gaganpreet-hundal-XJoTzpyJ.jpeg" alt="" width="100px" height="100px" class="img-circle">
		<h3>Gaganpreet Singh Hundal</h3>
		<h5>Gate 2015</h5>
		<h4>Reg No:ME33077S4277</h4>
		<h4>Marks : 66.06</h4>
		<div class="revtext">
			<p>Truly India's Best online course for GATE Mechanical Engineering. Thanks to the course and the experienced instructor, I was able to score 66.06 marks in my first attempt!</p>
		</div>			
		</div>
	<div class="col col-md-6">
		<img src="http://mindvis.in/uploads/images/ashish-chopra-vGhHHmn2.jpg" alt="" width="100px" height="100px" class="img-circle">
		<h3>Ashish Chopra</h3>
		<h5>Gate 2015</h5>
		<h4>Reg No:ME33077S4128</h4>
		<h4>Marks : 53.53</h4>
		<div class="revtext">
			<p>Truly India's Best Online GATE Preparation for Mechanical Engineering. Helped me secure 55.53 marks in GATE 2015. Thank you to the team behind this awesome course.</p>
		</div>			
		</div>
	</div>
</div>
</div>
</div>
</div>

<div class="ebook-foot"><div class="container"><div class="row">
	<div class="col col-md-6">
		<p>Address</p>
		<p>F-452, Industrial Area, Sector 91</p>
		<p>Mohali - Punjab (INDIA)</p>
		<p>Mobile: +91 9779434433 , Email: info@mindvis.in</p>
	</div>
	<div class="col col-md-6">
		Follow Us <p></p>
		<a href="http://www.facebook.com/mindvis"><i class="fa fa-facebook customfb"> </i></a>
		<a href="http://plus.google.com/mindvis"><i class="fa fa-google-plus"></i></a>
		<a href="https://twitter.com/mindvisindia"><i class="fa fa-twitter"></i></a>
	</div>
</div></div></div>
<script src="{{ elixir('js/all.js') }}"></script>
  </body>
</html>
