<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mindvis</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,700|Raleway:400,500,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>

<body>
<!--Container-->
<div id="container"> 
  <!--Wrapper-->
  <div id="wrapper"> 
    <!--Header-->
    <div class="header" id="top">
      <div class="main_row">
        <div class="logo"><a href="index.html"><img src="images/logo.png" alt="Logo" /></a></div>
        <div class="navigation">
          <ul>
            <li><a href="#">Login</a></li>
            <li><a href="#">Register</a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <!--Header-End--> 
    <!--Banner-->
    <div class="banner">
      <div class="banner_image"><img src="images/banner.jpg" alt="" /></div>
      <div class="banner_content">
        <div class="main_row">
          <div class="banner_inner">
            <div class="form_area">
              <form>
                <input type="text" name="text" placeholder="Search Courses..." />
                <input type="image" name="" src="images/search.png" />
              </form>
            </div>
            <div class="banner_bottom">
              <h2>Check  Courses Manually</h2>
              <a href="#section"><img src="images/arrow.png" alt="" /></a> </div>
          </div>
        </div>
      </div>
    </div>
    <!--Banner-End--> 
    <!--Section-->
    <div class="section" id="section">
      <div class="main_row">
        <h2>best selling courses</h2>
        <div class="grid">
          <figure class="effect-sadie"> <img src="images/section5.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Prelims Exams <span> <i class="fa fa-rupee"></i> 5000 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />587 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section1.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Reasoning <span> <i class="fa fa-rupee"></i> 1000 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />234 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section4.jpg" alt="img14"/>
            <figcaption>
            <h2>IMU CET 2016 <span> <i class="fa fa-rupee"></i> 5000 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />587 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Tmc Shipping</small></span> <span class="instructor_img"><img src="images/logo_admin2.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section3.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Quant <span> <i class="fa fa-rupee"></i> Free </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />778 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section6.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Clerk GK <span> <i class="fa fa-rupee"></i> 499 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />1198 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section2.jpg" alt="img14"/>
            <figcaption>
            <h2>GRE Math <span> <i class="fa fa-rupee"></i>2000 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />354 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section5.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Prelims Exams <span> <i class="fa fa-rupee"></i> 5000 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />587 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
          <figure class="effect-sadie"> <img src="images/section6.jpg" alt="img14"/>
            <figcaption>
            <h2>IBPS Clerk GK <span> <i class="fa fa-rupee"></i> 499 </span></h2>
            <p> <span class="grid_left"> <span class="learn"><img src="images/learner.png" alt="" />1198 Learners</span> <span class="rating"><b>Rating:</b><a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a> <a href="#"><img src="images/star.png" alt="" /></a></span> <span class="read_more"><a href="#">Learn More »</a></span> </span> <span class="grid_right"> <span class="instructor"><b>Instructor</b><small>Mindvis Admin</small></span> <span class="instructor_img"><img src="images/logo_admin.png" alt="" /></span> </span> </p>
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!--Section-End--> 
  
  <!--Footer-->
  <div class="footer">
    <div class="main_row">
      <div class="footer_area">
        <h2>Connect With Mindvis</h2>
        <div class="social_icon">
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
          </ul>
        </div>
        <h2>About Us</h2>
        <div class="information">
          <ul>
            <li><a href="#">Vision</a></li>
            <li><a href="#">Team</a></li>
          </ul>
        </div>
        <h2>Help and Support</h2>
        <div class="information">
          <ul>
            <li><a href="#">Help Desk</a></li>
          </ul>
        </div>
      </div>
      <div class="footer_area">
        <h2>Browse...</h2>
        <div class="information">
          <ul>
            <li><a href="#">Courses</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Forums</a></li>
          </ul>
        </div>
        <h2>Legal / Privacy</h2>
        <div class="information">
          <ul>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Terms and Conditions</a></li>
          </ul>
        </div>
      </div>
      <div class="footer_area">
        <h2>Contact</h2>
        <p><img src="images/map.png" alt="" />F-452, Industrial Area, Sector 91 Mohali Punjab (INDIA)</p>
        <h5><img src="images/phone.png" alt="" />Mobile: +91 9779434433</h5>
        <h6><img src="images/email.png" alt="" />Email: info@mindvis.in</h6>
        <div class="footer_main">
          <p>&copy; 2015 MindVis.in</p>
        </div>
      </div>
    </div>
    <div class="top_foter"><a href="#top"><img src="images/top.jpg" alt="" /></a></div>
  </div>
  <!--Footer-End--> 
</div>
<!--Wrapper-End-->
</div>
<!--Container-End--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="js/scrolling.js" type="text/javascript"></script>
</body>
</html>