<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="propeller" content="fbe82456adc1a72a7f21916f920621d9" />
	@if(isset($keywords))
	<meta name="keywords" content="{{$keywords}}" />
	@endif
	@if(isset($description))
	<meta name="description" content="{{$description}}"/>
	@endif
	<link href="/logo.png" rel="icon" type="image/x-icon" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Online Coaching for GATE, BANK PO, SSC, GRE, GMAT @yield('pageTitle')</title>
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8893040665337634",
    enable_page_level_ads: true
  });
</script>
</head>
<body  data-spy="scroll" data-target="#scroll-spy">
	<div class="coco">
		@include('partials.flash')
		@include('partials.elements.home')
		@include('partials.elements.footer')
	</div>
	<script src="{{ elixir('js/all.js') }}"></script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-61219916-1', 'auto');
	ga('send', 'pageview');
	</script>
<script type="text/javascript">
	// Animating the carousel's captions
var carouselContainer = $('.carouse');
var slideInterval = 5000;
    
    function toggleH(){
        $('.toggleHeading').hide()
        var caption = carouselContainer.find('.active').find('.toggleHeading').addClass('animated flipInX').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function (){
            $(this).removeClass('animated flipInX')});
    caption.slideToggle();
    }
    
    function toggleC(){
        $('.toggleCaption').hide()
        var caption = carouselContainer.find('.active').find('.toggleCaption').addClass('animated fadeInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function (){
            $(this).removeClass('animated fadeInUp')
    });
    caption.slideToggle();
    }
carouselContainer.carousel({
interval: slideInterval, cycle: true, pause: "hover"})
.on('slide.bs.carousel slid.bs.carousel', toggleH).trigger('slide.bs.carousel')
.on('slide.bs.carousel slid.bs.carousel', toggleC).trigger('slide.bs.carousel');


/*courses carousel owl*/
        $(document).ready(function() {
     var owl = $("#slider-carousel");
     owl.owlCarousel({
       items: 3,
       itemsDesktop: [1000, 3],
       itemsDesktopSmall: [900, 2],
       itemsTablet: [600, 1],
       itemsMobile: false,
       pagination: false
     });
     $(".next").click(function() {
       owl.trigger('owl.next');
     })
     $(".prev").click(function() {
       owl.trigger('owl.prev');
     })
   });


// change nav on scroll
$(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() >= 50) {
            $( "nav" ).addClass( "white-nav",{duration:500} );
        } else {
            $( "nav" ).removeClass( "white-nav",{duration:500} );
        }
    });
});


/*scrollspy*/
 $(document).ready(function(){
                              var navHeight=$("#nav").outerHeight();
                              // Scrollspy initiation
                              $('body').scrollspy({ 
                                target: '#scroll-spy',
                                offset: navHeight + 2
                              });
                              // On render, adjust body padding to ensure last Scroll target can reach top of screen
                              var height = $('#howto').innerHeight();
                              var windowHeight = $(window).height();
                              var navHeight = $('nav.navbar').innerHeight();
                              var siblingHeight = $('#howto').nextAll().innerHeight();


                              if(height < windowHeight){
                              }

                              // On window resize, adjust body padding to ensure last Scroll target can reach top of screen
                              $(window).resize(function(event){
                                var height = $('#howto').innerHeight();
                                var windowHeight = $(window).height();
                                var navHeight = $('nav.navbar').innerHeight();
                                var siblingHeight = $('#howto').nextAll().innerHeight();
                                
                                
                                if(height < windowHeight){
                                }
                              });
                              
                              $('nav.navbar a, .scrollTop').click(function(event){
                                // Make sure this.hash has a value before overriding default behavior
                                if (this.hash !== "") {
                                  // Prevent default anchor click behavior
                                  event.preventDefault();

                                  // Store hash (#)
                                  var hash = this.hash;
                                  
                                  // Ensure no section has relevant class
                                  $('section').removeClass("focus");

                                  // Add class to target
                                  $(hash).addClass("focus");

                                  // Remove thy class after timeout
                                  setTimeout(function(){
                                    $(hash).removeClass("focus");
                                  }, 2000);     
                                  
                                // Using jQuery's animate() method to add smooth page scroll
                                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area (the speed of the animation)
                                  $('html, body').animate({
                                    scrollTop: $(hash).offset().top
                                  }, 600, function(){
                                    
                                    // Add hash (#) to URL when done scrolling (default click behavior)
                                    window.location.hash = hash;        
                                  });
                                      
                                  // Collapse Navbar for mobile view
                                  $(".navbar-collapse").collapse('hide');     
                                }

                              });
                              $(window).scroll(function(){
                                var scrollPos = $('body').scrollTop();
                                if(scrollPos > 0){
                                  $('.navbar').addClass('show-color');
                                  $('.scrollTop').addClass('show-button');
                                } else{
                                  $('.navbar').removeClass('show-color');
                                  $('.scrollTop').removeClass('show-button');
                                }
                                
                              });
                            });
</script>
</body>
</html>
