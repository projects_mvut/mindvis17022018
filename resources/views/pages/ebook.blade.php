<!DOCTYPE html>
  <html lang="en">
  <head>
  	<meta charset="UTF-8">
    <link href="/logo.png" rel="icon" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bank PO Exam Preparation Tips</title>
    <meta name="keywords" content="ibps exam, ibps bank exam, ibps po exam 2016, ibps bank po exam, ibps online coaching, online test series for ibps po, online exam preparation for banking, online ibps po preparation, online bank exam coaching"/>
<meta name="description" content="Prepare for IBPS PO 2016 exam with a Power Packed Online Preparation Course from MindVis. This online preparation course for IBPS PO 2016 enables you to prepare Anytime, Anywhere you want. Subscribe today!"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
      </head>
  <body>
  	
        @include('partials.flash')
		@include('partials.errors')
<div class="ebook">
<div class="ebook1">
	<div class="container">
		<div class="ebook-header">
		<h2>A Complete guide to help you achieve success in Bank PO exam [ free E-Book ]</h2>
		</div>
<div class="row">
	<div class="col col-md-6">
		<img src="/images/ebuk.png" class="ebook-image">
	</div>
	<div class="col col-md-6">
		<div class="seminar-form">
		<div class="ebook-what">
			<h4><b>What will you learn from this E-book ?</b></h4>
			<p><i class="fa fa-star"></i> All About Bank PO <br><i class="fa fa-star"></i> Study and prepration Tips and Guidelines <br> <i class="fa fa-star"></i> Eligible for <span class="ebook-discount">30% off</span> on BANK PO online prep course subscription</p>
		</div>
		{!!Form::open(['url'=>'ibps-bank-po-2016-preparation-tips-ebook'])!!}
		<div class="form-group">
		{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('city',null,['placeholder'=>'City','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('working_or_student',null,['placeholder'=>'Are you a Working Professional or a Student ?','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		<p> <b>Would you like us to contact you regarding the course?</b></p>
		<select name="like_us_to_contact" class="form-control" style="width: 100px;">
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
		</div>
		<div class="form-group">
			{!! Form::submit('Download the Secret of Success',['class'=>'btn btn-danger','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
	</div></div>
</div>
</div>
</div>
</div>

<div class="ebook-foot"><div class="container"><div class="row">
	<div class="col col-md-6">
		<p>Address</p>
		<p>F-452, Industrial Area, Sector 91</p>
		<p>Mohali - Punjab (INDIA)</p>
		<p>Mobile: +91 9779434433 , Email: info@mindvis.in</p>
	</div>
	<div class="col col-md-6">
		Follow Us <p></p>
		<a href="http://www.facebook.com/mindvis"><i class="fa fa-facebook customfb"> </i></a>
		<a href="http://plus.google.com/mindvis"><i class="fa fa-google-plus"></i></a>
		<a href="https://twitter.com/mindvisindia"><i class="fa fa-twitter"></i></a>
	</div>
</div></div></div>
<script src="{{ elixir('js/all.js') }}"></script>
  </body>
</html>
