@extends ('layouts.pages')
@section('pageTitle', 'Our Team')
@section('content')

<div class="ribbon">
<h1>Our Team</h1>
<p style="
    text-align: center;
">Here, we put our best foot forward.</p>
</div>
<section>
<div class="container">
<div class="row">
	<p>We are a group of young individuals who just want to improve the education system. We believe that education is the road to a better world and therefore intend to take education to the masses. With contemporary technological aides education has the potential to reach to every corner of the world and we are here to enable that.</p>
<p>The idea is to untap the potential of each and every individual. We do this with a combination of technology and subjective intervention through highly skilled and experienced faculty coupled with a peer to peer learning approach.</p>

<div class="row teamrow">
	<div class="col col-md-6">
		<div class="teamdiv">
		<img src="/images/h.jpg">
		<h3>Er. Himanshu Vasistha</h3>
		<p>CEO</p>
		<h5>Mechanical Engineer, Marine Engineer (MEC 3), 6 years of teaching and mentoring, Educator, motivational speaker & entrepreneur.</h5>
		</div>
	</div>
	<div class="col col-md-6">
		<div class="teamdiv">
		<img src="/images/s.jpg">
		<h3>Er. Sonali Vasistha</h3>
		<p>Director</p>
		<h5>Electronics & Communication Engineer, Founder www.beyondengg.com, 6 years of teaching and mentoring, Educationist</h5>
		</div>
	</div>
</div>

<p>We would love to connect with you if you also feel the need to improve the education system and would like to contribute. Please contact us at <b>info@mindvis.in</b> .</p>
</div>
</div>
</section>
@stop
