<!DOCTYPE html>
  <html lang="en">
  <head>
  	<meta charset="UTF-8">
    <meta name="keywords" content="online courses, GATE coaching, online GATE coaching, BANK PO coaching, online BANK PO course, gate online course, gate 2016, gate coaching, bank po coaching, gate online course, gate 2016 ece, free bank po online classes, bank po online course, gate 2016 me, free classes for gate 2016, exam preparation, Quantitative Aptitude Online Course, Online Quantitative Aptitude Course, Gate Crash Course Online, Online Gate Crash Course, Online Coaching Classes For Gate, Bank Exam Online Coaching, Online Bank Exam Coaching, Engineering Mathematics Course, Gre Online Prep Course, Gre Online Preparation Course, Gre Crash Course, Gre Prep Classes, Gate Mechanical Coaching, Gate Ece Coaching, Gate Production Engineering Coaching, Gate Mechanical Engineering Coaching, automotive design, automotive sketching, GATE" />
    <meta name="description" content="Give your exam preparation a boost with online classes for GATE, BANK PO, SSC, GRE, GMAT by India's BEST teachers."/>
    <link href="/logo.png" rel="icon" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MindVis</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
      </head>
  <body>
    <div id="wrap">
      @include('partials.elements.nav') <!-- Navigation -->
        @include('partials.flash')
		@include('partials.errors')
<div class="seminar">
	<div class="container">
	<div class="seminar-head">
		<h3>Free Webinar</h3>
		<h1>IBPS 2016 Reasoning Section - Sitting Arrangement</h1>
		<p>On December 6th at 6PM</p>
	</div>
<div class="row">
	<div class="col col-md-6">
		<iframe src="https://www.youtube.com/embed/UW9wBNMVqnU" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div>
	<div class="col col-md-6">
		<div class="seminar-form">
		{!!Form::open(['url'=>'ibps-2016'])!!}
		<div class="form-group">
		{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::text('exam',null,['placeholder'=>'Which exam are you preparing for ?','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('Do You want to join an online course to prepare ?') !!}
		<select name="interested" class="form-control" style="width: 100px;">
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
		</div>
		<div class="form-group">
			{!! Form::submit('Register',['class'=>'btn btn-primary','name'=>"submit"]) !!}
		</div>
		{!! Form::close() !!}
	</div></div>
</div>
</div>
</div>

<div class="presenter">
<div class="container">
	<img src="/images/h.jpg" class="img-circle" width="200px">
	<h3>Er. Himanshu Vasistha</h3>
	<h4>Subject Expert, Mindvis</h4>
	<p>In this session we will help you tackle your doubts and queries in an important topic in the reasoning section - Sitting Arrangement.</p>
</div>
</div>

<div class="semfoot"><div class="container"><div class="row">
	<div class="col col-md-6">
		<p>Address</p>
		<p>F-452, Industrial Area, Sector 91</p>
		<p>Mohali - Punjab (INDIA)</p>
		<p>Mobile: +91 8283865488 , Email: info@mindvis.in</p>
	</div>
	<div class="col col-md-6">
		Follow Us <p></p>
		<a href="http://www.facebook.com/mindvis"><i class="fa fa-facebook customfb"> </i></a>
		<a href="http://plus.google.com/mindvis"><i class="fa fa-google-plus"></i></a>
		<a href="https://twitter.com/mindvisindia"><i class="fa fa-twitter"></i></a>
	</div>
</div></div></div>
        @include('partials.elements.searchmodal') <!-- Search Modal -->
  	<div id="push"></div>
	</div>
<div id="footer">
    <div class="container">
        <hr class="footer-divider">
        <p class="pull-right tp"><a href="#">Back to top</a></p>
        <p class="copy">&copy; 2015 MindVis.in </p>
    </div>
</div>
<script src="{{ elixir('js/all.js') }}"></script>
  </body>
</html>
