@extends('layouts.dashboard')

@section('dashcon')

<h3>	<i>{{$user->first_name}} {{$user->last_name}}</i> scored {{$marks}} out of {{$total_marks}} in {{$quiz->title}}.</h3>
<h5>Taken : {{$test->created_at->diffForHumans()}}</h5>

 <table class="table">
	<thead>
	    <tr>
	        <th>Question Number</th>
	        <th>Correct Answer</th>
	        <th>Attempt</th>
	        <th>Marks</th>
	    </tr>
	</thead>
	<tbody>
	    @foreach($questions as $question)
	    <?php 
	    $attempt=\App\Attempt::where('test_id',$test->id)->where('qno',$question->question_number)->first();
	    ?>
	    <tr>
	        <td>{{$question->question_number}}</td>
	        <td>{{$question->correct_answer}}</td>
	        @if($attempt)
	        <td>{{$attempt->answer}}</td>
	            @if($attempt->answer==$question->correct_answer)
	            <td>{{$question->positive_marks}}</td>
	            @else
	            <td>-{{$question->negative_marks}}</td>
	            @endif
	        @else
	            <td></td>
	            <td>0</td>
	        @endif
	    </tr>
	    @endforeach
	</tbody>
</table>

@stop