@extends('layouts.dashboard')

@section('dashcon')

 <table class="table">
	<thead>
	    <tr>
	        <th>Quiz Taken</th>
	        @if(Auth::user()->is_teacher)
	        <th>Taker</th>
	        @endif
	        <th>Taken</th>

	        <th>Actions</th>
	    </tr>
	</thead>
	<tbody>
	    @foreach($tests as $test)
	    <tr>
	        <td>{{$test->quiz->title}}</a></td>
	        @if(Auth::user()->is_teacher)
	        <td>{{$test->user->first_name}} {{$test->user->last_name}}</td>
	        @endif
	        <td>{{$test->created_at->diffForHumans()}}</td>
	        <td><a href="/quiz-report/{{$test->id}}" class="btn btn-info">Details</a>
	        <a href="/quiz/{{$test->quiz->slug}}/start" class="btn btn-warning">Start Again</a></td>
	    </tr>
	    @endforeach
	</tbody>
</table>


@stop