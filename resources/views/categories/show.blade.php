@extends ('layouts.course-index')

@section('catbutt')
<a href="/categories/{{$category->slug}}/edit" class="btn btn-primary form-control"><i class="fa fa-pencil-square-o"></i> Category</a>
<a href="/categories/{{$category->slug}}/delete" class="btn btn-danger form-control"><i class="fa fa-trash"></i> Category</a>
@stop

@section('content')
	<h3><a href="{{$category->slug}}">{{$category->title}}</a></h3>
	<p>{{$category->preview}}</p>

		@if(!$courses->isEmpty())
		<div class="row">
		@foreach($courses as $course)
		<div class="col courses-index">
			<a href="/courses/{{$course->slug}}">
			<img src="{{$course->url}}" height="150px" width="300px" alt="{{$course->title}}"><p></p>
			{{$course->title}}</a>
		</div>
		@endforeach
		</div>
		@else
		<p>No courses available for {{$category->title}}.</p>
		@endif		
@stop


		

