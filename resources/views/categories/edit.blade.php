@extends ('layouts.pages')

@section('content')

<h1>Edit a new Category</h1>
{!!Form::model($category,['url'=>'categories/'.$category->slug,'method'=>'patch'])!!}

@include('partials.forms.category')

@stop