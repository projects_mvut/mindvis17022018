@extends ('layouts.pages')

@section('content')

<h1>Add a new Category</h1>

{!!Form::open(['url'=>'categories'])!!}

@include('partials.forms.category')

@stop