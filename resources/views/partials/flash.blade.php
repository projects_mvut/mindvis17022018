@if (session('status'))
    <div class="alert alert-success notification notification-success">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p>{{ session('status') }}</p>
    </div>
@endif
@if (session('status-danger'))
    <div class="alert alert-danger notification notification-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p>{{ session('status-danger') }}</p>
    </div>
@endif
@if (session('status-alert'))
    <div class="alert alert-warning notification notification-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p>{{ session('status-alert') }}</p>
    </div>
@endif

@if (session('status-email'))
    <div class="alert alert-warning" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p>Your Email is yet to be verified.</p>
        <p>Kindly login to your email account and verify your email to login.</p>
        <p></p>
    </div>
@endif

@if (session('status-modal'))
<div class="alert subscribe-modal">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <p>{!! session('status-modal') !!}</p>
</div>
@endif