@if($errors->any())
<div class="alert alert-danger notification notification-danger">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	@foreach($errors->all() as $error)
		<p class="error-not">{{$error}}</p>
	@endforeach 
</div>
@endif