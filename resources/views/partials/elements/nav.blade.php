<header>
<!-- navbar --> 
 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top transparent navbar-inverse other " id="nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#scroll-spy" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="/images/logo.png" class="img-responsive" style="max-width:45px;"><span>MINDVIS</span></a>
        </div>
        <div id="scroll-spy" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
              <li><a href="/courses"><i class="fa fa-graduation-cap"></i> Courses</a></li>
              <li><a href="/articles/page/1"><i class="fa fa-book"></i> Blog</a></li>
              <li><a href="http://forums.mindvis.in"><i class="fa fa-forumbee"></i> Forums</a></li>
<!--               <li><a href="/gks"><i class="fa fa-newspaper-o"></i> News</a></li>
 -->




 </ul>
         <ul class="nav navbar-nav navbar-right">
              <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Search</a></li>
              @if(Auth::user())




 <li> <p class="navbar-btn">

          <a href="/dashboard" class="btn btn-default reg-btn">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
          </p>
          </li>
                  <li> <p class="navbar-btn">

          <a href="/auth/logout" class="btn btn-default login-btn">Logout</a>
          </p>
          </li>

              @else
             <li> <p class="navbar-btn">
                    <a href="/auth/register" class="btn btn-default reg-btn">Register</a>
                </p></li>
                <li> <p class="navbar-btn">
                    <a href="/auth/login" class="btn btn-default login-btn">Login</a>
                </p></li>   
              @endif
            </ul>
           
         
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</header>

<!-- 
<nav class="navbar navbar-default navbar-static-top hidden">
        <div class="container cononav">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
              <img src="/logo.png" class="logo" alt=""></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="/courses"><i class="fa fa-graduation-cap"></i> Courses</a></li>
              <li><a href="/articles/page/1"><i class="fa fa-book"></i> Blog</a></li>
              <li><a href="/forums"><i class="fa fa-forumbee"></i> Forums</a></li>
              <li><a href="/gks"><i class="fa fa-newspaper-o"></i> News</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Search</a></li>
              @if(Auth::user())
              <li><a href="/dashboard"><i class="fa fa-user"></i>  {{Auth::user()->first_name}} {{Auth::user()->last_name}}</a></li>
              <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Logout</a></li>              
              @else
              <li><a href="/auth/login"><i class="fa fa-sign-in"></i> Login</a></li>
              <li><a href="/auth/register"><i class="fa fa-users"></i> Register</a></li>
              @endif
            </ul>
          </div>
        </div>
      </nav> -->