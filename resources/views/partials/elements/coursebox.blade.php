<section class="main light-bg " id="courses">
  <div class="container">
<div class="row">
<div class="title text-center">
<h1 class="black-txt background"><span class="light-bg">Online Courses</span></h1>
</div>
  <div id="slider-carousel" class="owl-carousel">
   

@foreach($courses as $course)
				<?php
				$instructor=$course->users()->where('is_teacher',true)->first();
				$rating=\App\Review::where('course_id',$course->id)->avg('rating');
				if(!$rating){
					$rating=3.5;
				}
				?>
    
      
       <div class="item">
      <a class="hoverfx course-image" href="/courses/{{$course->slug}}" >
        <div class="overlay">
        <span class="student" style="color:#fff; display:table-cell;
 vertical-align:middle;">
								<i class="fa fa-users"></i>
								{{$course->base_users+$course->users()->where('is_teacher',false)->get()->count()}}  Learners
							</span>
        </div>	
        <img src="{{$course->url}}" alt="Course image">
      </a>
      <div class="course-title">
      <h4>{{$course->title}}
</h4>
      </div>
  <div class="course-title">
<div class="col-md-6 col-xs-6">
<div class="row text-center">
<h5>Assisted</h5>
<h4 class="red-txt">{{$course->price}}</h4>
</div>
</div>
<div class="col-md-6 col-xs-6">
<div class="row text-center">
<h5>Self Paced</h5>
<h4 class="red-txt">{{$course->price_self}}</h4>
</div>
</div>
      </div>    


      <div class="course-rating">
    						<span class="rating">
								<span class="raty" data-score="{{$rating}}"></span>
							</span>
	  
      </div>
      </div>


      				@endforeach





























  </div>
 <div class="customNavigation">
    <a class="btn gray prev"><img src="/images/new-home/back.svg" width="25"></a>
    <a class="btn gray next"><img src="/images/new-home/next.svg" width="25"></a>
  </div>
</div>
</div>
</section>






<div class="coursebox-w hidden">
	<div class="container">
		<div class="coursebox-container">
			<div class="row">
				@foreach($courses as $course)
				<?php
				$instructor=$course->users()->where('is_teacher',true)->first();
				$rating=\App\Review::where('course_id',$course->id)->avg('rating');
				if(!$rating){
					$rating=3.5;
				}
				?>
				<div class="col-md-4 col-sm-6">
					<a href="/courses/{{$course->slug}}">
						<span class="coursebox">
							<img class="headimg" src="{{$course->url}}" alt="Course image">
							<span class="title">
								{{$course->title}}
								@if($instructor)
								<span class="instructor-name">By <i>{{$instructor->first_name}} {{$instructor->last_name}}</i></span>
								@endif
							</span>
							<span class="student">
								<i class="fa fa-users"></i>
								{{$course->base_users+$course->users()->where('is_teacher',false)->get()->count()}}  Learners
							</span>
							<span class="price">
								<h4>Assisted</h4>
								@if($course->price)
								<i class="fa fa-inr"></i> {{$course->price}}
								@else
								Free
								@endif
							</span>
							<span class="price_self">
								<h4>Self Paced</h4>
								@if($course->price_self)
								<i class="fa fa-inr"></i> {{$course->price_self}}
								@else
								N.A.
								@endif
							</span>
							<span class="instructor">
								@if($instructor)
								<img class="img-circle" src="{{$instructor->url}}" height="50px" width="50px">
								@endif
							</span>
							<span class="rating">
								<span class="raty" data-score="{{$rating}}"></span>
							</span>
						</span>
					</a>
				</div>

				@endforeach
			</div>
		</div>
	</div>
</div>