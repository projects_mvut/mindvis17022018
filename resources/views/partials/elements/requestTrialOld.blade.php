<span class="btn btn-info" data-toggle="modal" data-target=".trial">Request 2 Days Trial Access</span>

<div class="modal fade trial" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" style="padding:10px;">
      <h3 style="text-align:center">Request Trial for {{$course->title}} Course</h3>
      {!! Form::open(['url'=>'courses/'.$course->slug.'/trial']) !!}

<div class="form-group">
{!! Form::label('Email') !!}
{!! Form::text('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::label('Name') !!}
{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::label('Mobile') !!}
{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
</div>
      <div class="form-group">
      {!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
    </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>