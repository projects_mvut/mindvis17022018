<div id="assignments-{{$course->slug}}" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				@if(!$assignments->isEmpty())
				<h4>Assignments for Course {{$course->title}}</h4>
				<ol>
					@foreach($assignments as $ass)
					<li><a href="{{$ass->link}}"  target="_blank">{{$ass->title}} - Created {{$ass->created_at->diffForHumans()}}</a> 
					@if(Auth::check())
					@if(Auth::user()->is_admin)
					<a href="/assignment/{{$ass->id}}/delete"><i class="fa fa-times"></i></a>
					@endif
					@endif
					</li>
					@endforeach
				</ol>
				@else
				<h4>No Assignments for Course {{$course->title}}</h4>
				@endif
				@if(Auth::check())
				@if(Auth::user()->is_admin)
				<hr>
				<h4>Create New Assignement</h4>
				{!! Form::open(['url'=>'courses/'.$course->slug.'/assignment']) !!}
				<div class="form-group">
					{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
				</div>
				<div class="form-group">
				{!! Form::text('link',null,['placeholder'=>'Link','class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
				</div>
				{!! Form::close() !!}
				@endif
				@endif
			</div>
		</div>
	</div>
</div>