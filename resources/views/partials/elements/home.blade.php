


<header>
<!-- navbar --> 
 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top transparent navbar-inverse " id="nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#scroll-spy" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="/images/logo.png" class="img-responsive"><span>MINDVIS</span></a>
        </div>
        <div id="scroll-spy" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#carousel">Home</a></li>
            <li><a href="#what-we-do">What We Do</a></li>
            <li><a href="#courses">Courses</a></li>
            <li><a href="#why-choose-us">Why Choose Us</a></li>
            <li><a href="#testimonials">Testimonials</a></li>
            <li><a href="#articles">Articles</a></li>



@if(Auth::check())
 				<li> <p class="navbar-btn">

					<a href="/dashboard" class="btn btn-default reg-btn">Dashboard</a>
					</p>
					</li>
					 				<li> <p class="navbar-btn">

					<a href="/auth/logout" class="btn btn-default login-btn">Logout</a>
					</p>
					</li>
					@else
 				<li> <p class="navbar-btn">
                    <a href="/auth/register" class="btn btn-default reg-btn">Register</a>
                </p></li>
                <li> <p class="navbar-btn">
                    <a href="/auth/login" class="btn btn-default login-btn">Login</a>
                </p></li>					
                @endif
				</div>


            <li class="dropdown hidden ">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
         
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</header>


<!-- carousel --> 
<section id="carousel" class="">
  <div id="myCarousel" class="carousel slide carousel-fade">
        <!-- Indicators -->
        <ol class="carousel-indicators hidden">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

          <!-- <li data-target="#myCarousel" data-slide-to="4"></li> -->
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <!-- First slide -->
          <div class="item active">
<div class="fullscreen-bg hidden-xs">
     <video muted style="width:100%" autoplay="" loop="" class="fillWidth fadeIn animated fullscreen-bg__video" poster="https://s3-us-west-2.amazonaws.com/coverr/poster/Traffic-blurred2.jpg" id="video-background">
    <source src="https://player.vimeo.com/external/195107125.hd.mp4?s=33f7ae216e85a9d5fa504786467ac41669657043&profile_id=174" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
</video>
</div>

            <div class="container">
              <div class="carousel-caption">
                <h1 class="toggleHeading">Augment Learning With Our Effective & Efficient Online Courses
</h1>
                <p class="toggleCaption"></p>
                <div class="btn">
<a href="/courses" class="btn btn-default main-btn">Start a Course</a>

                </div>
                <div class="scroll-down">
                  <a class="btn-circle page-scroll btn-lg btn btn-default" href="#courses">
                  <i class="fa fa-angle-down fa-6"></i>
                  </a>
                </div>
                <!--<p><a class="btn btn-lg btn-primary page-scroll" href="#services" role="button">Sign up today</a></p>-->
              </div>
            </div>
          </div>
          <!-- Fourth slide 1512x450 -->
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</section>
<section class="main white-bg " id="what-we-do">
  <div class="container">
<div class="row">
<div class="title text-center">
<h1 class="black-txt background"><span>What We Do</span></h1>
          <h5 class="grey-txt text-center">We bring efficient, disciplined and dedicated learning by providing well researched online courses from our expert faculties.</h5>

</div>
<div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="/images/new-home/internet.png" width="75">
              </div>
              <h3 class="pad-bt15 hidden">Heading Text</h3><br>
              <p>Choose to learn from wherever and whenever you like.</p>
            </div>
          </div>
<div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="/images/new-home/student.png" width="75">
              </div>
              <h3 class="pad-bt15 hidden">Heading Text</h3><br>
              <p>Full mentor support system with HQ video content.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="/images/new-home/play-button.png" width="75">
              </div>
              <h3 class="pad-bt15 hidden">Heading Text</h3><br>
              <p>Choose between self paced and assisted learning.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="images/new-home/professor.png" width="75">
              </div>
              <h3 class="pad-bt15 hidden">Best Faculty</h3><br>
              <p>Get full study planning and monitoring support from best faculty.</p>
            </div>
          </div>

</div>
  </div>
</section>

	@include('partials.elements.coursebox')

<section class="main white-bg " id="why-choose-us">
  <div class="container">
<div class="row">
<div class="title text-center">
<h1 class="black-txt background"><span>Why Choose MindVis?</span></h1>
</div>
<div class="col-sm-6  col-md-6 col-xs-12"  id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">
<img src="/images/new-home/teacher.png" width="80" class="shadowfilter">
</div>
<div class="col-sm-9 col-md-9 col-xs-9">
<p>
<a class="tit" target="_self" href="#">Experience 1 to 1 teacher-student engagement</a>
<br>
</p>
<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>
</div>
<div class="col-xs-12 visible-xs">&nbsp;</div>
<div class="col-sm-6  col-md-6 col-xs-12" id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">
<img class="shadowfilter" src="/images/new-home/planning.png" width="80">
</div>
<div class="col-sm-9 col-md-9 col-xs-9">
<p>
<a class="tit" target="_self" href="#">Dedicated and disciplined courses with complete study plan and time table.</a>
<br>
</p>
<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>


<div class="col-md-12 hidden-xs "><div class="spacer">&nbsp;</div></div>


<div class="col-sm-6  col-md-6 col-xs-12" id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">

<img class="shadowfilter" src="/images/new-home/computer.png" width="80">
</div>

<div class="col-sm-9 col-md-9 col-xs-9">

<p>
<a class="tit" target="_self" href="#">Proper tracking procedure for the learner's progress</a>
<br>
</p>

<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>

<div class="col-xs-12 visible-xs">&nbsp;</div>

<div class="col-sm-6  col-md-6 col-xs-12" id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">

<img class="shadowfilter" src="/images/new-home/distance-courses.png" width="80">
</div>

<div class="col-sm-9 col-md-9 col-xs-9">

<p>
<a class="tit" target="_self" href="#">Courses are offered with LIVE sessions and complete mentorship in Assisted courses.</a>
<br>
</p>

<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>



<div class="col-md-12 hidden-xs "><div class="spacer">&nbsp;</div></div>



<div class="col-sm-6  col-md-6 col-xs-12" id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">

<img class="shadowfilter" src="/images/new-home/support.png" width="80">
</div>

<div class="col-sm-9 col-md-9 col-xs-9">

<p>
<a class="tit" target="_self" href="#">24 X 7 support from the team</a>
<br>
</p>

<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>

<div class="col-xs-12 visible-xs">&nbsp;</div>

<div class="col-sm-6  col-md-6 col-xs-12" id="t-1">
<div class="row">
<div class="col-sm-3 col-md-3 col-xs-3">

<img class="shadowfilter" src="/images/new-home/focus.png" width="80">
</div>

<div class="col-sm-9 col-md-9 col-xs-9">

<p>
<a class="tit" target="_self" href="#">You focus on your studies, rest we take care for you</a>
<br>
</p>

<div class="" >
    <div>
    <p class="moto-text_normal hidden">Office computer repair and business IT service at its best. We provide timely business computer. <br></p>
    </div>
</div>
</div>
</div>
<div>
    <div class="moto-widget-spacer-block" style="height: 15px;"></div>
</div>
</div>


</div>
</div>
</section>



<section class=" separate " id="testimonials">
<div class="bg-img-1">
<div class="container">
<div class="row">
 <div id="testimonials-slider" class="carousel slide">
            <ol class="carousel-indicators hidden">
              <li data-target="#testimonials-slider" data-slide-to="0" class="active"></li>
              <li data-target="#testimonials-slider" data-slide-to="1" class=""></li>
            </ol>
            <!-- Carousel items-->
            <div class="carousel-inner">
              <!-- Testimonial 1-->
              <div class="item active">

                  <p class="lead">             
Great way to prepare for IBPS exams. A must have for all the IBPS exam aspirants.
</p>

                  <cite><img src="/images/new-home/test1.png" class="img-responsive img-circle img-thumbnail" width="50" style="margin:0 auto">&nbsp;Jasjeet Sandhu, 							<span class="raty" data-score="5"></span>
</cite>
              </div>
              <!-- Testimonial 2-->
              <div class="item">
                  <p class="lead">I feel lucky to have found this course for my GATE prep. It is THE Best Online GATE Preparation for Mechanical Engineering. Helped me secure 67.2 marks in GATE 2016.</p>
                  <cite><img src="/images/new-home/test5.png" class="img-responsive img-circle img-thumbnail" width="50" style="margin:0 auto">&nbsp;Ashish Chopra, 							<span class="raty" data-score="5"></span>
</cite>
                </div>
                 <div class="item">
                  <p class="lead">A great course for preparing for IBPS exams! The videos are awesome and so is the mentor support system. The study plan actually works.</p>
                  <cite><img src="/images/new-home/test3.png" class="img-responsive img-circle img-thumbnail" width="50" style="margin:0 auto">&nbsp;Priyanka Vohra, 							<span class="raty" data-score="5"></span>
</cite>
                </div>
                
                 <div class="item">
                  <p class="lead">Truly India's Best online course for GATE Mechanical Engineering. Thanks to the course and the experienced instructor, I was able to score 66.06 marks in my first attempt!</p>
                  <cite><img src="/images/new-home/test6.png" class="img-responsive img-circle img-thumbnail" width="50" style="margin:0 auto">&nbsp;Gaganpreet Singh Hundal, 							<span class="raty" data-score="5"></span>
</cite>
                </div>
              
              
            </div>
            <!-- Carousel navigation arrows--><a href="#testimonials-slider" data-slide="prev" class="carousel-control fui-arrow-left left"><i class="fa fa-caret-left"></i></a><a href="#testimonials-slider" data-slide="next" class="carousel-control fui-arrow-right right"><i class="fa fa-caret-right"></i></a>
          </div>
          <!-- ////////// end of Carousel Slider //////////-->
</div>
</div>
</div>
</section>




<section class="main white-bg " id="articles">
  <div class="container">
<div class="row">
<div class="title text-center">
<h5 class="grey-txt hidden">Must Read Articles</h5>
<h1 class="black-txt background"><span>Latest Articles</span></h1>
</div>


       @foreach($articles as $article)
        <div class="col-lg-4 col-sm-4 portfolio-item">
          <div class=" h-100">
            <a href="/articles/{{$article->slug}}"><img class="card-img-top img-responsive" src="{{$article->url}}"  alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">{{$article->title}}</a>
              </h4>
             
            </div>
          </div>
        </div>
				
				@endforeach

       

 



</div>
</div>
</section>

<section class="" style="background-color:#ededec">
  <div  class="container-fluid">
  <div class="row">
<div class="col-md-6 col-sm-6">
      <div class="row">
       <div class="card left text-center">
       <h5>Our co-brand</h5>
        <h4 class="wow fadeIn" data-wow-offset="200" style="visibility: visible; animation-name: fadeIn;">       <img src="/images/new-home/head.png" class="img-responsive" style="margin:0 auto; max-width:150px;">
</h4>
      </div>
    </div>
  </div>

<div class="col-md-6 col-sm-6">
      <div class="row">
       <div class="card right text-center">
        <h4 class="wow fadeIn" data-wow-offset="200" style="visibility: visible; animation-name: fadeIn;">A platform where we love and encourage inquisitiveness, with a mission to foster strong learner - educator  community.     <br>  <a href="https://geekvis.com" class="btn btn-default btn-geekvis" target="_blank">Sign Up</a>
</h4>

      </div>
    </div>
  </div>


</div>
</div>
</section>



<div id="home" class="hidden">
	<div class="first hidden">
		<div class="navig">
			<div class="row">
				<div class="col col-sm-4"><img src="/images/l.png" alt=""></div>
				<div class="col col-sm-4"></div>
				<div class="col col-sm-4 inn">
					@if(Auth::check())
					<a href="/dashboard" class="btn btn-info">Dashboard</a>
					<a href="/auth/logout" class="btn btn-danger">Logout</a>
					@else
					<a href="/auth/register" class="btn btn-success">Register</a>
					<a href="/auth/login" class="btn btn-info">Login</a>
					@endif
				</div>
			</div>
		</div>
		<div class="container inn">
			<h4>Augment Learning With Our Effective & Efficient</h4>
			<h2> ONLINE COURSES.</h2>
			<a href="/courses" class="btn btn-warning btn-lg">Start a Course</a>
		</div>
	</div>
	@include('partials.elements.coursebox')
	<div class="rib">Join The Community of 10,000+ Successful Learners.</div>
	<div class="reviews">
		<div class="container">
			<div class="row">
				<div class="col col-sm-6">
					<div class="testimonials t1">
						<div class="online_image"><img src="/images/testimonials1.jpg" alt=""></div>
						<div class="testimonials_content">
							<h2>Jasjeet Sandhu</h2>
							<span class="raty" data-score="5"></span>
							<p>Great way to prepare for IBPS exams. A must have for all the IBPS exam aspirants. </p>

						</div>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="testimonials t2">
						<div class="online_image"><img src="https://mindvis.in/uploads/images/ashish-chopra-vGhHHmn2.jpg" alt=""></div>
						<div class="testimonials_content">
							<h2>Ashish Chopra</h2>
							<span class="raty" data-score="5"></span>
							<p>I feel lucky to have found this course for my GATE prep. It is THE Best Online GATE Preparation for Mechanical Engineering. Helped me secure 67.2 marks in GATE 2016. Thank you to the team behind this awesome course. Awesome tutor support.</p>

						</div>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="testimonials t3">
						<div class="online_image"><img src="/images/testimonials3.jpg" alt=""></div>
						<div class="testimonials_content">
							<h2>Priyanka Vohra</h2>
							<span class="raty" data-score="5"></span>
							<p>A great course for preparing for IBPS exams! The videos are awesome and so is the mentor support system. The study plan actually works.</p>

						</div>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="testimonials t4">
						<div class="online_image"><img src="https://mindvis.in/uploads/images/gaganpreet-hundal-XJoTzpyJ.jpeg" alt=""></div>
						<div class="testimonials_content">
							<h2>Gaganpreet Singh Hundal</h2>
							<span class="raty" data-score="5"></span>
							<p>Truly India's Best online course for GATE Mechanical Engineering. Thanks to the course and the experienced instructor, I was able to score 66.06 marks in my first attempt!</p>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="articles">
		<div class="container">
			<h2>Latest Articles</h2>
			<div class="row">
				@foreach($articles as $article)
				<div class="col col-sm-4">
					<a href="/articles/{{$article->slug}}">
						<span class="inn">
							<img src="{{$article->url}}" alt="">
							<h4>{{$article->title}}</h4>
						</span>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="benefits">
		<div class="container">
			<h3>Why you must choose MindVis for your learning needs?</h3>
			<div class="row">
				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q1.png" alt="">
						Experience 1 to 1 teacher-student engagement
					</span>
				</div>
        <div class="clearfix"></div>
				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q2.png" alt="">
						Dedicated and disciplined courses with complete study plan and time table.
					</span>
				</div>
                <div class="clearfix"></div>

				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q3.png" alt="">
						Proper tracking procedure for the learner's progress
					</span>
				</div>
                <div class="clearfix"></div>

				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q4.png" alt="">
						Courses are offered with LIVE sessions and complete mentorship in Assisted courses.
					</span>
				</div>
				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q5.png" alt="">
						24 X 7 support from the team
					</span>
				</div>
				<div class="col col-sm-4 col-xs-6">
					<span class="inn">
						<img src="/images/q6.png" alt="">
						You focus on your studies, rest we take care for you
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="publishers">
		<div class="container">
					<h3>We bring efficient, disciplined and dedicated learning by providing well researched online courses from our expert faculties.</h3>
						<div class="col col-sm-3 col-xs-6 publishers_area">
							<div class="services_area"><i class="fa fa-paper-plane"></i></div>
							<p>Choose to learn from wherever and whenever you like. </p>
						</div>
						<div class="col col-sm-3 col-xs-6 publishers_area">
							<div class="services_area"><i class="fa fa-video-camera"></i></div>
							<p>Full mentor support system with HQ video content.</p>
						</div>
						<div class="col col-sm-3 col-xs-6 publishers_area">
							<div class="services_area"><i class="fa fa-graduation-cap"></i></div>
							<p>Choose between self paced and assisted learning. </p>
						</div>
						<div class="col col-sm-3 col-xs-6 publishers_area">
							<div class="services_area"><i class="fa fa-life-ring"></i></div>
							<p>Get full study planning and monitoring support from best faculty. </p>
						</div>

		</div>
	</div>
</div>