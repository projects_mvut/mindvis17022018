<div class="container cono">
    <div class="col-md-12 cono">
        <div id="carousel-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <img class="first-slide img-responsive" src="/images/slider/1.jpeg" alt="First slide">
                    <div class="carousel-caption">
                        <h3 class="car-head">Online Courses</h3>
                        <p>Study online </p><p>
                            Augment learning with our effective & efficient online courses.</p>
                    </div>
                </div>
                <div class="item">
                    <img class="second-slide img-responsive" src="/images/slider/2.jpeg" alt="Second slide">
                    <div class="carousel-caption">
                        <h3 class="car-head">Multi-Media Units</h3>
                        <p>Audio, Video playback </p>
                        <p>Feel the power of technology, in education. Better delivery and higher retention guaranteed!.</p>
                    </div>
                </div>
                <div class="item">
                    <img class="third-slide img-responsive" src="/images/slider/3.jpeg" alt="Third slide">
                    <div class="carousel-caption">
                        <h3 class="car-head">Subscribe Courses</h3>
                            <p>Buy Courses</p>
                            <p>Whether you're a student or a teacher, you can always find something to learn here!</p>
                    </div>
                </div>
            </div>
                    <a class="left carousel-control" href="#carousel-generic" data-slide="prev"></a>
                    <a class="right carousel-control" href="#carousel-generic" data-slide="next"></a>
        </div>
        <div class="main-text">
            <div class="row">
            <div class="inav">
            <ul>
                <li class="ibrand"><a href="/">
              <img src="/images/logo.png" class="ilogo" alt=""></a></li>
                <div class="fr">
                  @if(Auth::user())                               
                  <li><a href="/dashboard"><i class="fa fa-user"></i>  {{Auth::user()->first_name}} {{Auth::user()->last_name}}</a></li>
                  <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Logout</a></li>              
                  @else
                  <li><a href="/auth/login"><i class="fa fa-sign-in"></i> Login</a></li>
                  <li><a href="/auth/register"><i class="fa fa-users"></i> Register</a></li>
                  @endif
              </div>
            </ul>
          
          </div></div>
            <div class="col-sm-6 text-center search-bar col-sm-offset-3">                
                <form action="/search" method="get">
                    <div class="input-group">
                        <input type="query" class="form-control" placeholder="Search Courses" id="query" name="query" value="">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-search">
                                <i class="fa fa-search" style="font-size: 17px;
"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>