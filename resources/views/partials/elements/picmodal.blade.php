<div id="PicModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Profile Pic</h4>
      </div>
      <div class="modal-body">
      {!!Form::open(['url'=>'user/pic','files' => true])!!}
        <div class="form-group">
          {!! Form::label('Image') !!}
          {!! Form::file('url', null,['class'=>'btn btn-primary']) !!}
        </div>
        <div class="form-group">
          <input class="btn btn-primary" type="submit">
        </div>
      {!!Form::close()!!}
    </div>        
  </div>
</div>
</div>