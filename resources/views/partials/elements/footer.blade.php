<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h1>MindVis C/O BeyondEngg </h1>
				<p>F-452, Industrial Area, Sector 91, Mohali - Punjab (INDIA)</p>
				<div class="footer_links">
					<a href="/vision">Vision </a>
					<a href="/about-us">Team</a>
					<a href="/helpdesk">Help Desk</a>
					<a href="/privacy">Privacy Policy</a>
					<a href="/terms">Terms and Conditions</a>
				</div>
				<h2> +91 9779434433 </h2>
				<h1>Connect With MindVis</h1>
				<div class="social_links">
					<a href="https://www.facebook.com/mindvis"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/mindvisindia"><i class="fa fa-twitter"></i></a>
					<a href="mailto:info@mindvis.in"><i class="fa fa-envelope"></i></a>
					<a href="https://www.youtube.com/channel/UCcZ9l294MgHN3d4P3_f45Jw" ><i class="fa fa-youtube-play"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="footer2">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<p>© 2017 MindVis.in </p>
			</div>
		</div>
	</div>
</div>