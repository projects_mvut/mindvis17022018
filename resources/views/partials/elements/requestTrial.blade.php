<div class="requestTrial">
		<div class="col col-sm-6 col-xs-1"></div>
		<div class="col col-sm-4 col-xs-10 coco">
			<h3>Request Trial Access</h3>
			{!! Form::open(['url'=>'courses/'.$course->slug.'/trial']) !!}

			<div class="form-group">
				{!! Form::label('Email') !!}
				{!! Form::text('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('Name') !!}
				{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('Mobile') !!}
				{!! Form::text('mobile',null,['placeholder'=>'Mobile','class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::submit('Submit',['class'=>'btn','name'=>"submit"]) !!}
			</div>
			{!! Form::close() !!}
		</div>
</div>