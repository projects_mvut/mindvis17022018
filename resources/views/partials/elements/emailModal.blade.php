<div class="modal fade emailstudents" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<h3 style="text-align:center">Email to {{$course->title}} students</h3>
			{!! Form::open(['url'=>'courses/'.$course->slug.'/emailstudents','files'=>true]) !!}
			<div class="form-group">
			{!! Form::textarea('content',null,['placeholder'=>'Enter the content','class'=>'form-control tinytext']) !!}
			</div>
			<div class="row">
			<div class="form-group col col-md-3 col-md-offset-1">
					{!! Form::label('File') !!}
					{!! Form::file('file', null,['class'=>'btn btn-primary']) !!}
			</div></div>
			<div class="containerado">
				
			<label class="radio-inline"><input type="radio" name="radio" value="2" checked>All Users</label>
			<label class="radio-inline"><input type="radio" name="radio" value="1">Active Users</label>
			<label class="radio-inline"><input type="radio" name="radio" value="0">Inactive Users</label>
			</div>
			<div class="form-group">
			{!! Form::submit('Submit',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
		</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
<script>tinymce.init({selector:".tinytext",theme:"modern",plugins:["advlist autolink lists link image charmap print preview hr anchor pagebreak","searchreplace wordcount visualblocks visualchars code fullscreen","insertdatetime media nonbreaking save table contextmenu directionality","emoticons template paste textcolor colorpicker textpattern imagetools"],toolbar1:"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",toolbar2:"print preview media | forecolor backcolor emoticons",image_advtab:!0});</script>