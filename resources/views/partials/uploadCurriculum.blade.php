<div id="uploadCurriculum-{{$course->slug}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

{!!Form::open(['url'=>'courses/'.$course->slug.'/uploadCurriculum','files' => true])!!}

<div class="form-group">
    {!! Form::label('Upload Curriculum') !!}
    {!! Form::file('curriculum', null,['class'=>'btn btn-info']) !!}
</div>

<div class="form-group">
	{!! Form::label('Slug Affix') !!}
	{!! Form::text('affix',null,['placeholder'=>'new- (- is added automatically)','class'=>'form-control']) !!}
</div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Upload Schedule</button>
    </div>
</form>

</div>
</div>
</div>
</div>
