<div class="newsletter">
	<div class="closeNewsletter"><i class="fa fa-times"></i></div>
	<h3>Subscribe to Our Newsletter</h3>
	<form method="POST" action="/newsletter">
		{!! csrf_field() !!}


		<div class="form-group">
			<label>Email</label>
			<input placeholder="Email Adress" class="form-control" type="email" name="email" value="{{ old('email') }}">
		</div>

		<div class="form-group">
			<label>Name</label>
			<input placeholder="Name" class="form-control" type="name" name="name" value="{{ old('name') }}">
		</div>

		<div class="form-group">
			<label>Contact</label>
			<input placeholder="Contact" class="form-control" name="contact" value="{{ old('contact') }}">
		</div>

		<div class="form-group">
			<label>Exam Preparing for ?</label>
			@if(isset($course))
			<input placeholder="Exam Preparing for ?" class="form-control" type="course" name="course" value="{{ $course->title }}">
			@else
			<input placeholder="Exam Preparing for ?" class="form-control" type="course" name="course" value="{{ old('course') }}">
			@endif
		</div>
		
		<div class="form-group">
			<button class="btn btn-primary" type="submit"><b>Submit </b></button> 
		</div>
	</form>
</div>