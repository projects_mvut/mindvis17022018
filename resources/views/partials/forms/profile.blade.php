{!!Form::model($user,['url'=>'user/'.$user->username,'method'=>'patch','files' => true])!!}


<div class="form-group">
{!! Form::text('first_name',null,['placeholder'=>'First Name','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('last_name',null,['placeholder'=>'Last Name','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('username',null,['placeholder'=>'Username','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('address1',null,['placeholder'=>'Address','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('city',null,['placeholder'=>'City','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('state',null,['placeholder'=>'State','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('pin',null,['placeholder'=>'Pin Code','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('address2',null,['placeholder'=>'Enter University/College Name','class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Image') !!}
    {!! Form::file('url', null,['class'=>'btn btn-primary']) !!}
</div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary form-control">Update</button>
    </div>
</form>
