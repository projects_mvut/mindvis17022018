@section('headinclude')
@include('partials.forms.tinymce')
@stop

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-3">
{!! Form::text('slug',null,['placeholder'=>'Slug','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-3">
{!! Form::text('sequence',null,['placeholder'=>'Sequence No.','class'=>'form-control']) !!}
</div>
</div>

<div class="form-group">
{!! Form::text('preview',null,['placeholder'=>'Preview','class'=>'form-control']) !!}
</div>


<div class="row">
<div class="form-group col col-md-3">
    {!! Form::label('Image') !!}
    {!! Form::file('url', null,['class'=>'btn btn-primary']) !!}
</div>
<div class="form-group col col-md-3">
    {!! Form::label('Public') !!} <br>
	{!! Form::checkbox('is_public') !!}
</div>
</div>

<div class="form-group">
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Enter the content','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Publish Now',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!} <p></p>
	{!! Form::submit('Publish Later',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@include('partials.errors')
