<h3>Add a Forum</h3>
{!!Form::open(['url'=>'forums'])!!}

<div class="form-group">
{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Enter the content','class'=>'form-control']) !!}
</div>
@if(\Auth::check())
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
</div>
@else
<a href="/auth/login" class="btn btn-success">Login to Submit Forum</a>
@endif
{!!Form::close()!!}
@include('partials.errors')