<div class="inputrat">
Rating : <span class="inputraty" data-score="0"></span>
</div>
<input name="rating" type="hidden" value="">
<div class="form-group">
{!! Form::textarea('content',null,['placeholder'=>'Enter Your Reviews','class'=>'form-control reviewcon']) !!}
</div>

@if(Auth::check())
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary form-control','name'=>"submit"]) !!}
</div>
@else
<a href="/auth/login" class="btn btn-success">Login To Review</a>
@endif
{!!Form::close()!!}
@include('partials.errors')