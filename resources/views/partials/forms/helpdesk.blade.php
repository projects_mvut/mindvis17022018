<div class="form-group">
{!! Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::text('contact',null,['placeholder'=>'Contact No.','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::email('email',null,['placeholder'=>'Email','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::textarea('content',null,['placeholder'=>'Text','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary form-control','name'=>"submit"]) !!}
</div>

{!!Form::close()!!}
@include('partials.errors')
</div>