@section('headinclude')
@include('partials.forms.tinymce')
@stop

<div class="form-group">
	{!! Form::text('subject',null,['placeholder'=>'Enter the Subject','class'=>'form-control']) !!}
</div>


<div class="form-group">
	{!! Form::label('content','Content') !!}
	{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Enter the content','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Save',['class'=>'btn btn-primary','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@include('partials.errors')
