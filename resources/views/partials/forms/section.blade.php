@section('headinclude')
@include('partials.forms.tinymce')
@stop

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('slug',null,['placeholder'=>'Slug','class'=>'form-control']) !!}
</div>
</div>

<div class="form-group">
{!! Form::text('preview',null,['placeholder'=>'Preview','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Content','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>

{!!Form::close()!!}
@include('partials.errors')