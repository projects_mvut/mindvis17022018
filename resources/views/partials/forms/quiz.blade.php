<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('slug',null,['placeholder'=>'Slug','class'=>'form-control']) !!}
</div>
</div>

<div class="row">
<div class="form-group col col-sm-9">
{!! Form::text('preview',null,['placeholder'=>'Preview','class'=>'form-control']) !!}
</div>

<div class="form-group col col-sm-3">
    {!! Form::label('Public') !!} <br>
	{!! Form::checkbox('is_public') !!}
</div>
</div>

{!! Form::label('Timer') !!}
<div class="row">
	<div class="col col-md-4 form-group">
		{!! Form::text('hours',null,['placeholder'=>'Hours','class'=>'form-control']) !!}
	</div>
	<div class="col col-md-4 form-group">
		{!! Form::text('minutes',null,['placeholder'=>'Minutes','class'=>'form-control']) !!}
	</div>
	<div class="col col-md-4 form-group">
		{!! Form::text('seconds',null,['placeholder'=>'Seconds','class'=>'form-control']) !!}
	</div>
</div>


<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>

{!!Form::close()!!}
@include('partials.errors')