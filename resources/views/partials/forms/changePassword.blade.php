{!!Form::model($user,['url'=>'user/'.$user->username.'/password','method'=>'patch'])!!}


<div class="form-group">
{!! Form::password('password',['placeholder'=>'New Password','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::password('password_confirmation',['placeholder'=>'Confirm New Password','class'=>'form-control']) !!}
</div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary form-control">Update</button>
    </div>
</form>
