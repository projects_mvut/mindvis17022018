<div id="QuizcsvModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Quiz Csv</h4>
      </div>
      <div class="modal-body">
      {!!Form::open(['url'=>'addcsv/'.$quiz->slug,'files' => true])!!}
      {!! Form::label('Quiz Csv') !!}
      {!! Form::file('quiz_csv', null,['value'=>'Upload Quiz CSV']) !!}
        <div class="form-group">
          <input class="btn btn-primary" type="submit" style="margin-top: 20px;">
        </div>
      {!!Form::close()!!}
    </div>        
  </div>
</div>
</div>