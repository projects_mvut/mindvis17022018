@section('headinclude')
@include('partials.forms.tinymce')
@stop

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('slug',null,['placeholder'=>'Slug','class'=>'form-control']) !!}
</div>
</div>

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('seo_title',null,['placeholder'=>'MindVis | ...Seo Title Here...','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('keywords',null,['placeholder'=>'Enter the Keywords','class'=>'form-control']) !!}
</div>
</div>
<div class="form-group">
{!! Form::text('description',null,['placeholder'=>'Enter the Description','class'=>'form-control']) !!}
</div>

<div class="row">
<div class="form-group col col-md-4">
{!! Form::text('base_users',null,['placeholder'=>'Base Users','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-4">
{!! Form::text('curriculum_color',null,['placeholder'=>'Curriculum Color','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-4">
{!! Form::text('curriculum_text_color',null,['placeholder'=>'Curriculum Text Color','class'=>'form-control']) !!}
</div>
</div>

<div class="form-group">
{!! Form::text('preview',null,['placeholder'=>'Preview','class'=>'form-control']) !!}
</div>
<div class="form-group">
{!! Form::text('demo_video',null,['placeholder'=>'Demo Video Url','class'=>'form-control']) !!}
</div>

<div class="row">
<div class="form-group col col-md-6">
    {!! Form::label('No. of Videos') !!}
    {!! Form::text('no_of_video',null,['placeholder'=>'No. of Videos','class'=>'form-control']) !!}
</div>

<div class="form-group col col-md-6">
    {!! Form::label('No. of Quizzes') !!}
    {!! Form::text('no_of_quiz',null,['placeholder'=>'No. of Sectional Tests','class'=>'form-control']) !!}
</div>
</div>

<div class="row">
<div class="form-group col col-md-6">
    {!! Form::label('No. of Live Sessions') !!}
    {!! Form::text('no_of_live_session',null,['placeholder'=>'No. of Live Sessions','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
    {!! Form::label('No. of Mock Tests') !!}
    {!! Form::text('no_of_mocks',null,['placeholder'=>'No. of Mock Tests','class'=>'form-control']) !!}
</div>
</div>

<div class="row">
	
<div class="form-group col col-md-3">
    {!! Form::label('Notes') !!}
    <label class="radio-inline"> {!! Form::radio('notes', 'Yes',true) !!} Yes</label>
    <label class="radio-inline"> {!! Form::radio('notes', 'No') !!} No </label>
</div>
<div class="form-group col col-md-3">
    {!! Form::label('DVDs') !!}
    <label class="radio-inline"> {!! Form::radio('dvds', 'Yes',true) !!} Yes</label>
    <label class="radio-inline"> {!! Form::radio('dvds', 'No') !!} No </label>
</div>
<div class="form-group col col-md-3">
    {!! Form::label('Trial Allowed') !!}
    <label class="radio-inline"> {!! Form::radio('trial', 1) !!} Yes</label>
    <label class="radio-inline"> {!! Form::radio('trial', 0) !!} No </label>
</div>

</div>

<div class="row">
<div class="form-group col col-md-6">
{!! Form::label('Category') !!}
{!! Form::select('category_id',$select_category,null , ['class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-3">
    {!! Form::label('Banner Image') !!}
    {!! Form::file('banner', null,['value'=>'Upload Banner']) !!}
</div>
<div class="form-group col col-md-3">
    {!! Form::label('Image') !!}
    {!! Form::file('url', null,['class'=>'btn btn-primary']) !!}
</div>
</div>

<div class="form-group">
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Content','class'=>'form-control']) !!}
</div>
    {!! Form::label('Assisted') !!}
<div class="row">
	<div class="col col-xs-5 col-sm-3 cprice">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-inr"></i></div>
			{!! Form::text('price',null,['placeholder'=>'Assisted Course Price','class'=>'form-control']) !!}
		</div>
	</div>

	<div class="form-group col col-xs-9 col-sm-9">	
		{!! Form::text('instamojo_button',null,['placeholder'=>'Instamojo Payment Slug for Assisted Course','class'=>'form-control']) !!}
	</div>
</div>
    {!! Form::label('Self Based') !!}
<div class="row">
    <div class="col col-xs-5 col-sm-3 cprice">
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-inr"></i></div>
            {!! Form::text('price_self',null,['placeholder'=>'Self Based Course Price','class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group col col-xs-9 col-sm-9">  
        {!! Form::text('instamojo_self',null,['placeholder'=>'Instamojo Payment Slug for Self Based Course','class'=>'form-control']) !!}
    </div>
</div>
<div class="form-group">
	{!! Form::submit('Publish Now',['class'=>'btn btn-primary','name'=>"submit"]) !!}
	{!! Form::submit('Publish Later',['class'=>'btn btn-default','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@include('partials.errors')





