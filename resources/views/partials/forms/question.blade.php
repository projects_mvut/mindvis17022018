@section('headinclude')
@include('partials.forms.tinymce')

@stop

<div class="row">
<div class="form-group col col-md-6">
{!! Form::label('Question Number') !!}
{!! Form::text('question_number',null,['placeholder'=>'Enter the Question Number','class'=>'form-control']) !!}
</div>

<div class="form-group col col-md-6">
    {!! Form::label('title','Title') !!}
    {!! Form::text('title',null,['placeholder'=>'Enter the title','class'=>'form-control']) !!}
</div>
</div>


<div class="form-group">
{!! Form::label('content','Content') !!}
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Enter the content','class'=>'form-control','id'=> 'mytextarea']) !!}
</div>

<div id="preview"></div>

<div class="row">
<div class="form-group col col-md-6">
    {!! Form::label('Correct Answer') !!}
    {!! Form::text('correct_answer',null,['placeholder'=>'Enter the Correct Answer or Option 1,2,3 or 4 for MCQ','class'=>'form-control']) !!}
</div>

<div class="form-group col col-md-2">
    {!! Form::label('Positive Marks') !!}
    {!! Form::text('positive_marks',null,['placeholder'=>'Positive Marks','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-2">
    {!! Form::label('Negative Marks') !!}
    {!! Form::text('negative_marks',null,['placeholder'=>'Negative Marks','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-2">
    {!! Form::label('Type') !!} <p></p>
    <label class="radio-inline"> {!! Form::radio('type', 'mcq',true) !!} MCQ</label>
    <label class="radio-inline"> {!! Form::radio('type', 'fil') !!} Fillups </label>
</div>
</div>

<div class="row options">
<div class="form-group col col-md-6">
{!! Form::text('option1',null,['placeholder'=>'A.','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('option2',null,['placeholder'=>'B.','class'=>'form-control']) !!}
</div>

<div class="form-group col col-md-6">
{!! Form::text('option3',null,['placeholder'=>'C.','class'=>'form-control']) !!}
</div>

<div class="form-group col col-md-6">
{!! Form::text('option4',null,['placeholder'=>'D.','class'=>'form-control']) !!}
</div>

</div>

<div class="form-group">
{!! Form::label('Solution') !!}
{!! Form::textarea('solution',null,['id'=>'solution','placeholder'=>'Enter the solution','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>



{!!Form::close()!!}
@include('partials.errors')
<script>
setInterval(function(){
    $('#preview').html(tinyMCE.get('mytextarea').getContent());
     $(".math").each(function() {
        var texTxt = $(this).text();
        el = $(this).get(0);
        try {
            katex.render(texTxt, el);
        }
        catch(err) {
            $(this).html("<span class='err'>"+err);
        }
    });
}, 2000);
</script>