<form method="POST" action="/auth/register">
    {!! csrf_field() !!}
<div class="row">
    <div class="form-group col col-md-6">
        <input placeholder="First Name" class="form-control authinput" type="text" name="first_name" value="{{ old('name') }}">
    </div>

    <div class="form-group col col-md-6">
        <input placeholder="Last Name" class="form-control authinput" type="text" name="last_name" value="{{ old('name') }}">
    </div>
</div>
    <div class="form-group">
        <input placeholder="Userame" class="form-control authinput" type="text" name="username" value="{{ old('name') }}">
    </div>

    <div class="form-group">
        <input placeholder="Email Address" class="form-control authinput" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">        
        <input placeholder="Password" class="form-control authinput" type="password" name="password">
    </div>

    <div class="form-group">        
        <input placeholder="Confirm Password" class="form-control authinput" type="password" name="password_confirmation">
    </div>

    <div class="g-recaptcha" data-sitekey="6Ld1nCkUAAAAAImOiUAlu-53gHZMnmmoUu6xMPuV"></div><p></p>

    <div class="form-group">
        <button class="btn btn-primary" type="submit"><b>Register</b></button>
    </div>
</form>
@include('partials.errors')
