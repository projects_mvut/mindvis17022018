@section('headinclude')
@include('partials.forms.tinymce')
@stop

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('title',null,['placeholder'=>'Enter the Title','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('slug',null,['placeholder'=>'Enter the Slug','class'=>'form-control']) !!}
</div>
</div>

<div class="row">
<div class="form-group col col-md-6">
{!! Form::text('seo_title',null,['placeholder'=>'MindVis | ...Seo Title Here...','class'=>'form-control']) !!}
</div>
<div class="form-group col col-md-6">
{!! Form::text('keywords',null,['placeholder'=>'Enter the Keywords','class'=>'form-control']) !!}
</div>
</div>
<div class="form-group">
{!! Form::text('description',null,['placeholder'=>'Enter the Description','class'=>'form-control']) !!}
</div>



<div class="form-group">
{!! Form::text('preview',null,['placeholder'=>'Preview','class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::label('Category') !!}
{!! Form::select('blog_id',$select_category,null , ['class'=>'form-control']) !!}
</div>

<div class="row">
<div class="form-group col col-md-6">
    {!! Form::label('Image') !!}
    {!! Form::file('url', null,['class'=>'btn btn-primary']) !!}
</div>
<div class="form-group col col-md-6">
    {!! Form::label('Banner Image') !!}
    {!! Form::file('banner', null,['class'=>'btn btn-primary']) !!}
</div>
</div>


<div class="form-group">
{!! Form::label('content','Content') !!}
{!! Form::textarea('content',null,['id'=>'content','placeholder'=>'Enter the content','class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Publish Now',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
	{!! Form::submit('Publish Later',['class'=>'btn btn-primary btn-submit','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@include('partials.errors')
