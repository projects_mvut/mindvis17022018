<form method="POST" action="/auth/login">
    {!! csrf_field() !!}


    <div class="form-group">
        <input placeholder="Email Address" class="form-control authinput" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">        
        <input placeholder="Password" class="form-control authinput" type="password" name="password">
    </div>
    
    <div class="form-group">
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div class="form-group">
        <button class="btn btn-primary" type="submit"><b>Login </b></button> 
        <a href="/password/email" class="btn btn-default" >Reset Password</a>
    </div>
</form>
@include('partials.errors')