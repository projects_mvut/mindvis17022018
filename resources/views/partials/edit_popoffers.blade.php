@extends('layouts.dashboard')
@section('dashcon')
<h1>Edit</h1>
<p class="lead">Add to your Main Menu</p>
<div class="container">
  <form method="post" action="{{action('PopController@update', $id)}}">
   <div class="form-group">
        {{csrf_field()}}
               <input name="_method" type="hidden" value="PATCH">

    <label for="txt">Text</label>
    <input type="text" class="form-control" name="poptext" id="txt" value="{{$pop_edit->poptext}}">
  </div>
   <div class="form-group">
    <label for="txt">Link To</label>
    <input type="text" placeholder="https://" class="form-control" name="calltoaction" id="txt" value="{{$pop_edit->calltoaction}}">
  </div>
  <div class="form-group">
    <label for="bgc">Background Color</label>
 <select name="popbgcolor">
<option value="#0375B4" @if($pop_edit->popbgcolor == "#0375B4") selected @else  @endif style="background-color:#0375B4; color:#fff;">Starry Night</option>
<option value="#007849" @if($pop_edit->popbgcolor == "#007849") selected @else  @endif style="background-color:#007849; color:#fff;">IRISES</option>
<option value="#813772"  @if($pop_edit->popbgcolor == "#813772") selected @else  @endif style="background-color:#813772; color:#fff;">POSY</option>
<option value="#B82601" @if($pop_edit->popbgcolor == "#B82601") selected @else  @endif style="background-color:#B82601; color:#fff;">EMBERS</option>
<option value="#565656" @if($pop_edit->popbgcolor == "#565656") selected @else  @endif style="background-color:#565656; color:#fff;">BLACKBOARD</option>
<option value="#49274A" @if($pop_edit->popbgcolor == "#49274A") selected @else  @endif style="background-color:#49274A; color:#fff;">EGGPLANT</option>
<option value="#1A2930" @if($pop_edit->popbgcolor == "#1A2930") selected @else  @endif style="background-color:#1A2930; color:#fff;">DENIM</option>
<option value="#01ABAA" @if($pop_edit->popbgcolor == "#01ABAA") selected @else  @endif style="background-color:#01ABAA; color:#fff;">AZURE</option>
</select>
   </div>
  <div class="form-group">
    <label for="tc">Text Color</label>
   <select name="poptextcolor">
<option value="#fff">White</option>
<option value="#000">Black</option>
</select>
  </div>
<select name="courseid">
@foreach($courses as $course)
  <option @if( $course['id'] == $pop_edit->courseid) selected @else  @endif value="{{$course['id']}}">{{$course['title']}}</option>
@endforeach   
</select>
    <div class="form-group row">
      <div class="col-md-2"></div>
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
</div>
@stop

