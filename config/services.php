<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN','mindvis.in'),
        'secret' => env('MAILGUN_SECRET','key-5b3690ca1bad1740fed71f7a754d593b'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook'=>[
    'client_id'=>'1616964004991860',
    'client_secret'=>'7f63ebea78a003cf883ab3f34a31767e',
    'redirect'=>env('FB_REDIRECT','https://mindvis.in/facebook')
    ],

    'google'=>[
    'client_id'=>'734615602577-agpdl5pkrp3kuhqsqki4mglhurqu5kg1.apps.googleusercontent.com',
    'client_secret'=>'qtJkhkeBpsF6dpXS249BQ2GV',
    'redirect'=>env('GOOGLE_REDIRECT','https://mindvis.in/google')
    ],


];
