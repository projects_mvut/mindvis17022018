<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $fillable=['title','content','course_id','is_published','slug','url','preview','section_id','sequence','is_public'];
    // protected $with = ['lcomments'];
    public function scopePublished($query){
    	return $query->where('is_published',true);
    }

	public function scopeDrafted($query){
	    	return $query->where('is_published',false);
	}

    public function courses(){
    	return $this->belongsTo('App\Course');
    }

    public function sections(){
        return $this->belongsTo('App\Section');
    }

    public function lcomments(){
        return $this->hasMany('App\Lcomment');
    }

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('deadline', 'completed');
    }
}
