<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lcomment extends Model
{
    protected $fillable=['content','user_id','lecture_id'];

    protected $with = ['user'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function lecture(){
    	return $this->belongsTo('App\Lecture');
    }
}
