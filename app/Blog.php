<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable =['title','slug'];

    public function articles(){
        return $this->hasMany('App\Article');
    }
}
