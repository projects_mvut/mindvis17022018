<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);

        $router->bind('article',function($article){
            return \App\Article::where('slug',$article)->firstorfail();
        });

        $router->bind('blog',function($blog){
            return \App\Blog::where('slug',$blog)->firstorfail();
        });

        $router->bind('course',function($course){
            return \App\Course::where('slug',$course)->firstorfail();
        });

        $router->bind('lecture',function($lecture){
            return \App\Lecture::where('slug',$lecture)->firstorfail();
        });

        $router->bind('teacher',function($teacher){
            return \App\User::teacher()->where('username',$teacher)->firstorfail();
        });

        $router->bind('student',function($student){
            return \App\User::student()->where('username',$student)->firstorfail();
        });

        $router->bind('user',function($user){
            return \App\User::where('username',$user)->firstorfail();
        });

        $router->bind('category',function($category){
            return \App\Category::where('slug',$category)->firstorfail();
        });

        $router->bind('forum',function($forum){
            return \App\Forum::where('slug',$forum)->firstorfail();
        });

        $router->bind('subject',function($subject){
            return \App\Subject::where('slug',$subject)->firstorfail();
        });

        $router->bind('section',function($section){
            return \App\Section::where('slug',$section)->firstorfail();
        });

        $router->bind('quiz',function($quiz){
            return \App\Quiz::where('slug',$quiz)->firstorfail();
        });

        $router->bind('review',function($review){
            return \App\Review::where('id',$review)->firstorfail();
        });

        $router->bind('email',function($email){
            return \App\Email::where('id',$email)->firstorfail();
        });

        $router->bind('trial',function($trial){
            return \App\Trial::where('id',$trial)->firstorfail();
        });

        $router->bind('gk',function($gk){
            return \App\Gk::where('slug',$gk)->firstorfail();
        });

        $router->bind('lcomment',function($lcomment){
            return \App\Lcomment::where('id',$lcomment)->firstorfail();
        });

        $router->bind('assignment',function($assignment){
            return \App\Assignment::where('id',$assignment)->firstorfail();
        });
        $router->bind('test',function($test){
            return \App\Test::where('id',$test)->with('attempts')->firstorfail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
