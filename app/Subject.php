<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $fillable=['title','course_id','preview','slug'];

	protected $with = ['sections'];

    public function sections(){
    	return $this->hasMany('App\Section');
    }

    public function course(){
    	return $this->belongsTo('App\Course');
    }
}
