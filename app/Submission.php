<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable=['assignment_id','link','user_id'];

    public function assignment(){
    	return $this->belongsTo('App\Assignment');
    }
}
