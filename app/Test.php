<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable=['user_id','quiz_id','answers','score'];

    public function user(){
		return $this->belongsTo('App\User');
	}

	public function quiz(){
		return $this->belongsTo('App\Quiz');
	}

	public function attempts(){
		return $this->hasMany('App\Attempt')->orderBy('qno');
	}
}
