<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    protected $fillable=['qno','test_id','answer','marked'];

    public function test(){
    	return $this->belongsTo('App\Test');
    }
}
