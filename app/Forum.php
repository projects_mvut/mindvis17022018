<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $fillable=['title','user_id','slug','content'];

    public function answers(){
    	return $this->hasMany('App\Answer');
    }

    public function users(){
    	return $this->belongsTo('App\User');
    }
}
