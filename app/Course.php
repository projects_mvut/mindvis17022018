<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $fillable=['title','content','is_published','slug','url','preview','category_id','banner','price','instamojo_button','base_users','curriculum_color','curriculum_text_color','demo_video','no_of_video','no_of_quiz','no_of_live_session','dvds','notes','no_of_mocks','instamojo_self','price_self','keywords','description','seo_title','trial','created_at'];

	public function scopePublished($query){
		return $query->where('is_published',true);
	}

	public function scopeDrafted($query){
		return $query->where('is_published',false);
	}

	public function scopeOrder($query){
		return $query->orderBy('title','asc');
	}

	public function assignments(){
		return $this->hasMany('App\Assignment');
	}

	public function lectures(){
		return $this->hasMany('App\Lecture');
	}

	public function sections(){
		return $this->hasMany('App\Section');
	}

	public function subjects(){
		return $this->hasMany('App\Subject');
	}

	public function quizzes(){
		return $this->hasMany('App\Quiz');
	}

	public function reviews(){
		return $this->hasMany('App\Review');
	}

	public function users(){
		return $this->belongsToMany('App\User')->withPivot('is_active');
	}

	public function category(){
		return $this->belongsTo('App\Category');
	}
	public function Pop() 
    {
    	return $this->belongsTo('App\Pop');
	}
}
