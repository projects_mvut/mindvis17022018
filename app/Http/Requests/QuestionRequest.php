<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title'=>'required',
            'question_number'=>'required|integer',
            'type'=>'in:mcq,fil',
            'positive_marks'=>'numeric',
            'negative_marks'=>'numeric',
            // 'correct_answer'=>'required',
        ];
    }
}
