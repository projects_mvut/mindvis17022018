<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id='';
        if($this->course){
            $id=$this->course->id;
        }

        return [
            'title'=>'required',
            'slug'=>'unique:courses,slug,'.$id,
            'price'=>'numeric',
            'category_id'=>'required',
            'trial'=>'required',
            // 'banner'=>'mimes:jpg',
        ];
    }
}
