<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EbookRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email',
            'mobile'=>'required',
            'working_or_student'=>'required',
            'city'=>'required',
            'like_us_to_contact'=>'required'
        ];
    }
}
