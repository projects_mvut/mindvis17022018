<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \App\Http\Middleware\CheckUserSession::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'admin' => \App\Http\Middleware\Admin::class,
        'owner' => \App\Http\Middleware\Owner::class,
        'verify' => \App\Http\Middleware\Verify::class,
        'selectcourse' => \App\Http\Middleware\SelectCourse::class,
        'teacher' => \App\Http\Middleware\Teacher::class,
        'courseauthor' => \App\Http\Middleware\CourseAuthor::class,
        'artauthor' => \App\Http\Middleware\ArtAuthor::class,
        'forumauthor' => \App\Http\Middleware\ForumAuthor::class,
        'subscriber' => \App\Http\Middleware\Subscriber::class,
        'reviewauthor' => \App\Http\Middleware\ReviewAuthor::class,
        'testtaker' => \App\Http\Middleware\TestTaker::class,
    ];
}
