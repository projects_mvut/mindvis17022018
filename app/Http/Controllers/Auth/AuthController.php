<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;
class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/
	protected $redirectPath = 'dashboard';
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function authenticated(Request $request, User $user)
	{
		$user->swap();		
		return redirect()->intended($this->redirectPath());
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'username' => 'required|max:255|unique:users',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		$captcha=$data['g-recaptcha-response'];
		$respon=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ld1nCkUAAAAAGEDv0t9kJLL6X3S9b1aYvHjct1b&response=".$captcha);
		$respon=json_decode($respon,true);
		$data['username']=str_slug($data['username']).'-'.str_random(4);
		if($respon['success']){
			$confirmation_code=str_random(60);
			$user=User::create([
				'first_name' => ucfirst(strtolower($data['first_name'])),
				'last_name' => ucfirst(strtolower($data['last_name'])),
				'username' => $data['username'],
				'email' => $data['email'],
				'password' => bcrypt($data['password']),
				'slug'=>$data['username'],
				'confirmation_code'=>$confirmation_code,
				]);

			Mail::queue('emails.verify', ['token' => $confirmation_code], function($message) use ($user){
				$message->to($user->email, $user->username)
				->subject('Verify Your email');
			});
			Mail::queue('emails.notify',['user'=>$user],
				function($m){
					$m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
					->subject('Mindvis User Register Notification');
				});
			return $user;
		}
		else{
			return redirect()->back()->with('status-danger','Spam');
		}
		
	}
}
