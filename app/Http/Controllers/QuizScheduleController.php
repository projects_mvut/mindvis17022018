<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Course;
use App\Quiz;
use Carbon\Carbon;

class QuizScheduleController extends Controller
{
    public function unassign(User $user, Quiz $quiz){
        $user->quizzes()->detach($quiz->id);
        return redirect()->back()->with('status-alert','Unassigned');    
    }
    
   public function add(User $user, Quiz $quiz,$days){
    $lipo=$user->quizzes->find($quiz->id);
    if($lipo){
        $deadline=$lipo->pivot->deadline;
        $deadline=Carbon::createFromTimeStamp(strtotime($deadline));
        $deadline=$deadline->addDays($days);
        $user->quizzes()->updateExistingPivot($quiz->id, ['deadline' => $deadline]);
    }
    else{
        $deadline=Carbon::now();
        $deadline=$deadline->addDays($days);
        $user->quizzes()->attach($quiz->id, ['deadline' => $deadline, 'completed' => 'No']);
    }
    return redirect()->back()->with('status','Deadline Updated');
   }

   public function sub(User $user, Quiz $quiz,$days){
    $lipo=$user->quizzes->find($quiz->id);
    if($lipo){
        $deadline=$lipo->pivot->deadline;
        $deadline=Carbon::createFromTimeStamp(strtotime($deadline));;
        $deadline=$deadline->subDays($days);
        $user->quizzes()->updateExistingPivot($quiz->id, ['deadline' => $deadline]);
    }
    else{
        $deadline=Carbon::now();
        $deadline=$deadline->subDays($days);
        $user->quizzes()->attach($quiz->id, ['deadline' => $deadline, 'completed' => 'No']);
    }
    return redirect()->back()->with('status','Deadline Updated');
   }

   public function today(User $user, Quiz $quiz){
    $lipo=$user->quizzes->find($quiz->id);
    $deadline=Carbon::now();
    if($lipo){
        $user->quizzes()->updateExistingPivot($quiz->id, ['deadline' => $deadline]);
    }
    else{
        $user->quizzes()->attach($quiz->id, ['deadline' => $deadline, 'completed' => 'No']);
    }
    return redirect()->back()->with('status','Deadline Updated');
   }

   public function check(Quiz $quiz){
    $user=Auth::user();
    $user->quizzes()->updateExistingPivot($quiz->id, ['completed' => 'Yes']);
    return redirect()->back()->with('status','Schedule Updated');
   }

   public function uncheck(Quiz $quiz){
    $user=Auth::user();
    $user->quizzes()->updateExistingPivot($quiz->id, ['completed' => 'No']);
    return redirect()->back()->with('status','Schedule Updated');
   }
}
