<?php

namespace App\Http\Controllers;
use \Auth;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Requests\ArticleRequest;
use App\Http\Controllers\Controller;

use App\Blog;

class ArticlesController extends Controller
{
      
    public function index($page)
    {
        $blog=0;
        $blogs=Blog::all();
        if(!ctype_digit(strval($page))){
            return abort('404');
        }
        $next=1;
        $prev=1;
        $articles=Article::latest()->published()->get()->forPage($page,5);
        $Nextarticles=Article::latest()->published()->get()->forPage($page+1,5);
        $Prevarticles=Article::latest()->published()->get()->forPage($page-1,5);
        if($articles->isEmpty() or $page==0){
            return abort('404');
        }
        if($Nextarticles->isEmpty()){
            $next=0;
        }
        if($Prevarticles->isEmpty() or $page==1){
            $prev=0;
        }       
        return view('articles.index',compact('articles','page','next','prev','blogs','blog'));
    }

 
    public function create()
    {
        $select_category=Blog::lists('title', 'id')->toArray();
        return view('articles.create',compact('select_category'));
    }


    public function store(ArticleRequest $request)
    {
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        include 'imagefunction.php';
        if($input['submit']=="Publish Now"){
            $input['is_published']=true;
            Auth::user()->articles()->create($input);
            return redirect('articles/page/1')->with('status','Article Created');
        }
        else{
            $input['is_published']=false;
            Auth::user()->articles()->create($input);
            return redirect('articles/page/1')->with('status','Article Saved');
        }
    }


    public function show(Article $article)
    {
        $author=false;
        if(!Auth::check()){
            $author=false;
        }
        else if(Auth::user()->is_admin or Auth::user()->articles->contains($article->id)){
            $author=true;
        }
        $seo_title=$article->seo_title;
        $keywords=$article->keywords;
        $description=$article->description;
        return view('articles.show',compact('articles','article','author','keywords','description','seo_title'));
    }


    public function edit(Article $article)
    {
        $select_category=Blog::lists('title', 'id')->toArray();
        return view('articles.edit',compact('article','select_category'));
    }

 
    public function update(ArticleRequest $request, Article $article)
    {
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        //Add image to UPLOADS
        include 'imagefunction.php';
        if($input['submit']=="Publish Now"){
            $input['is_published']=true;
            $article->update($input);
            return redirect('articles/'.$article->slug)->with('status','Article Updated');
        }
        else{
            $input['is_published']=false;
            $article->update($input);
            return redirect('articles/'.$article->slug)->with('status','Article Saved');
        }
    }
  
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect('articles/page/1')->with('status-danger','Article Deleted');
    }
}
