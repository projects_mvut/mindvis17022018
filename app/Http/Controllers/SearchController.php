<?php

namespace App\Http\Controllers;

use \Input;
use App\Course;
use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function index()
    {
            if (Input::has('query')) {
                 $query = Input::get('query');
                 $courses_title=Course::published()->where('title', 'LIKE', "%$query%")->get();
                 $courses_content=Course::published()->where('content', 'LIKE', "%$query%")->get();
                 $courses=$courses_content->merge($courses_title);
                 $articles_title=Article::published()->where('title', 'LIKE', "%$query%")->get();
                 $articles_content=Article::published()->where('content', 'LIKE', "%$query%")->get();
                 $articles=$articles_content->merge($articles_title);
                 return view('pages.search',compact('courses','query','articles'));
             }
             else{
                return redirect('courses');
             }
     }

}
