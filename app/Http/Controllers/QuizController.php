<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\QuizRequest;
use App\Http\Requests\QuizcsvRequest;
use App\Http\Controllers\Controller;
use App\Course;
use App\Subject;
use App\Section;
use App\Quiz;
use App\Lecture;
use App\Question;
use Response;
class QuizController extends Controller
{
    public function create(Course $course,Subject $subject,Section $section){
        return view('quizzes.create',compact('course','subject','section'));
    }

    public function show(Course $course,Subject $subject,Section $section, Quiz $quiz,Request $request){
        $path=$request->route()->quiz->slug;
        $questions=Question::where('quiz_id',$quiz->id)->order()->get();
        $question=Question::where('quiz_id',$quiz->id)->order()->first();
        $lectures=Lecture::where('section_id',$section->id)->get();
        $quizzes=Quiz::where('section_id',$section->id)->get();
        return view('quizzes.show',compact('course','quiz','questions','question','subject','section','lectures','quizzes','path'));
    }

    public function edit(Course $course,Subject $subject,Section $section, Quiz $quiz){
        return view('quizzes.edit',compact('course','quiz','section','subject'));
    }
    
    public function store(Course $course,Subject $subject,Section $section, QuizRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $input['course_id']=$course->id;
        $input['section_id']=$section->id;
        Quiz::create($input);
        return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status','Quiz Created');
    }

    public function draft(Quiz $quiz){
        $quiz->update(['drafted'=>1]);
        return redirect()->back();
    }

    public function live(Quiz $quiz){
        $quiz->update(['drafted'=>0]);
        return redirect()->back();
    }

    public function update(Course $course,Subject $subject,Section $section, Quiz $quiz, QuizRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $quiz->update($input);
        return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/quiz/'.$quiz->slug)->with('status','Quiz Updated');
    }

    public function delete(Course $course,Subject $subject,Section $section, Quiz $quiz){
        $quiz->delete();
        return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status-danger','Quiz Deleted');
    }

    public function quizcsv(Quiz $quiz, QuizcsvRequest $request){
        $input=$request->all();
        $questions = array_map('str_getcsv', file($request->file('quiz_csv')));
        // dd($questions);
        $head=$questions[0];     
        $size=sizeof($head);
        $error='';
        foreach($questions as $question){
            $hold=[];
            $hold['quiz_id']=$quiz->id;
            for($count=0;$count<$size;$count++){
                $hold[$head[$count]]=$question[$count];
            }
            $q=(int)$question[0];
            // $w=!empty($question[1]);
            $w=true;
            if($q and $w){
                Question::create($hold);
            }else{
                $error=$error.' '.$q.' is not a number. Question dropped , please add it manually.<br>';
            }
        }
        return redirect()->back()->with('status','Question Added');
    }

    public function downloadcsv(Quiz $quiz){
        $output = implode(",", array('question_number','title','content','correct_answer','positive_marks','negative_marks','type','option1','option2','option3','option4','solution'));
        $output.="\r\n";
        $questions=Question::where('quiz_id',$quiz->id)->order()->get();
        foreach($questions as $question){
            // $content=$question->content;
            // $content=str_replace("\r","",$content); 
            // $content=str_replace("\n","",$content);  
            // $content=str_replace('"','""',$content);  

            $output .=  implode(",", array(
                $question->question_number,
                $this->encsv($question->title),
                $this->encsv($question->content),
                $this->encsv($question->correct_answer),
                $question->positive_marks,
                $question->negative_marks,
                $question->type,
                $this->encsv($question->option1),
                $this->encsv($question->option2),
                $this->encsv($question->option3),
                $this->encsv($question->option4),
                $this->encsv($question->solution)
                ));
            $output.="\r\n";
        }
        $filename=$quiz->title.date(" jS F").'.csv';
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$filename,
        );
        return Response::make(rtrim($output, "\n"), 200, $headers);
    }

    public function encsv($string) {
        $string = '"' . str_replace('"', '""', $string) . '"';
        $string = str_replace(array("\r\n", "\r", "\n"), "", $string);
        return $string;
    }
}
