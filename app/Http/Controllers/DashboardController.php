<?php

namespace App\Http\Controllers;
use App\User;
use App\Course;
use App\Article;
use App\Setting;
use App\Newsletter;
use App\Email;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\MobileRequest;
use App\Http\Requests\AddUserRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Response;
class DashboardController extends Controller
{
	public function __construct(){
		$this->middleware('selectcourse',['only'=>['courses']]);
		$this->middleware('verify',['only'=>['courses']]);
	}

	public function active(User $user, Course $course){
		$course->users()->updateExistingPivot($user->id, ['is_active'=>1]);;
		return redirect('/dashboard/courses/'.$course->slug.'/users')->with('status','User activation changed');
	}

	public function deactive(User $user, Course $course){
		$course->users()->updateExistingPivot($user->id, ['is_active'=>0]);;
		return redirect('/dashboard/courses/'.$course->slug.'/users')->with('status','User activation changed');
	}

	public function newsletter(){
		$newsletters=Newsletter::all();
		return view('dashboard.newsletter',compact('newsletters'));
	}

	// public function gather(){
	// 	//set_time_limit(200);
	// 	$data=array();
	// 	$data['users']=\App\User::all();
	// 	$data['categories']=\App\Category::all();
	// 	$data['courses']=\App\Course::all();
	// 	$data['subjects']=\App\Subject::all();
	// 	$data['sections']=\App\Section::all();
	// 	$data['lectures']=\App\Lecture::all();
	// 	$data['course_user']=DB::table('course_user')->get();
	// 	$data['blogs']=\App\Blog::all();
	// 	$data['articles']=\App\Article::all();
	// 	$data['forums']=\App\Forum::all();
	// 	$data['answers']=\App\Answer::all();
	// 	$data['quizzes']=\App\Quiz::all();
	// 	$data['questions']=\App\Question::all();
	// 	$data['images']=\App\Image::all();
	// 	$data['reviews']=\App\Review::all();
	// 	$data['gks']=\App\Gk::all();
	// 	$data['tests']=\App\Test::all();
	// 	$data['attempts']=\App\Attempt::all();
	// 	$data['lcomments']=\App\Lcomment::all();
	// 	$data['settings']=\App\Setting::all();
	// 	$data['lecture_user']=DB::table('lecture_user')->get();
	// 	$data['quiz_user']=DB::table('quiz_user')->get();
	// 	$data['newsletters']=\App\Newsletter::all();
	// 	$data['assignments']=\App\Assignment::all();
	// 	$data['submissions']=\App\Submission::all();
	// 	// $data['desks']=\App\Desk::all();
	// 	// $data['chats']=\App\Chat::all();
	// 	foreach ($data['users'] as $key => $user) {
	// 		$user['pass']=$user->password;
	// 	}
	// 	return $data;
	// }

	// public function downloaddb(){
	// 	$output=$this->gather();
	// 	// dd($output);
	// 	$output=json_encode($output);
	// 	$filename='mindvisdb.json';
	// 	$headers = array(
	// 		'Content-Type' => 'text/json',
	// 		'Content-Disposition' => 'attachment; filename='.$filename,
	// 		);
	// 	return Response::make($output, 200, $headers);
	// }

	public function downloadNewsletter(){
		$output = implode(",", array('name','email','contact','course'));
		$output.="\r\n";
		$newsletters=Newsletter::all();
		foreach($newsletters as $newsletter){
			$output .=  implode(",", array(
				$this->encsv($newsletter->name),
				$this->encsv($newsletter->email),
				$this->encsv($newsletter->contact),
				$this->encsv($newsletter->course),
				));
			$output.="\r\n";
		}
		$filename='newsletter.csv';
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename='.$filename,
			);
		return Response::make(rtrim($output, "\n"), 200, $headers);
	}
	public function encsv($string) {
		$string = '"' . str_replace('"', '""', $string) . '"';
		$string = str_replace(array("\r\n", "\r", "\n"), "", $string);
		return $string;
	}

	public function upload_schedule(Request $request,User $user){
		$input=$request->all();
		if($request->hasFile('schedule_url')){
			$filename=$user->username.'-schedule';
			$filename = $filename.'.'.$request->file('schedule_url')->getClientOriginalExtension();
			$request->file('schedule_url')->
			move(base_path().'/public/uploads/schedules/',$filename);
			$user->schedule_url='/uploads/schedules/'.$filename;
			$user->save();
		}
		return redirect()->back()->with('status','Schedule Uploaded');
	}

	public function download_schedule(){
		$filepath=base_path().'/public'.Auth::user()->schedule_url;
		return response()->download($filepath);
	}

	public function teachers(){    
		$teachers=User::teacher()->order()->get();
		return view('dashboard.teachers',compact('teachers'));
	}

	public function users(){    
		$users=User::student()->where('is_admin',false)->order()->get();

		return view('dashboard.users',compact('users'));
	}

	public function date_users($date){
		$date=Carbon::parse($date);
		$today=new Carbon($date);
		$next=new Carbon($date->addDay());
		$prev=new Carbon($date->subDays(2));
		$users=User::where('created_at','>=',$today)->where('created_at','<=',$next)->get();
		$emails=Email::lists('subject', 'id')->toArray();
		return view('dashboard.date_users',compact('users','today','next','prev','emails'));
	}

	public function courses(){
		if(Auth::user()->is_admin){    
			$courses=Course::order()->get();
			return view('dashboard.courses',compact('courses'));
		}
		elseif(Auth::user()->is_teacher){
			$user=Auth::user();
			$courses=$user->courses()->order()->get();
			return view('dashboard.courses',compact('courses'));
		}
		else{
			$courses=Auth::user()->courses()->order()->get();
			return view('users.courses',compact('courses'));
		}
		
	}

	public function subscribed(Course $course){
		$users=$course->users()->student()->order()->get();
		$output = implode(",", array('name','email','mobile','course'));
		$output.="\r\n";
		foreach($users as $user){
			$output .=  implode(",", array(
				$this->encsv($user->first_name.' '.$user->last_name),
				$this->encsv($user->email),
				$this->encsv($user->mobile),
				$this->encsv($course->title),
				));
			$output.="\r\n";
		}
		$filename=$course->slug.'-subscribed-users.csv';
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename='.$filename,
			);
		return Response::make(rtrim($output, "\n"), 200, $headers);
	}

	public function unsubscribed(Course $course){
		$users=User::where('course_interested',$course->title)->get();
		$output = implode(",", array('name','email','mobile','course'));
		$output.="\r\n";
		foreach($users as $user){
			$count=$user->courses()->count();
			if(!$count){    
				$output .=  implode(",", array(
					$this->encsv($user->first_name.' '.$user->last_name),
					$this->encsv($user->email),
					$this->encsv($user->mobile),
					$this->encsv($course->title),
					));
				$output.="\r\n";
			}
		}
		$filename=$course->slug.'-unsubscribed-users.csv';
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename='.$filename,
			);
		return Response::make(rtrim($output, "\n"), 200, $headers);
	}

	public function admins(){    
		$admins=User::where('is_admin',true)->order()->get();
		return view('dashboard.admins',compact('admins'));
	}

	public function author(User $student){
		$student->update(['is_teacher'=>true]);
		return redirect('/dashboard/teachers')->with('status','User assigned as Teacher');
	}

	public function suspend(User $user){
		if($user->is_admin){
			return redirect()->back()->with('status-danger','Admin cannot be suspended.');
		}
		elseif($user->is_teacher){
			$user->update(['is_teacher'=>false]);
			return redirect('/dashboard/users')->with('status-alert','Teacher Suspended');
		}      
		else{
			return redirect()->back()->with('status-alert','Already Suspended.');
		}  
		
	}

	public function delete(User $user){
		//deletes user
		if($user->is_admin){
			return redirect()->back()->with('status-danger','Admin cannot be Deleted.');
		}
		else{
			$user->delete();
			return redirect()->back()->with('status-alert','Deleted');
		}

	}

	public function delete_course(Course $course){
		$course->delete();
		return redirect()->back()->with('status-danger','Course Deleted.');
	}

	public function delete_article(Article $article){
		$article->delete();
		return redirect()->back()->with('status-danger','Article Deleted.');
	}

	public function teacher_courses(User $teacher){       
		$courses=$teacher->courses()->order()->get();
		$courses_un=Course::order()->get()->diff($courses);
		return view('dashboard.teacherCourses',compact('teacher','courses','courses_un'));
	}

	public function user_courses(User $user){
		$courses=$user->courses()->order()->get();
		$courses_un=Course::order()->get()->diff($courses);
		return view('dashboard.userCourses',compact('user','courses','courses_un'));
	}

	public function course_teachers(Course $course){
		$teachers=$course->users()->teacher()->order()->get();
		$teachers_un=User::teacher()->order()->get()->diff($teachers);
		return view('dashboard.courseTeachers',compact('course','teachers','teachers_un'));
	}

	public function course_users(Course $course){
		$users=$course->users()->student()->order()->get();
		$users_un=User::student()->order()->get()->diff($users);
		return view('dashboard.courseUsers',compact('course','users','users_un'));
	}

	public function assign(User $user, Course $course){
		if(!$user->courses->contains($course->id)){
			if($user->confirmation_code){
				$user->update(['confirmation_code'=>NULL]);
			}
			$user->courses()->attach($course->id);
			return redirect()->back()->with('status','Success.');
		}
		else{
			return redirect()->back()->with('status-alert','Already Added.');
		}
		
		
	}

	public function unassign(User $user, Course $course){
		if($user->courses->contains($course->id)){
			$user->courses()->detach($course->id);
			return redirect()->back()->with('status','Success.');
		}
		else{
			return abort('404');
		}
		
	}

	public function blogs(){
		if(!Auth::user()){
			return redirect('auth/login');
		}
		elseif(Auth::user()->is_admin){
			$blogs=Article::all();
		}
		else{
			$blogs=Auth::user()->articles()->get();
		}
		
		return view('dashboard.blogs',compact('blogs'));
	}

	public function adduser(){
		return view('dashboard.adduser');
	}

	public function storeuser(AddUserRequest $request){
		$input=$request->all();
		$input['username']=str_slug($input['username']).'-'.str_random(4);
		$input['slug']=$input['username'];
		$input['password'] = bcrypt($input['password']);
		$input['is_confirmed']=1;
		User::create($input);
		return redirect('dashboard/users')->with('status','User Created');
	}

	public function mobile(){
		return view('dashboard.mobile');
	}

	public function storemobile(MobileRequest $request){
		$input=$request->all();
		$mobile=$input['mobile'];
		if($mobile==Auth::user()->mobile){
			return redirect()->back()->with('status-alert','Same No. not allowed');
		}
		$otp = substr(str_shuffle("0123456789"), 0, 4);
		$msg="Your verification code for MindVis is ".$otp;
		$url="http://bhashsms.com/api/sendmsg.php?user=mindvis&pass=BulkSMS1234&sender=MINDVI&phone=".$mobile."&text=".urlencode($msg)."&priority=ndnd&stype=normal";
		$respon=file_get_contents($url);
		Auth::user()->update(['vmobile'=>$mobile,'otp'=>$otp]);
		return redirect()->back()->with('status',"Verifiction code sent to your Mobile No.");
	}

	public function resendOtp(){
		$otp=Auth::user()->otp;
		if(!$otp){
			return redirect()->back()->with('status-alert','No Request Pending');
		}
		$mobile=Auth::user()->vmobile;
		$msg="Your verification code for MindVis is ".$otp;
		$url="http://bhashsms.com/api/sendmsg.php?user=mindvis&pass=BulkSMS1234&sender=MINDVI&phone=".$mobile."&text=".urlencode($msg)."&priority=ndnd&stype=normal";
		echo($url);
		$respon=file_get_contents($url);
		return redirect()->back()->with('status',"Verification Code Sent");
	}

	public function verifymobile(Request $request){
		$input=$request->all();
		if($input['otp']==Auth::user()->otp){
			$mobile=Auth::user()->vmobile;
			Auth::user()->update(['otp'=>null,'mobile'=>$mobile,'vmobile'=>null]);
			return redirect('dashboard/courses')->with('status','Mobile No. Updated');
		}
		else{
			return redirect()->back()->with('status-alert','Try again');
		}
	}

	public function course_interested(){
		$courses=Course::published()->latest()->lists('title');
		return view('dashboard.course_interested',compact('courses'));
	}

	public function store_course_interested(Request $request){
		$input=$request->all();
		$c=$input['course_interested'];
		if($c){
			Auth::user()->update(['course_interested'=>$c]);
			return redirect('dashboard/courses')->with('status','Saved');
		}
		return redirect('dashboard/courses')->with('status-alert','Select a Course');
	}

	public function home_seo(){
		$home=Setting::where('title','home')->first();
		return view('dashboard.seo',compact('home'));
	}

	public function save_home_seo(Request $request){
		$input=$request->all();
		$home=Setting::where('title','home')->first();
		if($home){
			$home->update($input);
		}
		else{
			$input['title']='home';
			Setting::create($input);
		}
		return redirect()->back()->with('status','Seo Settings Updated');
	}

	public function userlist(){
		$users=User::all();
		$output = implode(",", array('First Name','Last Name','Username','Email','Mobile','Course Intrested','No. of Courses Subscribed'));
		$output.="\r\n";
		foreach($users as $user){
			$output .=  implode(",", array(
				$this->encsv($user->first_name),
				$this->encsv($user->last_name),
				$this->encsv($user->username),
				$this->encsv($user->email),
				$this->encsv($user->mobile),
				$this->encsv($user->course_interested),
				count($user->courses->toArray()),
				));
			$output.="\r\n";
		}
		$filename='userList.csv';
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename='.$filename,
			);
		return Response::make(rtrim($output, "\n"), 200, $headers);
	}
}
