<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Trial;
use App\Email;
use App\User;
use App\Tracker;
use Mail;
use Carbon\Carbon;
class TrialController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}

	public function index(){
		$trials=Trial::latest()->get();
		$emails=Email::lists('subject', 'id')->toArray();
		$mailCount=Email::count();

		return view('dashboard.trials',compact('trials','emails','mailCount'));
	}

	public function updateAll(Request $request){
		$input=$request->all();
		foreach ($input as $key => $value) {
			if (strpos($key, '-') !== false) {
				$e=explode('-',$key);
				$id=$e[1];
				$k=$e[0];
				$f=Trial::where('id',$id)->first();
				$f->update([$k=>$value]);
			}
		}
		return redirect()->back();
	}

	public function date_trials($date){
		$date=Carbon::parse($date);
		$today=new Carbon($date);
		$next=new Carbon($date->addDay());
		$prev=new Carbon($date->subDays(2));
		$trials=Trial::where('created_at','>=',$today)->where('created_at','<=',$next)->get();
		$emails=Email::lists('subject', 'id')->toArray();
		$mailCount=Email::count();
		return view('dashboard.trials',compact('trials','today','next','prev','emails','mailCount'));
	}

	public function update(Trial $trial,Request $request){
		$input=$request->all();
		$trial->update($input);
		return redirect()->back();
	}

	public function delete(Trial $trial){
		$trial->delete();
		return redirect()->back();
	}

	public function mail(Trial $trial,Request $request){
		$input=$request->all();
		$email=Email::where('id',$input['email_id'])->first();
		if($input['submit']=="Test"){
			$email1='himanshu.vasistha@gmail.com';
			$name='himanshu';
		}
		else{
			$email1=$trial->email;
			$name=$trial->name;
			$count=Tracker::where('trial_id',$trial->id)->where('email_id',$email->id)->count();
			if($count){
				if($input['submit']!="Force"){
					return redirect()->back()->with('status-alert','Mail Already Sent, Use Force');
				}
			}
			else{
				Tracker::create(['trial_id'=>$trial->id,'email_id'=>$email->id]);
			}
		}
		Mail::send([],[],function($m) use ($email1,$email,$name){
			$m->to($email1,$name)
			->subject($email->subject)
			->setBody($email->content, 'text/html');
		});
		return redirect()->back();
	}

	public function usermail(User $user,Request $request){
		$input=$request->all();
		$email=Email::where('id',$input['email_id'])->first();
		if($input['submit']=="Test"){
			$email1='himanshu.vasistha@gmail.com';
			$name='himanshu';
		}
		else{
			$email1=$user->email;
			$name=$user->first_name;

		}
		Mail::send([],[],function($m) use ($email1,$email,$name){
			$m->to($email1,$name)
			->subject($email->subject)
			->setBody($email->content, 'text/html');
		});
		return redirect()->back();
	}
}
