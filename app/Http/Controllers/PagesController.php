<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests\EbookRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsletterRequest;
use App\Newsletter;
use App\Course;
use App\Article;
use App\Review;
use App\Setting;

class PagesController extends Controller
{

    public function newsletter(NewsletterRequest $request){
        $input=$request->all();
        Newsletter::create($input);
        return redirect()->back()->with('status',"You have been Subscribed to our Newsletter");
    }

    public function index()
    {
        $description='';
        $keywords='';
        $home=Setting::where('title','home')->first();
        if($home){
            $description=$home->description;
            $keywords=$home->keywords;
        }
        $courses=Course::published()->latest()->paginate(6);
        $articles=Article::published()->latest()->paginate(3);

        return view('pages.index',compact('courses','description','keywords','articles'));
    }

    public function ebook(){
    	return view('pages.ebook');
    }

    public function download_ebook(EbookRequest $request){
    	$user=$request->all();
    	Mail::queue('emails.ebook',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->cc('sonali.vijan@gmail.com','Sonali Vijan')
                    ->subject('Mindvis Bank PO Ebook Download Notification');
        });
        return response()->download($filepath);
    }

    public function gateebook2019(){
        return view('pages.gateebook2019');
    }

    public function email_gateebook2019(EbookRequest $request){
    	$filepath=realpath(__DIR__.'/../../../storage/gate-2019-ebook-min.pdf');
        $user=$request->all();
        $user['filepath']=$filepath;
        Mail::queue('emails.gateebook',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->cc('sonali.vijan@gmail.com','Sonali Vijan')
                    ->cc('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->subject('Mindvis GATE 2019 Ebook Download Notification');
        });
        Mail::queue('emails.gateebookuser',['user'=>$user],
                function($m) use ($user){
                    $m->to($user['email'],$user['name'])
                    ->subject('GATE 2019 Preparation Tips Ebook')
                    ->attach($user['filepath']);
        });
        return redirect()->back()->with('status',"Ebook Emailed");
    }



public function gateebook2020(){
        return view('pages.gateebook2020');
    }

    public function email_gateebook2020(EbookRequest $request){
        $filepath=realpath(__DIR__.'/../../../storage/gate-2020-ebook-min.pdf');
        $user=$request->all();
        $user['filepath']=$filepath;
        Mail::queue('emails.gateebook',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->cc('sonali.vijan@gmail.com','Sonali Vijan')
                    ->cc('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->subject('Mindvis GATE 2020 Ebook Download Notification');
        });
        Mail::queue('emails.gateebookuser',['user'=>$user],
                function($m) use ($user){
                    $m->to($user['email'],$user['name'])
                    ->subject('GATE 2020 Preparation Tips Ebook')
                    ->attach($user['filepath']);
        });
        return redirect()->back()->with('status',"Ebook Emailed");
    }





    public function gatepage(){
    	return view('pages.gatepage');
    }

    public function gatemail(Request $request){
        $user=$request->all();
        $user['like_us_to_contact']='yes';
        Mail::queue('emails.gateebook',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->cc('sonali.vijan@gmail.com','Sonali Vijan')
                    ->cc('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->subject('Mindvis GATE Form Fill Notification');
        });
        return redirect('/courses/gate-2018-mechanical-engineering-online-course');
    }
}
