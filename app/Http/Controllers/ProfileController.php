<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('owner');
        $this->middleware('verify');

    }

    public function show(User $user){
        return view('dashboard.profile',compact('user'));
    }

    public function update(ProfileRequest $request,User $user){
        $input=$request->all();
        $input['username']=str_slug($input['username']);
        $input['slug']=$input['username'];
        $slug=$input['slug'];
        if($request->hasFile('url')){
            $imageName = $slug.'-'.str_random(8).'.'.$request->file('url')->getClientOriginalExtension();
            $request->file('url')->move(
            base_path() . '/public/uploads/images/',$imageName);
            $input['url']='/uploads/images/'.$imageName;
        }
        $user->update($input);
        return redirect('user/'.$user->username)->with('status','Profile Updated');
    }

    public function password(ChangePasswordRequest $request,User $user){
        $input=$request->all();
        $user->update(['password'=>bcrypt($input['password'])]);
        return redirect()->back()->with('status','Password Updated');
    }
}
