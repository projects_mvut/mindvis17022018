<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class UserpicController extends Controller
{
    public function add(Request $request){
        $input=$request->all();
        if($request->hasFile('url')){
            $picName=\Auth::user()->username.'-'.str_random(8);
            $imageName = $picName.'.'.$request->file('url')->getClientOriginalExtension();
            $request->file('url')->
            move(base_path().'/public/uploads/profile/',$imageName);
            Auth::user()->url='/uploads/profile/'.$imageName;
            Auth::user()->save();
        }
        return redirect('dashboard/courses')->with('status','Profile Pic Uploaded');
    }    
}