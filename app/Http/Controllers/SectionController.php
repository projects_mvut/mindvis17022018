<?php

namespace App\Http\Controllers;

use App\Http\Requests\SectionRequest;
use App\Http\Controllers\Controller;
use App\Course;
use App\Subject;
use App\Section;
use App\Lecture;
use App\Quiz;

class SectionController extends Controller
{
    public function show(Course $course,Subject $subject, Section $section){
        $lectures=Lecture::where('section_id',$section->id)->get();
        $quizzes=Quiz::where('section_id',$section->id)->get();
        $path="section";
        return view('sections.show',compact('section','course','lectures','quizzes','subject','path'));
    }
    public function create(Course $course,Subject $subject){
    	return view('sections.create',compact('course','subject'));
    }

    public function store(Course $course,Subject $subject, SectionRequest $request){
    	$input=$request->all();
    	if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $input['course_id']=$course->id;
    	$input['subject_id']=$subject->id;
    	Section::create($input);
    	return redirect('courses/'.$course->slug.'/curriculum')->with('status','Section Created');
    }

    public function edit(Course $course,Subject $subject, Section $section){
        return view('sections.edit',compact('course','subject','section'));
    }

    public function update(Course $course,Subject $subject, Section $section ,SectionRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $input['course_id']=$course->id;
        $section->update($input);
        return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status','Section Updated');
    }

    public function delete(Course $course, Subject $subject , Section $section){
        $section->delete();
        return redirect('courses/'.$course->slug.'/curriculum')->with('status-danger','Section Deleted');
    }
}
