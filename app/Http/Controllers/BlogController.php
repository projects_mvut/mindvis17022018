<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Blog;

class BlogController extends Controller
{
    public function save(Request $request){
        $input=$request->all();
        if($input['title']){
            $val=$input['title'];
            $input['slug']=str_slug($val);
            Blog::create($input);
            return redirect()->back()->with('status','Category Created');
        }
        return redirect()->back()->with('status-danger','Title Required');
    }

    public function delete(Blog $blog){
        $blog->delete();
        return redirect()->back()->with('status-danger','Category Deleted');
    }

    public function show(Blog $blog){
        $blogs=Blog::all();
        $page=1;
        $next=0;
        $prev=0;
        $articles=$blog->articles;
        return view('articles.index',compact('articles','page','next','prev','blogs','blog'));
    }
}
