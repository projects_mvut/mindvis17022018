<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecture;
use App\Course;
use App\Subject;
use App\Section;
use App\Quiz;
use App\Lcomment;
use App\Http\Requests\LectureRequest;
use App\Http\Controllers\Controller;

class LecturesController extends Controller
{
	public function create(Course $course,Subject $subject,Section $section)
	{
		return view('lectures.create',compact('course','subject','section'));
	}

	public function store(LectureRequest $request,Course $course,Subject $subject, Section $section)
	{
		$input=$request->all();
		if(!$input['slug']){
			$val=$input['title'];
			$input['slug']=str_slug($val);
		}
		//Add image to UPLOADS
		include 'imagefunction.php';
		$input['course_id']=$course->id;
		$input['section_id']=$section->id;
		if($input['submit']=="Publish Now"){
			Lecture::create($input);
			return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status', 'Lecture Created!');
		}
		elseif($input['submit']=="Publish Later"){
			$input['is_published']=false;
			Lecture::create($input);
			return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status', 'Lecture Saved!');
		}
	}



	public function show(Course $course,Subject $subject,Section $section, Lecture $lecture,Request $request)
	{
		$path=$request->route()->lecture->slug;
		$lectures=Lecture::where('section_id',$section->id)->get();
		$quizzes=Quiz::where('section_id',$section->id)->get();

		$previousLectureID = Lecture::where('id', '<', $lecture->id)
		->where('course_id',$course->id)
		->where('section_id',$section->id)
		->where('is_published',true)
		->max('id');

		$nextLectureID = Lecture::where('id', '>', $lecture->id)
		->where('course_id',$course->id)
		->where('section_id',$section->id)
		->where('is_published',true)
		->min('id');

		$previousLecture=Lecture::where('id',$previousLectureID)->first();
		$nextLecture=Lecture::where('id',$nextLectureID)->first();
		return view('lectures.show',compact('lecture','course','subject','section','previousLecture','nextLecture','lectures','quizzes','path'));
	}


	public function edit(Course $course,Subject $subject,Section $section, Lecture $lecture)
	{
		return view('lectures.edit',compact('course','lecture','subject','section'));
	}

	public function update(LectureRequest $request, Course $course,Subject $subject,Section $section, Lecture $lecture)
	{
		$input=$request->all();
		if(!$input['slug']){
			$val=$input['title'];
			$input['slug']=str_slug($val);
		}
		//Add image to UPLOADS
		include 'imagefunction.php';
		if($input['submit']=="Publish Now"){
			$input['is_published']=true;
			$lecture->update($input);
			return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/lecture/'.$lecture->slug)->with('status', 'Lecture Updated!');
		}
		elseif($input['submit']=="Publish Later"){
			$input['is_published']=false;
			$lecture->update($input);
			return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/lecture/'.$lecture->slug)->with('status', 'Lecture Saved!');
		}
	}


	public function destroy(Course $course,Subject $subject,Section $section, Lecture $lecture)
	{
		$lecture->delete();
		return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug)->with('status-danger', 'Lecture Deleted!');
	}
}