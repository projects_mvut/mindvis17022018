<?php

namespace App\Http\Controllers;
use App\User;
use Socialite;
use Mail;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    
    public function handleFacebookCallback(Request $request)
    {
        $user = Socialite::driver('facebook')->user();
        $email=$user->getEmail();
        $logu=User::where('email',$email)->first();
        if(!is_null($logu)){
            \Auth::login($logu);
            $logu->swap();
        }
        else{
        $name=explode(" ",$user->getName());
        $length = 4;
        $randomString = substr(str_shuffle("abbcdefghijklmnopqrstuvwxyz"), 0, $length);
        $username=$name[0].'_'.$name[1].'_'.$randomString;
        $username=str_slug($username);
        $data=['first_name'=>$name[0],'last_name'=>$name[1],'username'=>$username,'email'=>$email,'slug'=>$username,'is_confirmed'=>true,'confirmation_code'=>Null];
        $user=User::create($data);
        Mail::queue('emails.notify',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->subject('Mindvis User Register Notification');
        });
        Mail::queue('emails.welcome',['user'=>$user],
                function($m) use ($user){
                    $m->to($user->email, $user->username)
                    ->subject('Welcome To Mindvis');
        });
        \Auth::login($user);
        $user->swap();
        }
        return redirect('dashboard/courses');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    
    public function handleGoogleCallback(Request $request)
    {
        $user = Socialite::driver('google')->user();
        $email=$user->getEmail();
        $logu=User::where('email',$email)->first();
        if(!is_null($logu)){
            \Auth::login($logu);
            $logu->swap();
        }
        else{
        $name=explode(" ",$user->getName());
        $length = 4;
        $randomString = substr(str_shuffle("abbcdefghijklmnopqrstuvwxyz"), 0, $length);
        $username=$name[0].'_'.$name[1].'_'.$randomString;
        $username=str_slug($username);
        $data=['first_name'=>$name[0],'last_name'=>$name[1],'username'=>$username,'email'=>$email,'slug'=>$username,'is_confirmed'=>true,'confirmation_code'=>Null];
        $user=User::create($data);
        Mail::queue('emails.notify',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->subject('Mindvis User Register Notification');
        });
        Mail::queue('emails.welcome',['user'=>$user],
                function($m) use ($user){
                    $m->to($user->email, $user->username)
                    ->subject('Welcome To Mindvis');
        });
        \Auth::login($user);
        $user->swap();
        }
        return redirect('dashboard/courses');
    }
}