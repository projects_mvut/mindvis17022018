<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LcommentRequest;
use App\Http\Controllers\Controller;
use App\Lecture;
use App\Lcomment;

class LcommentController extends Controller
{
    public function store(Lecture $lecture, LcommentRequest $request){
        $input=$request->all();
        $input['lecture_id']=$lecture->id;
        $input['user_id']=\Auth::user()->id;
        Lcomment::create($input);
        return redirect()->back()->with('status','Comment Added');
    }

    public function delete(Lcomment $lcomment){
        $lcomment->delete();
        return redirect()->back()->with('status-danger', 'Comment Deleted!');
    }
}
