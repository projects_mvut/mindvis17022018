<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeskRequest;
use App\Http\Controllers\Controller;
use Mail;
class HelpdeskController extends Controller
{
	public function form(){
		return view('helpdesk.form');
	}

	public function submit(DeskRequest $request){
		$input=$request->all();
		Mail::queue('emails.helpdesk',$input,
			function($m){
				$m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
				->subject('Mindvis Helpdesk Query');
			});
		return redirect()->back()->with('status','Thank for the Query.
			We will respond soon.');
	}
}
