<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Http\Controllers\Controller;
use App\Review;
use App\Course;
class ReviewsController extends Controller
{
    public function store(Course $course, ReviewRequest $request){
        $input=$request->all();
        $input['course_id']=$course->id;
        $input['user_id']=\Auth::user()->id;
        Review::create($input);
        return redirect()->back()->with('status','Review Added');
    }

    public function delete(Course $course, Review $review){
    	$review->delete();
        return redirect()->back()->with('status-danger', 'Review Deleted!');
    }
}
