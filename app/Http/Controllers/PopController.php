<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pop;
use App\Course;
class PopController extends Controller
{
     public function index()
    {
    	$pops = Pop::all()->toArray();
      $courses = Course::all()->toArray();
		  return view('dashboard.popoffers',compact('pops','courses'));
    }

     public function store(Request $request)
    {
     	$pop = new Pop([
          'poptext' => $request->get('poptext'),
          'popbgcolor' => $request->get('popbgcolor'),
          'poptextcolor' => $request->get('poptextcolor'),
          'courseid' => $request->get('courseid'),
          'calltoaction' => $request->get('calltoaction')
        ]);
        $pop->save();
        return redirect('/dashboard/popoffers');
    }

     public function edit($id){
        $pop_edit = Pop::find($id);
        $courses = Course::all()->toArray() ;
        return view('partials.edit_popoffers', compact('pop_edit','id','courses'));

     }

 public function update(Request $request, $id)
    {
        $pop_update = Pop::find($id);
        $pop_update->poptext = $request->get('poptext');
        $pop_update->popbgcolor = $request->get('popbgcolor');
        $pop_update->poptextcolor = $request->get('poptextcolor');
        $pop_update->courseid = $request->get('courseid');
        $pop_update->calltoaction = $request->get('calltoaction');
        $pop_update->save();
        return redirect('/dashboard/popoffers');
    }

    public function destroy($id)
    {
        $pop = Pop::find($id);
        $pop->delete();
        return redirect('/dashboard/popoffers');
    }
}
