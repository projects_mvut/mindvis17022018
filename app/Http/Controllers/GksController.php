<?php

namespace App\Http\Controllers;

use App\Http\Requests\GksRequest;
use App\Http\Controllers\Controller;
use App\Gk;

class GksController extends Controller
{
    public function index(){
        $recg=Gk::latest()->get();
        return view('gks.index',compact('recg'));
    }

    public function show(Gk $gk){
        return view('gks.show',compact('gk'));
    }

    public function create(){
        return view('gks.create');
    }

    public function store(GksRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);        
        }
        include 'imagefunction.php';
        Gk::create($input);
        return redirect('gks')->with('status','News Created');
    }

    public function edit(Gk $gk){
        return view('gks.edit',compact('gk'));
    }

    public function update(Gk $gk,GksRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        include 'imagefunction.php';
        $gk->update($input);
        return redirect('gks/'.$gk->slug)->with('status','News Updated');
    }

    public function delete(Gk $gk){
        $gk->delete();
        return redirect('gks')->with('status-danger','News Deleted');
    }
    
}
