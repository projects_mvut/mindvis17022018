<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Email;
class EmailController extends Controller
{
    public function index(){
        $emails=Email::latest()->get();
        return view('mailer.index',compact('emails'));
    }

    public function create(){
        return view('mailer.create');
    }

    public function edit(Email $email){
        return view('mailer.edit',compact('email'));
    }


    public function save(Request $request){
        $input=$request->all();
        // dd($input);
        Email::create($input);
        return redirect('dashboard/emails');
    }

    public function update(Email $email, Request $request){
        $input=$request->all();
        $email->update($input);
        return redirect('dashboard/emails');
    }

    public function delete(Email $email){
        $email->delete();
        return redirect('dashboard/emails');
    }
}
