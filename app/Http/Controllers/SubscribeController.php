<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\User;

class SubscribeController extends Controller
{
    public function add(Request $request){
        $input=$request->all();
        $url='https://www.instamojo.com/api/1.1/payments/'.$input['payment_id'].'/';
        $data = curl_init($url);
        curl_setopt($data, CURLOPT_HTTPHEADER, array(
            'X-Api-Key: 667e07e9b770c50cae0ad0d3053a5e87',
            'X-Auth-Token: 124e2bb0f034a2a3bc818d43fc674bd7'
          ));
        curl_setopt($data,CURLOPT_RETURNTRANSFER,1);
        $result =curl_exec($data);
        $res=json_decode($result,true);
        $slug=$res['payment']['link_slug'];
        $email=$res['payment']['buyer_email'];
        $course=Course::where('instamojo_button',$slug)->first();
        if(!$course){
            $course=Course::where('instamojo_self',$slug)->first();
        }
        $user=User::where('email',$email)->first();
        if(!$user->courses->contains($course->id)){
            $user->courses()->attach($course->id);
        }
        return redirect('courses/'.$course->slug)->with('status','Congratulations you\'ve subscribed to this course.');
    }
}
