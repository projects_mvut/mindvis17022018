<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TakeQuizRequest;
use App\Http\Controllers\Controller;
use App\Course;
use App\Quiz;
use App\Question;
use App\Test;
use App\Attempt;
use Auth;
use Carbon\Carbon;
class TakeQuizController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function start(Quiz $quiz){
        $input['user_id']=Auth::user()->id;
        $input['quiz_id']=$quiz->id;
        Test::create($input);
        $firstQuestion=$quiz->questions->first();
        $url='quiz/'.$quiz->slug.'/question/'.$firstQuestion->question_number;
        return redirect($url);
    }

    public function show(Quiz $quiz,$qno){
        $test=Test::where('quiz_id',$quiz->id)->where('user_id',Auth::user()->id)->latest()->first();
        if(!$test){
            $course=Course::where('id',$quiz->course_id)->first();
            return redirect('courses/'.$course->slug.'/quizzes/'.$quiz->slug)->with('status-alert','Start from begining');
        }
        $duration=$quiz->hours*3600+$quiz->minutes*60+$quiz->seconds;
        $time_taken= ($test->created_at->diffInSeconds(Carbon::now()));
        $time=$duration-$time_taken;
        $hours=intval($time/3600);
        $time=$time%3600;
        $minutes=intval($time/60);
        $seconds=$time%60;
        
        $question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
        $answer=Attempt::where('test_id',$test->id)->where('qno',$qno)->first();
        // dd($answer->toArray());
        if($answer){
            $answer=$answer->answer;
        }
        $questions=Question::where('quiz_id',$quiz->id)->order()->get();
        $nextQuestion=Question::where('quiz_id',$quiz->id)->where('question_number','>',$qno)->min('question_number');
        if($nextQuestion){
            $nexturl='quiz/'.$quiz->slug.'/question/'.$nextQuestion;
        }
        else{
            $firstQuestion=Question::where('quiz_id',$quiz->id)->first();
            $nexturl='quiz/'.$quiz->slug.'/question/'.$firstQuestion->question_number;
        }
        return view('takequiz.show',compact('quiz','question','questions','hours','minutes','seconds','answer','nexturl','test'));
    }

    public function store(TakeQuizRequest $request,Quiz $quiz,$qno){
        $test=Test::where('quiz_id',$quiz->id)->where('user_id',Auth::user()->id)->latest()->first();
        if(!$test){
            $course=Course::where('id',$quiz->course_id)->first();
            return redirect('courses/'.$course->slug.'/quizzes/'.$quiz->slug)->with('status-alert','Start from begining');
        }
        $question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
        $input=$request->all();
        if($input['submit'] =="Submit & Next"){
            $input['marked']=0;
        }
        else{
            $input['marked']=1;
        }
        $attempt=Attempt::where('test_id',$test->id)->where('qno',$qno)->first();
        if($attempt){
            $attempt->update(['answer'=>$input['answer'],'marked'=>$input['marked']]);
        }
        else{
            Attempt::create(['qno'=>$qno,'answer'=>$input['answer'],'marked'=>$input['marked'],'test_id'=>$test->id]);    
        }
        $nextQuestion=Question::where('quiz_id',$quiz->id)->where('question_number','>',$qno)->min('question_number');
        if($nextQuestion){
            $url='quiz/'.$quiz->slug.'/question/'.$nextQuestion;
        }
        else{
            $firstQuestion=Question::where('quiz_id',$quiz->id)->order()->first();
            $url='quiz/'.$quiz->slug.'/question/'.$firstQuestion->question_number;
        }
            return redirect($url);
    }

    public function finished(Quiz $quiz){
        $test=Test::where('quiz_id',$quiz->id)->where('user_id',Auth::user()->id)->latest()->first();
        $course=$quiz->course;
        if(!$test){
            return redirect('courses/'.$course->slug.'/quizzes/'.$quiz->slug)->with('status-alert','Start from begining');
        }
        $answers=$test->attempts;
        $questions=$quiz->questions;
        $total_marks=0;
        foreach($questions as $question){
            $total_marks=$total_marks+$question->positive_marks;
        }
        $marks=0;
        foreach($answers as $answer){
            $question=$quiz->questions->where('question_number',$answer->qno)->first();
            if($answer->answer==$question->correct_answer){
                $marks=$marks+$question->positive_marks;
            }
            else{
                $marks=$marks-$question->negative_marks;
            }
        }
        $test->update();
        return view('takequiz.finished',
            compact('quiz','marks','total_marks','course','answers','questions','test'));
    }


public function show_ques(Quiz $quiz){
       
  $test=Test::where('quiz_id',$quiz->id)->where('user_id',Auth::user()->id)->latest()->first();
        $course=$quiz->course;
        if(!$test){
            return redirect('courses/'.$course->slug.'/quizzes/'.$quiz->slug)->with('status-alert','Start from begining');
        }
        $answers=$test->attempts;
        $questions=$quiz->questions;
        $total_marks=0;
        foreach($questions as $question){
            $total_marks=$total_marks+$question->positive_marks;
        }
        $marks=0;
        foreach($answers as $answer){
            $question=$quiz->questions->where('question_number',$answer->qno)->first();
            if($answer->answer==$question->correct_answer){
                $marks=$marks+$question->positive_marks;
            }
            else{
                $marks=$marks-$question->negative_marks;
            }
        }
        $test->update();

return view("takequiz.showques",compact('quiz','course','test','questions','answers'));
    }




}
