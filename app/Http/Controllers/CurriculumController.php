<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SubjectRequest;
use App\Http\Controllers\Controller;
use App\Course;
use App\Subject;

class CurriculumController extends Controller
{
    public function index(Course $course){
        $subjects=$course->subjects()->get();
        $usections=$course->sections()->where('subject_id',null)->get();
        $fontcolor="#333";
        $color="#CEF9EE;";
        if($course->curriculum_color){
            $color=$course->curriculum_color;
        }
        if($course->curriculum_text_color){
            $fontcolor=$course->curriculum_text_color;
        }
        return view('curriculum.index',compact('course','subjects','usections','color','fontcolor'));
    }

    public function createSubject(Course $course){
        return view('subject.create',compact('course'));
    }

    public function storeSubject(Course $course, SubjectRequest $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $input['course_id']=$course->id;
        Subject::create($input);
        return redirect('courses/'.$course->slug.'/curriculum')->with('status','Subject Created');
    }

    public function editSubject(Course $course,Subject $subject){
        return view('subject.edit',compact('course','subject'));
    }

    public function updateSubject(Course $course, Subject $subject, Request $request){
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val);            
        }
        $input['course_id']=$course->id;
        $subject->update($input);
        return redirect('courses/'.$course->slug.'/curriculum')->with('status','Subject Updated');
    }

    public function deleteSubject(Course $course, Subject $subject){
        $subject->delete();
        return redirect('courses/'.$course->slug.'/curriculum')->with('status-danger','Subject Deleted');
    }
}
