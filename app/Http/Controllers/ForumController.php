<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Forum;
use App\Http\Requests;
use App\Http\Requests\ForumRequest;
use App\Http\Requests\AnswerRequest;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{
    
    public function index()
    {
        $forums=Forum::latest()->get();
        return view('forums.index',compact('forums'));
    }

  
    public function store(ForumRequest $request)
    {
        $input=$request->all();
        $val=$input['title'];
        $input['slug']=str_slug($val);
        if(Forum::where('slug',$input['slug'])->exists()){
              $input['slug']=$input['slug'].'-'.str_random(4);
        }  
        $input['user_id']=Auth::user()->id;
        Forum::create($input);
        return redirect('forums')->with('status', 'Forum Created!');
    }

   
    public function show(Forum $forum)
    {
        // dd(Auth::user()->id);
        $forums=Forum::latest()->get();
        $answers=$forum->answers()->latest()->get();
        $user=User::where('id',$forum->user_id)->first();
        return view('forums.show',compact('forum','forums','answers','user'));
    }

    
    public function destroy(Forum $forum)
    {
        $forum->delete();
        return redirect('forums')->with('status-danger', 'Forum Deleted!');
    }


    public function add_answer(AnswerRequest $request,Forum $forum){
        $input=$request->all();
        $input['user_id']=Auth::user()->id;
        $input['forum_id']=$forum->id;
        \App\Answer::create($input);
        return redirect('forums/'.$forum->slug)->with('status', 'Answer Added!');
    }

}