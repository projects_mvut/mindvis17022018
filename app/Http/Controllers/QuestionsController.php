<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Http\Controllers\Controller;
use App\Course;
use App\Quiz;
use App\Question;

class QuestionsController extends Controller
{
   public function create(Course $course, Quiz $quiz){
    return view('questions.create',compact('course','quiz'));
   }

   public function store(Course $course, Quiz $quiz , QuestionRequest $request){
   	$input=$request->all();
   	$input['quiz_id']=$quiz->id;
   	Question::create($input);
      $section=$quiz->section()->first();
      $subject=$section->subject()->first();
   	return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/quiz/'.$quiz->slug)->with('status','Question Added');
   }

   public function show(Course $course, Quiz $quiz, $qno){
      $question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
      return view('questions.show',compact('course','quiz','question'));
   }

   public function edit(Course $course, Quiz $quiz,$qno){
      $question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
      return view('questions.edit',compact('course','quiz','question'));
   }

   public function update(Course $course, Quiz $quiz , $qno, QuestionRequest $request){
      $question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
      $input=$request->all();
      $input['quiz_id']=$quiz->id;
      $question->update($input);
      return redirect('courses/'.$course->slug.'/quizzes/'.$quiz->slug.'/questions/'.$question->question_number)->with('status','Question Updated');
   }

   public function delete(Course $course, Quiz $quiz , $qno){
   	$question=Question::where('quiz_id',$quiz->id)->where('question_number',$qno)->firstorfail();
      $question->delete();
      $section=$quiz->section()->first();
      $subject=$section->subject()->first();
      return redirect('courses/'.$course->slug.'/subject/'.$subject->slug.'/section/'.$section->slug.'/quiz/'.$quiz->slug)->with('status-danger','Question Deleted');
   }
}
