<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    public function create()
    {
        return view('categories.create');
    }


    public function store(CategoryRequest $request)
    {
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val); 
        }
        Category::create($input);
        return redirect('categories')->with('status', 'Category Created!');
    }


    public function show(Category $category)
    {
        $categories=Category::all();
        $courses=$category->courses()->published()->latest()->get();
        return view('categories.show',compact('category','courses','categories'));
    }

    public function edit(Category $category)
    {
        return view('categories.edit',compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $input=$request->all();
        if(!$input['slug']){
            $val=$input['title'];
            $input['slug']=str_slug($val); 
        }
        $category->update($input);
        return redirect('categories/'.$category->slug)->with('status', 'Category Updated!');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect('categories')->with('status-danger', 'Category Deleted!');
    }
}
