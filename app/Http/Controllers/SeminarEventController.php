<?php

namespace App\Http\Controllers;

use App\Http\Requests\SeminarEventRequest;
use App\Http\Controllers\Controller;
use Mail;

class SeminarEventController extends Controller
{
    public function index(){
        return view('seminar.index');
    }

    public function store(SeminarEventRequest $request){
        $user=$request->all();
        // dd($user);
        Mail::queue('emails.seminar',['user'=>$user],
                function($m){
                    $m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
                    ->cc('sonali.vijan@gmail.com','Sonali Vijan')
                    ->subject('Mindvis Seminar Register Notification');
        });
        return redirect()->back()->with('status','Registerd');
    }

    public function ibps(){
        return view('seminar.ibps');
    }
}
