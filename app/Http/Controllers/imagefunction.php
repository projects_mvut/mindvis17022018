<?php
if($request->hasFile('url')){
            $slug=$input['slug'];
            $imageName = $slug.'-'.str_random(8).'.'.$request->file('url')->getClientOriginalExtension();
            $request->file('url')->move(
            base_path() . '/public/uploads/images/',$imageName);
            $input['url']='/uploads/images/'.$imageName;
}

if($request->hasFile('banner')){
            $slug=$input['slug'];
            $imageName = $slug.'-banner-'.str_random(8).'.'.$request->file('banner')->getClientOriginalExtension();
            $request->file('banner')->move(
            base_path() . '/public/uploads/banner/',$imageName);
            $input['banner']='/uploads/banner/'.$imageName;
}
?>