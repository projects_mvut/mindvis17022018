<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Mail;
use App\User;
use App\Course;
use App\Lecture;
use Carbon\Carbon;

class ScheduleController extends Controller
{

	public function updateSchedule(Course $course, User $user){
		Mail::queue('emails.schedule',['user'=>$user,'course'=>$course],
			function($m){
				$m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
				->cc('sonali.vijan@gmail.com','Sonali Vijan')
				->subject('Mindvis Schedule Update Request');
			});
		return redirect()->back()->with('status','Request Sent');
	}

	public function sendUpdateTest(Course $course){
		$this->sendUpdate($course,Auth::user());
		return redirect()->back()->with('status','Request Sent');
	}

	public function sendUpdate(Course $course, User $user){
		Mail::queue('emails.update',['user'=>$user,'course'=>$course],
			function($m) use ($user){
				$m->to($user->email, $user->username)
				->subject('Mindvis Schedule Updated');
			});
		return redirect()->back()->with('status','Request Sent');
	}

	public function unassign(User $user, Lecture $lecture){
		$user->lectures()->detach($lecture->id);
		return redirect()->back()->with('status-alert','Unassigned');    
	}

	public function index(User $user, Course $course){
		return view('schedule.index',compact('user','course'));
	}

	public function add(User $user, Lecture $lecture,$days){
		$lipo=$user->lectures->find($lecture->id);
		if($lipo){
			$deadline=$lipo->pivot->deadline;
			$deadline=Carbon::createFromTimeStamp(strtotime($deadline));
			$deadline=$deadline->addDays($days);
			$user->lectures()->updateExistingPivot($lecture->id, ['deadline' => $deadline]);
		}
		else{
			$deadline=Carbon::now();
			$deadline=$deadline->addDays($days);
			$user->lectures()->attach($lecture->id, ['deadline' => $deadline, 'completed' => 'No']);
		}
		return redirect()->back()->with('status','Deadline Updated');
	}

	public function sub(User $user, Lecture $lecture,$days){
		$lipo=$user->lectures->find($lecture->id);
		if($lipo){
			$deadline=$lipo->pivot->deadline;
			$deadline=Carbon::createFromTimeStamp(strtotime($deadline));;
			$deadline=$deadline->subDays($days);
			$user->lectures()->updateExistingPivot($lecture->id, ['deadline' => $deadline]);
		}
		else{
			$deadline=Carbon::now();
			$deadline=$deadline->subDays($days);
			$user->lectures()->attach($lecture->id, ['deadline' => $deadline, 'completed' => 'No']);
		}
		return redirect()->back()->with('status','Deadline Updated');
	}

	public function today(User $user, Lecture $lecture){
		$lipo=$user->lectures->find($lecture->id);
		$deadline=Carbon::now();
		if($lipo){
			$user->lectures()->updateExistingPivot($lecture->id, ['deadline' => $deadline]);
		}
		else{
			$user->lectures()->attach($lecture->id, ['deadline' => $deadline, 'completed' => 'No']);
		}
		return redirect()->back()->with('status','Deadline Updated');
	}

	public function user(Course $course){
		$user=Auth::user();
		return view('schedule.user',compact('user','course'));
	}

	public function check(Lecture $lecture){
		$user=Auth::user();
		$user->lectures()->updateExistingPivot($lecture->id, ['completed' => 'Yes']);
		return redirect()->back()->with('status','Schedule Updated');
	}

	public function uncheck(Lecture $lecture){
		$user=Auth::user();
		$user->lectures()->updateExistingPivot($lecture->id, ['completed' => 'No']);
		return redirect()->back()->with('status','Schedule Updated');
	}
}
