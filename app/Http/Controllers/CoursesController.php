<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Course;
use App\Subject;
use App\Lecture;
use App\Category;
use App\Section;
use App\Quiz;
use App\Review;
use App\Assignment;
use App\Submission;
use App\User;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\TrialRequest;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use App\Trial;
use App\Pop;

class CoursesController extends Controller
{

	public function sane($data){
		unset($data['id']);
		unset($data['created_at']);
		unset($data['updated_at']);
		unset($data['course_id']);
		unset($data['subject_id']);
		unset($data['section_id']);
		return $data;
	}

	public function downloadCurriculum(Course $course){
		$subjects=$course->subjects;
		// dd($subjects);
		foreach($subjects as $subject){
			$sections=$subject->sections;
			foreach($sections as $section){
				$lectures=$section->lectures;
				$quizzes=$section->quizzes;
				foreach($lectures as $lecture){
					$lecture=$this->sane($lecture);
				}
				foreach($quizzes as $quiz){
					$quiz=$this->sane($quiz);
				}

				$section['lectures']=$lectures;
				$section['quizzes']=$quizzes;
				$section=$this->sane($section);
			}
			$subject['sections']=$sections;
			$subject=$this->sane($subject);
		}
		$output=$subjects->toJson();
		$filename=$course->title." Curriculum.json";
		$headers = array(
			'Content-Type' => 'text/json',
			'Content-Disposition' => 'attachment; filename='.$filename,
			);
		return Response::make($output, 200, $headers);
		// return Response::json($subjects, 200, array(), JSON_PRETTY_PRINT);
	}

	public function uploadCurriculum(Course $course,Request $request){
		Subject::where('course_id',$course->id)->delete();
		$input=$request->all();
		$suf=$input['affix'];
		// dd($suf);
		// if(isEmpty($suf)){
		// 	$sub=str_random(6);
		// }
		$curi=file($request->file('curriculum'))[0];
		$subjects=json_decode($curi,true);
		foreach($subjects as $subject){
			$sections=$subject['sections'];
			$subject['slug']=$suf.'-'.$subject['slug'];
			$subject['course_id']=$course->id;
			$subject=Subject::create($subject);
			foreach($sections as $section){
				$lectures=$section['lectures'];
				$quizzes=$section['quizzes'];
				$section['slug']=$suf.'-'.$section['slug'];
				$section['course_id']=$course->id;
				$section['subject_id']=$subject->id;
				$section=Section::create($section);
				foreach($lectures as $lecture){
					$lecture['slug']=$suf.'-'.$lecture['slug'];
					$lecture['course_id']=$course->id;
					$lecture['section_id']=$section->id;
					Lecture::create($lecture);
				}
				foreach($quizzes as $quiz){
					$quiz['slug']=$suf.'-'.$quiz['slug'];
					$quiz['course_id']=$course->id;
					$quiz['section_id']=$section->id;
					Quiz::create($quiz);
				}
			}
		}
		return redirect('courses/'.$course->slug)->with('status','Curriculum Uploaded');
	}

	public function trial(Course $course, TrialRequest $request){
		$user=$request->all();
		$name=$user['name'];
		if(Trial::where('email',$user['email'])->count()){
			return redirect()->back()->with('status-alert','Trials Request Already Sent');
		}
		if(!env('courseMailer')){
			if($course->slug=='gate-2018-mechanical-engineering-online-course'){
				Mail::queue('emails.trial_gate2018',['name'=>$name],function($m) use ($user){
					$m->to($user['email'],$user['name'])
					->subject('GATE MECHANICAL ENGINEERING 2018 ONLINE COURSE DETAILS');
				});
			}
			if($course->slug=='gate-2019-mechanical-engineering'){
				Mail::queue('emails.trial_gate2019',['name'=>$name],function($m) use ($user){
					$m->to($user['email'],$user['name'])
					->subject('GATE 2019 MECHANICAL ENGINEERING ONLINE COURSE DETAILS');
				});
			}
                        if($course->slug=='gate-2020-mechanical-engineering'){
				Mail::queue('emails.trial_gate2020',['name'=>$name],function($m) use ($user){
					$m->to($user['email'],$user['name'])
					->subject('GATE 2020 MECHANICAL ENGINEERING ONLINE COURSE DETAILS');
				});
			}
			if($course->slug=='upsc-ese-mechanical-engineering-2018-online-prep-course'){
				Mail::queue('emails.trial_upse',['name'=>$name],function($m) use ($user){
					$m->to($user['email'],$user['name'])
					->subject('UPSC ESE MECHANICAL ENGINEERING ONLINE COURSE DETAILS');
				});
			}
		}

		$user['subject']=$course->title;
		$user['phone']=$user['mobile'];

		Trial::create($user);

		if(!env('courseMailer')){
		Mail::queue('emails.trial',['course'=>$course,'user'=>$user],
			function($m) use ($user,$course){
				$m->to('himanshu.vasistha@gmail.com','Himanshu Vasistha')
				->cc('sonali.vijan@gmail.com','Sonali Vijan')
				->subject('Course '.$course->title.' Trial Request');
			});
		}
		return redirect()->back()->with('status','Your request has been sent. Please check your email for trial videos.');
	}

	public function emailstudents(Course $course, Request $request){
		$input=$request->all();
		$radio=$input['radio'];
		$content=$input['content'];
		$fileurl=0;
		if($radio=="0"){
			$users=$course->users()->where('is_active',false)->get();
		}
		elseif($radio=="1"){
			$users=$course->users()->where('is_active',true)->get();
		}
		else{
			$users=$course->users()->get();
		}
		if($request->hasFile('file')){
			$filename=$request->file('file')->getClientOriginalName();
			$request->file('file')->move(
				base_path() . '/public/uploads/files/',$filename);
			$fileurl= base_path() . '/public/uploads/files/'.$filename;
		}
		foreach($users as $user){
			Mail::queue('emails.emailstudents',['user'=>$user,'content'=>$content,'course'=>$course,'fileurl'=>$fileurl],
				function($m) use ($user,$content,$course,$fileurl){
					$m->to($user->email, $user->username)
					->subject('Course '.$course->title.' Email | Mindvis');
					if($fileurl){
						$m->attach($fileurl);
					}
				});
		}
		return redirect()->back()->with('status','Emails Sent');
	}

	public function popular(){
		$courses=Course::published()->with('reviews')->with('users')->get()->sortBy(function($course){
			return $course->reviews->count()+$course->users->count();
		},true,true);
		$categories=Category::all();
		return view('courses.index',compact('courses','categories'));
	}

	public function index(){
		$courses=Course::latest()->published()->get();
		$categories=Category::all();
		return view('courses.index',compact('courses','categories'));
	}


	public function create(){
		$select_category=Category::lists('title', 'id')->toArray();
		$categories=Category::all();
		return view('courses.create',compact('categories','select_category'));
	}


	public function store(CourseRequest $request)
	{
		$input=$request->all();
		$input['trial']=intval($input['trial']);
		if(!$input['slug']){
			$val=$input['title'];
			$input['slug']=str_slug($val);
		}
		include 'imagefunction.php';
		if($input['submit']=="Publish Now"){
			$input['is_published']=true;
			Auth::user()->courses()->create($input);
			return redirect('courses')->with('status', 'Course Created!');
		}
		elseif($input['submit']=="Publish Later"){
			$input['is_published']=false;
			Auth::user()->courses()->create($input);
			return redirect('courses')->with('status', 'Course Saved!');
		}
	}

	public function trialUser(Course $course){
		if(Auth::check() && Auth::user()->courses->contains($course->id)){
			return 0;
		}
		return 1;
	}

	public function show(Course $course)
	{
		$seo_title=$course->seo_title;
		$keywords=$course->keywords;
		$description=$course->description;
		$reviews=Review::where('course_id',$course->id)->latest()->get();
		$category=$course->category()->first();
		$assignments=$course->assignments;
		$show_banner=0;
		$show_trial=0;
		if($course->price){
			if($course->banner){
				$show_banner=1;
				if($course->trial){
					if($this->trialUser($course)){
						$show_trial=1;
					}
				}
			}
		}
        $pops=Pop::all()->toArray();
		return view('courses.show',compact('reviews','category','course','keywords','description','seo_title','assignments','show_banner','show_trial','pops'));
	}


	public function edit(Course $course)
	{
		$select_category=Category::lists('title', 'id')->toArray();
		$categories=Category::all();
		return view('courses.edit',compact('course','categories','select_category'));
	}


	public function update(CourseRequest $request, Course $course)
	{
		$input=$request->all();
		$input['trial']=intval($input['trial']);
		if(!$input['slug']){
			$val=$input['title'];
			$input['slug']=str_slug($val);
		}
		//Add image to UPLOADS
		include 'imagefunction.php';
		if($input['submit']=="Publish Now"){
			$input['is_published']=true;
			$course->update($input);
			return redirect('courses/'.$course->slug)->with('status', 'Course Updated!');
		}
		elseif($input['submit']=="Publish Later"){
			$input['is_published']=false;
			$course->update($input);
			return redirect('courses/'.$course->slug)->with('status', 'Course Saved!');
		}
	}


	public function destroy(Course $course)
	{
		$course->delete();
		return redirect('courses')->with('status-danger', 'Course Deleted!');
	}

	public function assignment(Course $course,Request $request){
		$input=$request->all();
		$course->assignments()->create($input);
		return redirect('courses/'.$course->slug)->with('status', 'Assignment Created!');
	}

	public function deleteAssignment($assignment){
		$assignment->delete();
		return redirect()->back()->with('status-danger', 'Assignment Deleted!');
	}
	public function assignments(Course $course){
		$assignments=$course->assignments;
		$uid=Auth::user()->id;
		// $a=$assignments[0]->self_submission();
		// dd($a->link);
		// dd($a->toArray());
		return view('users.assignments',compact('course','assignments','uid'));
	}

	public function submission(Course $course,Assignment $assignment, Request $request){
		$input=$request->all();
		// $input['assignment_id']=$assignment->id;
		$input['user_id']=Auth::user()->id;
		$submission=Submission::where('assignment_id',$assignment->id)->where('user_id',Auth::user()->id)->first();
		if($submission){
			$submission->update($input);
		}
		else{
			$assignment->submissions()->create($input);
		}
		return redirect()->back()->with('status','Assignment Submitted');
	}

	public function userAssignments(User $user, Course $course){
		$assignments=$course->assignments;
		return view('dashboard.assignments',compact('course','user','assignments'));
	}
}
