<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Test;
use App\User;
use App\Course;
use Auth;
class QuizReportController extends Controller
{
    public function __construct(){
        $this->middleware('testtaker',['only'=>['show']]);
    }

    public function index(){
        $tests=Test::with('quiz','user')->latest()->get();
        return view('report.index',compact('tests'));
    }

    public function show($test){
        $quiz=$test->quiz;
        $user=$test->user;
        $answers=$test->attempts;
        $course=$quiz->course;
        $questions=$quiz->questions;
        $total_marks=0;
        foreach($questions as $question){
            $total_marks=$total_marks+$question->positive_marks;
        }
        $marks=0;
        foreach($answers as $answer){
            $question=$quiz->questions->where('question_number',$answer->qno)->first();
            if($answer->answer==$question->correct_answer){
                $marks=$marks+$question->positive_marks;
            }
            else{
                $marks=$marks-$question->negative_marks;
            }
        }
        return view('report.show',compact('quiz','marks','total_marks','course','answers','questions','test','user'));
    }

    public function student(){
        $tests=Test::where('user_id',Auth::user()->id)->with('quiz')->latest()->get();
        return view('report.index',compact('tests'));
    }

    public function admin(User $user){
        $tests=Test::where('user_id',$user->id)->with('quiz')->latest()->get();
        return view('report.index',compact('tests'));
    }
}
