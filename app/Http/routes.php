<?php

if(env('HTTPS')){
	URL::forceSchema('https');
}

Route::get('search', array(
	'as'    =>  'search',
	'uses'  =>  'SearchController@index'
	));
Route::get('ibps-bank-po-2016-preparation-tips-ebook','PagesController@ebook');
Route::post('ibps-bank-po-2016-preparation-tips-ebook','PagesController@download_ebook');

Route::get('gate-2019-preparation-tips-free-ebook','PagesController@gateebook2019');
Route::post('gate-2019-preparation-tips-free-ebook','PagesController@email_gateebook2019');

Route::get('gate-2020-preparation-tips-free-ebook','PagesController@gateebook2020');
Route::post('gate-2020-preparation-tips-free-ebook','PagesController@email_gateebook2020');

Route::get('gate-mechanical-engineering-2018','PagesController@gatepage');
Route::post('gate-mechanical-engineering-2018','PagesController@gatemail');

Route::get('newsletter','PagesController@newsletter');
Route::post('newsletter','PagesController@newsletter');


Route::get('courses/popular','CoursesController@popular');
// Uses Admin Middleware
Route::group(['middleware' => 'admin'], function(){

	Route::get('sendUpdate/{course}/{user}','ScheduleController@sendUpdate');
	Route::get('sendUpdateTest/{course}','ScheduleController@sendUpdateTest');
	Route::group(['prefix' => 'dashboard'], function(){
	//schedule routes

		Route::get('userList','DashboardController@userlist');
		Route::get('downloaddb','DashboardController@downloaddb');
		Route::group(['prefix' => 'schedule/user/{user}/'], function(){
			Route::get('lecture/{lecture}/add/{days}','ScheduleController@add');
			Route::get('lecture/{lecture}/sub/{days}','ScheduleController@sub');
			Route::get('lecture/{lecture}/today','ScheduleController@today');
			Route::get('lecture/{lecture}/unassign','ScheduleController@unassign');

			Route::get('quiz/{quiz}/add/{days}','QuizScheduleController@add');
			Route::get('quiz/{quiz}/sub/{days}','QuizScheduleController@sub');
			Route::get('quiz/{quiz}/today','QuizScheduleController@today');
			Route::get('quiz/{quiz}/unassign','QuizScheduleController@unassign');
		});

		Route::get('newsletter','DashboardController@newsletter');
		Route::get('newsletter/download','DashboardController@downloadNewsletter');

		Route::post('schedule/{user}','DashboardController@upload_schedule');
		Route::get('quiz-report','QuizReportController@index');
		
		Route::get('home_seo','DashboardController@home_seo');
		Route::post('home_seo','DashboardController@save_home_seo');
		Route::get('admins','DashboardController@admins');
		Route::get('teachers','DashboardController@teachers');
		Route::get('users','DashboardController@users');

		Route::get('users/date',function(){
			return redirect()->back()->with('status-alert','Please select a date');
		});
		Route::get('users/date/{date}','DashboardController@date_users');
		Route::get('trials/date/{date}','TrialController@date_trials');

		Route::get('user/create','DashboardController@adduser');
		Route::post('user','DashboardController@storeuser');

		Route::get('userActivate/{user}/{course}/','DashboardController@active');
		Route::get('userDeactivate/{user}/{course}/','DashboardController@deactive');
		Route::get('assign/{user}/{course}/','DashboardController@assign');
		Route::get('author/{student}','DashboardController@author');
		Route::get('unassign/{user}/{course}/','DashboardController@unassign');
		Route::get('suspend/{teacher}','DashboardController@suspend');	
		Route::get('delete/{user}','DashboardController@delete');
		Route::get('courses/{course}/delete','DashboardController@delete_course');
		Route::get('courses/{course}/subscribed','DashboardController@subscribed');
		Route::get('courses/{course}/unsubscribed','DashboardController@unsubscribed');
		Route::get('teachers/{teacher}/courses','DashboardController@teacher_courses');
		Route::get('users/{student}/courses','DashboardController@user_courses');
		Route::get('courses/{course}/teachers','DashboardController@course_teachers');
		Route::get('images/{id}/delete','ImageController@delete');
	});

Route::get('categories/{category}/edit','CategoryController@edit');
Route::patch('categories/{category}','CategoryController@update');
Route::get('categories/{category}/delete','CategoryController@destroy');
Route::get('courses/{course}/delete','CoursesController@destroy');

Route::get('gks/create','GksController@create');
Route::post('gks','GksController@store');
Route::get('gks/{gk}/edit','GksController@edit');
Route::patch('gks/{gk}','GksController@update');
Route::get('gks/{gk}/delete','GksController@delete');
});

Route::get('dashboard/trials','TrialController@index');
Route::put('dashboard/trials/{trial}','TrialController@update');
Route::get('dashboard/trials/{trial}/delete','TrialController@delete');
Route::post('dashboard/trials/mail/{trial}','TrialController@mail');
Route::post('dashboard/users/mail/{user}','TrialController@usermail');
Route::post('dashboard/trials','TrialController@updateAll');

Route::get('dashboard/emails','EmailController@index');
Route::get('dashboard/emails/create','EmailController@create');
Route::post('dashboard/emails','EmailController@save');
Route::get('dashboard/emails/{email}/edit','EmailController@edit');
Route::patch('dashboard/emails/{email}','EmailController@update');
Route::get('dashboard/emails/{email}/delete','EmailController@delete');

Route::group(['middleware' => 'teacher'], function(){
	Route::get('categories/create','CategoryController@create');	
	Route::post('categories','CategoryController@store');
	Route::get('courses/create','CoursesController@create');
	Route::post('courses','CoursesController@store');	
	Route::post('courses/{course}/assignment','CoursesController@assignment');	
	Route::get('assignment/{assignment}/delete','CoursesController@deleteAssignment');	

	Route::resource('/dashboard/popoffers', 'PopController');


	Route::get('dashboard/images','ImageController@index');
	Route::post('dashboard/images','ImageController@store');
	Route::get('articles/create','ArticlesController@create');
	Route::post('articles','ArticlesController@store');
	Route::get('dashboard/schedule/user/{user}/course/{course}','ScheduleController@index');
	Route::get('dashboard/assignments/user/{user}/course/{course}','CoursesController@userAssignments');
	Route::get('quiz-report/user/{user}','QuizReportController@admin');
});

Route::group(['middleware' => 'courseauthor'], function(){
	Route::get('quiz/{quiz}/draft','QuizController@draft');
	Route::get('quiz/{quiz}/live','QuizController@live');

	Route::group(['prefix' => 'courses/{course}'], function(){
		Route::get('downloadCurriculum','CoursesController@downloadCurriculum');
		Route::post('uploadCurriculum','CoursesController@uploadCurriculum');
		Route::post('emailstudents','CoursesController@emailstudents');
		Route::get('subject/create','CurriculumController@createSubject');
		Route::post('subject','CurriculumController@storeSubject');
		Route::get('subject/{subject}/edit','CurriculumController@editSubject');
		Route::patch('subject/{subject}','CurriculumController@updateSubject');
		Route::get('subject/{subject}/delete','CurriculumController@deleteSubject');
	});

	Route::group(['prefix' => 'courses/{course}/subject/{subject}'], function(){
		Route::get('section/create','SectionController@create');
		Route::post('section','SectionController@store');
		Route::get('section/{section}/edit','SectionController@edit');
		Route::patch('section/{section}','SectionController@update');
		Route::get('section/{section}/delete','SectionController@delete');
	});

	Route::group(['prefix' => 'courses/{course}/subject/{subject}/section/{section}'], function(){
		Route::get('quiz/create','QuizController@create');
		Route::post('quiz','QuizController@store');
		Route::get('quiz/{quiz}/edit','QuizController@edit');
		Route::patch('quiz/{quiz}','QuizController@update');
		Route::get('quiz/{quiz}/delete','QuizController@delete');
		

		Route::get('lecture/create','LecturesController@create');
		Route::post('lecture','LecturesController@store');
		Route::get('lecture/{lecture}/edit','LecturesController@edit');
		Route::patch('lecture/{lecture}','LecturesController@update');
		Route::get('lecture/{lecture}/delete','LecturesController@destroy');

	});
	Route::get('dashboard/courses/{course}/users','DashboardController@course_users');
	Route::get('courses/{course}/edit','CoursesController@edit');
	Route::patch('courses/{course}/','CoursesController@update');

	Route::get('courses/{course}/quizzes/{quiz}/questions/create','QuestionsController@create');
	Route::post('courses/{course}/quizzes/{quiz}/questions','QuestionsController@store');
	Route::get('courses/{course}/quizzes/{quiz}/questions/{question}','QuestionsController@show');
	Route::get('courses/{course}/quizzes/{quiz}/questions/{question}/edit','QuestionsController@edit');
	Route::patch('courses/{course}/quizzes/{quiz}/questions/{question}','QuestionsController@update');
	Route::get('courses/{course}/quizzes/{quiz}/questions/{question}/delete','QuestionsController@delete');
	Route::post('addcsv/{quiz}','QuizController@quizcsv');
	Route::get('downloadcsv/{quiz}','QuizController@downloadcsv');
}); //middleware courseauthor ends




Route::group(['middleware' => 'artauthor'], function(){
	Route::get('dashboard/articles/{article}/delete','DashboardController@delete_article');
	Route::get('articles/{article}/edit','ArticlesController@edit');
	Route::patch('articles/{article}','ArticlesController@update');
	Route::get('articles/{article}/delete','ArticlesController@destroy');
});

Route::group(['middleware' => 'forumauthor'], function(){
	Route::get('forums/{forum}/delete','ForumController@destroy');
});

Route::group(['middleware' => 'reviewauthor'], function(){
	Route::get('courses/{course}/reviews/{review}/delete','ReviewsController@delete');
});

Route::group(['middleware' => 'subscriber'], function(){
	Route::get('courses/{course}/subject/{subject}/section/{section}/lecture/{lecture}','LecturesController@show');
	Route::get('courses/{course}/subject/{subject}/section/{section}/quiz/{quiz}','QuizController@show');
	Route::get('quiz/{quiz}/start','TakeQuizController@start');
	Route::get('quiz/{quiz}/question/{q}','TakeQuizController@show');
	Route::post('quiz/{quiz}/question/{q}','TakeQuizController@store');
	Route::get('quiz/{quiz}/finished','TakeQuizController@finished');
	Route::get('quiz/{quiz}/showques','TakeQuizController@show_ques');
});

Route::group(['middleware' => 'auth'], function(){

	Route::get('updateSchedule/{course}/{user}','ScheduleController@updateSchedule');
	Route::get('schedule/download','DashboardController@download_schedule');
	
	Route::get('assignments/{course}','CoursesController@assignments');
	Route::get('quiz-report','QuizReportController@student');
	Route::get('quiz-report/{test}','QuizReportController@show');
	Route::post('assignments/{course}/submit/{assignment}','CoursesController@submission');
	Route::get('schedule/{course}','ScheduleController@user');
	Route::get('schedule/lecture/{lecture}/check','ScheduleController@check');
	Route::get('schedule/lecture/{lecture}/uncheck','ScheduleController@uncheck');
	Route::get('schedule/quiz/{quiz}/check','QuizScheduleController@check');
	Route::get('schedule/quiz/{quiz}/uncheck','QuizScheduleController@uncheck');

	Route::post('courses/{course}/review','ReviewsController@store');
	Route::get('dashboard',function(){
		return redirect('dashboard/courses');
	});
	Route::get('dashboard/courses','DashboardController@courses');
	Route::get('dashboard/blogs','DashboardController@blogs');	
	Route::get('dashboard/course_interested','DashboardController@course_interested');	
	Route::post('dashboard/course_interested','DashboardController@store_course_interested');	
	Route::get('dashboard/mobile','DashboardController@mobile');	
	Route::post('dashboard/mobile','DashboardController@storemobile');	
	Route::post('dashboard/mobileVerify','DashboardController@verifymobile');	
	Route::get('dashboard/resendVerificationCode','DashboardController@resendOtp');	
	
	Route::get('forums/create','ForumController@create');
	Route::post('forums','ForumController@store');
	Route::post('forums/{forum}','ForumController@add_answer');
	Route::post('user/pic','UserpicController@add');
	Route::get('subscribe','SubscribeController@add');
});

//public
Route::get('home',function(){
	return redirect('/');
});

Route::get('/', 'PagesController@index');

Route::post('blog','BlogController@save');
Route::get('blog/{blog}/delete','BlogController@delete');
Route::get('blog/{blog}','BlogController@show');

Route::get('courses','CoursesController@index');
Route::get('courses/{course}','CoursesController@show');
Route::post('courses/{course}/trial','CoursesController@trial');
Route::get('articles/page/{page}','ArticlesController@index');
Route::get('articles/{article}','ArticlesController@show');


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('register/verify/{token}', 'RegisterController@verify');
// Route::get('register/activate', 'RegisterController@activate');


// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('categories',function(){
	return redirect('courses');
});
Route::get('categories/{category}','CategoryController@show');
Route::get('forums','ForumController@index');
Route::get('forums/{forum}','ForumController@show');
Route::get('login/facebook', 'SocialController@redirectToFacebook');
Route::get('facebook', 'SocialController@handleFacebookCallback');
Route::get('login/google', 'SocialController@redirectToGoogle');
Route::get('google', 'SocialController@handleGoogleCallback');

Route::get('helpdesk','HelpdeskController@form');
Route::post('helpdesk','HelpdeskController@submit');

Route::get('privacy',function(){
	return view('pages.privacy');
});

Route::get('terms',function(){
	return view('pages.terms');
});

Route::get('vision',function(){
	return view('pages.vision');
});

Route::get('about-us',function(){
	return view('pages.team');
});

Route::get('gks','GksController@index');
Route::get('gks/{gk}','GksController@show');

Route::get('user/{user}','ProfileController@show');
Route::patch('user/{user}','ProfileController@update');
Route::patch('user/{user}/password','ProfileController@password');

Route::get('confirm','ConfirmController@index');

Route::get('ibps-2016','SeminarEventController@index');
Route::get('ibps-exam-2016','SeminarEventController@ibps');
Route::post('ibps-2016','SeminarEventController@store');

//comments
Route::post('lectures/{lecture}/comment','LcommentController@store');
Route::get('comment/{lcomment}/delete','LcommentController@delete');

Route::get('courses/{course}/curriculum','CurriculumController@index');

Route::get('courses/{course}/subject/{subject}/section/{section}','SectionController@show');