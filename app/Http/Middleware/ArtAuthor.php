<?php

namespace App\Http\Middleware;
use \Auth;
use Closure;

class ArtAuthor
{

    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort('404');
        }
        elseif(Auth::user()->is_admin){
            return $next($request);            
        }
        $article=$request->route('article');
        $user_id=$article->user_id;
        if((Auth::user()->id == $user_id) and Auth::user()->is_teacher){
            return $next($request);            
        }       
        else{
            return abort('404');
        }
    }
}
