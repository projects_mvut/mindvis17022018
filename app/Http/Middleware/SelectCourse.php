<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class SelectCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('auth/login');
        }
        if(Auth::user()->is_admin or Auth::user()->is_teacher){
            return $next($request);
        }
        if(Auth::check()){
            if(Auth::user()->course_interested){
                return $next($request);
            }
            else{
                return redirect('dashboard/course_interested')->with('status-alert','Please Select Course');
            }
        }
        else{
            return redirect('dashboard/course_interested')->with('status-alert','Please Select Course');
        }
    }
}
