<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserSession
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$userhash   = \Session::get('userhash');
		$sessionId = \Session::getId();
		
		if (!auth()->guest() && auth()->user()->user_hash != $userhash) {
			\Session::getHandler()->destroy($sessionId);
			// return redirect()->intended($request->getUri());
			\Auth::logout();
			return redirect('auth/login')->with('status-alert','Only single login is allowed');
		}		
		return $next($request);
	}
}
