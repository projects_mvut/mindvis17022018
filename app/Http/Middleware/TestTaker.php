<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TestTaker
{
	public function handle($request, Closure $next)
	{
		if(!Auth::check()){
			return abort('404');
		}
		elseif(Auth::user()->is_admin){
			return $next($request);            
		}
		elseif(Auth::user()->is_teacher){
			return $next($request);            
		}
		// dd($request);
		$test=$request->route('test');
		$user_id=$test->user_id;
		if((Auth::user()->id == $user_id)){
			return $next($request);            
		}       
		else{
			return abort('404');
		}
	}
}
