<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('auth/login');
        }
        if(Auth::user()->is_admin){
            return $next($request);
        }
        $user=$request->route('user');
        if($user){
            if(!(Auth::user()->id==$user->id)){
                return redirect('user/'.Auth::user()->id)->with('status-alert','Not Authorised');
            }
            if(Auth::user()->id==$user->id){
                return $next($request);
            }
        }
        else{
            return redirect('dashboard')->with('status-alert','Not Authorized');
        }
    }
}
