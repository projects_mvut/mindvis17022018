<?php

namespace App\Http\Middleware;
use \Auth;
use Closure;

class CourseAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort('404');
        }
        elseif(Auth::user()->is_admin){
            return $next($request);
        }
        
        $course_id=$request->route('course')->id;
        
        if(Auth::user()->is_teacher){
            if(Auth::user()->courses->contains($course_id)){
            return $next($request);
            }
            else{
                return redirect('/courses/'.$request->route('course')->slug)->with('status-alert','You are not assigned to this course');
            }
        }
        
        else{
            return abort('404');
        }
    }
}
