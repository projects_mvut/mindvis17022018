<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Review;
class ReviewAuthor
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort('404');
        }
        $course=$request->route('course');
        $review=$request->route('review');
        $user_id=$review->user_id;
        if(Auth::user()->id == $user_id){
            return $next($request);          
        }
        elseif(Auth::user()->is_admin){
            return $next($request);            
        }
        else{
            return abort('404');
        }
        
    }
}
