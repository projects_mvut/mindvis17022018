<?php

namespace App\Http\Middleware;
use \Auth;
use Closure;

class ForumAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort('404');
        }
        elseif(Auth::user()->is_admin){
            return $next($request);            
        }
        $forum=$request->route('forum');
        $user_id=$forum->user_id;
        if(Auth::user()->id == $user_id){
            return $next($request);            
        }
        else{
            return abort('404');
        }
        
    }
}
