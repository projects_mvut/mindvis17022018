<?php

namespace App\Http\Middleware;
use \Auth;
use Closure;

class Teacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return abort('404');
        }
        elseif(Auth::user()->is_teacher or Auth::user()->is_admin){
            return $next($request);
        }
        else{
            return abort('404');
        }
    }
}
