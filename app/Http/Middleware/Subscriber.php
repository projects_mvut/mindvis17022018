<?php

namespace App\Http\Middleware;
use \Auth;
use Closure;
use App\Course;
class Subscriber
{

    public function handle($request, Closure $next)
    {
        $course=$request->route('course');
        $lecture=$request->route('lecture');
        $quiz=$request->route('quiz');
        if($course){
            $course_id=$course->id;
        }
        else{
            $course_id=$quiz->course_id;
            $course=Course::where('id',$course_id)->first();
        }
        $message_guest='Liked what you saw ? <p> For the full Course </p><p><span class="sub-span">Login</span> and <span class="sub-span">Subscribe</span> to '.$course->title.' for unlimited access.</p><p>Need Help in Signing Up ?</p><p> Call us at <span class="sub-span">09779434433</span>';
        $message_user='Liked what you saw ? <p> For the full Course </p><p><span class="sub-span">Subscribe</span> to '.$course->title.' for unlimited access.</p>';
        if($lecture and $lecture->is_public){
            return $next($request);
        }
        if($quiz and $quiz->is_public){
            return $next($request);
        }
        if(!Auth::check()){
            // return redirect('courses/'.$course->slug)->with('status-alert','alert');
            return redirect('courses/'.$course->slug)->with('status-modal',$message_guest);
        }
        if(Auth::user()->is_admin){
            return $next($request);            
        }
        elseif(Auth::user()->is_teacher){
            return $next($request);            
        }              
        if($course){
            $course_id=$course->id;
        }
        else{
            $course_id=$quiz->course_id;            
        }
        if(Auth::user()->courses->contains($course_id)){
            return $next($request);
        }       
        else{
            return redirect('courses/'.$course->slug)->with('status-modal',$message_user);
        }
    }
}
