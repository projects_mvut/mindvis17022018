<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable=['title','course_id','link'];

    public function course(){
    	return $this->belongsTo('App\Course');
    }

    public function submissions(){
    	return $this->hasMany('App\Submission');
    }

    public function link($id){
    	$s=$this->hasMany('App\Submission')->where('user_id',$id)->first();
    	if($s){
    		return $s->link;
    	}
    	return null;
    }
}
