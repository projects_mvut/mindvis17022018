<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name','last_name','username', 'email', 'password','is_admin','is_teacher','slug','is_confirmed','confirmation_code','url','mobile','vmobile','otp','course_interested','schedule_url','address1','address2','city','state','pin','user_hash'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function swap(){
		$hash = bcrypt(auth()->user()->getKey().microtime());
		
		\Session::put('userhash', $hash);
		
		$this->user_hash = $hash;
		$this->save();
	}

	public function courses(){
		return $this->belongsToMany('App\Course');
	}

	public function lectures(){
		return $this->belongsToMany('App\Lecture')->withPivot('deadline', 'completed');
	}

	public function quizzes(){
		return $this->belongsToMany('App\Quiz')->withPivot('deadline', 'completed');
	}

	public function articles(){
		return $this->hasMany('App\Article');
	}

	public function forums(){
		return $this->hasMany('App\Forum');
	}

	public function answers(){
		return $this->hasMany('App\Answer');
	}

	public function scopeTeacher($query){
		return $query->where('is_teacher',true);
	}

	public function scopeStudent($query){
		return $query->where('is_teacher',false)->where('is_admin',false);
	}

	public function scopeOrder($query){
		return $query->orderBy('first_name','asc');
	}
}
