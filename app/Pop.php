<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pop extends Model
{
     protected $primaryKey = 'id';
	    protected $fillable=['poptext','popbgcolor','poptextcolor','courseid','calltoaction'];

	public function Course() 
    	{
    	return $this->hasMany('App\Course');
	}
}
