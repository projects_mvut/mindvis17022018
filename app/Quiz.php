<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable=['title','preview','course_id','section_id','slug','hours','minutes','seconds','is_public','drafted'];
    public function course(){
    	return $this->belongsTo('App\Course');
    }

    public function section(){
    	return $this->belongsTo('App\Section');
    }

    public function questions(){
    	return $this->hasMany('App\Question')->orderBy('question_number');
    }

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('deadline', 'completed');
    }
}
