<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracker extends Model
{
	protected $fillable=['email_id','trial_id'];
}