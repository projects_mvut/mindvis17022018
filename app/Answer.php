<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable=['content','forum_id','user_id'];

    public function forums(){
    	return $this->belongsTo('App\Forum');
    }

    public function user(){
    	return $this->belongsTo('\App\User');
    }
}
