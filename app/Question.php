<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable=['question_number','type','title','content','correct_answer','quiz_id','option1','option2','option3','option4','solution','positive_marks','negative_marks'];

    public function scopeOrder($query){
        return $query->orderBy('question_number');
    }

    public function quizzes(){
    	return $this->belongsTo('App\Quiz');
    }

}
