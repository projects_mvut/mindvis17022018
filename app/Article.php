<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable=['title','content','is_published','user_id','slug','url','preview','banner','keywords','description','seo_title','blog_id'];

    public function scopePublished($query){
    	return $query->where('is_published',true);
    }

	public function scopeDrafted($query){
	    	return $query->where('is_published',false);
	}

	public function users(){
		return $this->belongsTo('App\Article');
	}

	public function blog(){
		return $this->belongsTo('App\Blog');
	}

}
