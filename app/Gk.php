<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gk extends Model
{
    protected $fillable=['title','content','slug','preview','url'];
}