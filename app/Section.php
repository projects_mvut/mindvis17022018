<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable=['title','course_id','subject_id','preview','slug','content'];

    public function lectures(){
    	return $this->hasMany('App\Lecture');
    }

    public function course(){
    	return $this->belongsTo('App\Course');
    }

    public function subject(){
        return $this->belongsTo('App\Subject');
    }
    
    public function quizzes(){
        return $this->hasMany('App\Quiz');
    }
}
