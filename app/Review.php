<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable=['content','user_id','course_id','rating'];
    protected $with = ['user'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
