<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trial extends Model
{
    protected $fillable=['name','email','phone','subject','detail','action'];

    public function trackers(){
    	return $this->hasMany('App\Tracker');
    }
}
