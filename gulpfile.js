process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir(function(mix) {
	mix.sass('app.scss')
	.scripts([
		'jquery/dist/jquery.min.js',
		'jquery-ui/jquery-ui.min.js',
		'bootstrap-sass/assets/javascripts/bootstrap.min.js',
		'raty/lib/jquery.raty.js',
		],
		'resources/assets/js/vendor.js','vendor/bower_components')
	.scripts(['vendor.js','main.js'])
	.version(['css/app.css','js/all.js']);
	});
