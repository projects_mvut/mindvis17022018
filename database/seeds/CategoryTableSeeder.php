<?php
use Illuminate\Database\Seeder;
class CategoryTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('categories')->insert([
                'title'=>'Engineering',
                'preview'=>'Engineering Courses',
                'slug'=>'engg',
            ]);     

        DB::table('categories')->insert([
                'title'=>'Humanities',
                'preview'=>'Humanities Courses',
                'slug'=>'humanities',
            ]);

        DB::table('categories')->insert([
                'title'=>'Science',
                'preview'=>'Science Courses',
                'slug'=>'science',
            ]);     
    }
 
}