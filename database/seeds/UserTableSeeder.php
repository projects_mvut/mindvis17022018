<?php
use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                'username'   => 'bharat',
                'email'      => 'bharatbgarg4@gmail.com',
                'password'   => bcrypt('qwertyuiop'),
                'first_name' => 'Bharat',
                'last_name'  => 'Garg',
                'is_admin'   => true,
                'is_teacher' => true, 
                'is_confirmed' => true, 
                'slug'   => 'bharat',
            ]);
        DB::table('users')->insert([
                'username'   => 'himanshu',
                'slug'   => 'himanshu',
                'email'      => 'himanshu.vasistha@gmail.com',
                'password'   => bcrypt('qwertyuiop'),
                'first_name' => 'Himanshu',
                'last_name'  => 'Vasistha',
                'is_admin'   => true,
                'is_teacher' => true,
                'is_confirmed' => true,   
            ]);
        DB::table('users')->insert([
                'username'   => 'doctorv',
                'slug'   => 'doctorv',
                'email'      => 'darthv@deathstar.com',
                'password'   => bcrypt('thedarkside'),
                'first_name' => 'Darth',
                'last_name'  => 'Vader',
                'is_teacher' => true,
                'is_confirmed' => true, 
            ]);
 
        DB::table('users')->insert([
                'username'   => 'goodsidesoldier',
                'slug'   => 'goodsidesoldier',
                'email'      => 'lightwalker@rebels.com',
                'password'   => bcrypt('hesnotmydad'),
                'first_name' => 'Luke',
                'last_name'  => 'Skywalker',
                'is_confirmed' => true, 
            ]);
 
        DB::table('users')->insert([
                'username'   => 'greendemon',
                'slug'   => 'greendemon',
                'email'      => 'dancingsmallman@rebels.com',
                'password'   => bcrypt('yodaIam'),
                'first_name' => 'Yoda',
                'last_name'  => 'Unknown',
                'is_confirmed' => true, 
            ]);
        DB::table('users')->insert([
                'username'   => 'teacher',
                'slug'   => 'teacher',
                'email'      => 'teacher@t.com',
                'password'   => bcrypt('teacher'),
                'first_name' => 'Teacher',
                'last_name'  => 'Test',
                'is_confirmed' => true, 
                'is_teacher'    => true,
            ]);
    }
 
}