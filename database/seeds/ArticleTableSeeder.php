<?php
use Illuminate\Database\Seeder;
class ArticleTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'1',
                'slug'=>'lorem-ipsum',
            ]);
        DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'1',
                'slug'=>'lorem-ipsum2',
            ]);
        DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'1',
                'slug'=>'lorem-ipsum3',
            ]);
        DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'2',
                'slug'=>'lorem-ipsum6',
            ]);
        DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'1',
                'is_published'=>false,
                'slug'=>'lorem-ipsum4',
            ]);
                DB::table('articles')->insert([
                'title'=>'Lorem ipsum dolor.',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi cumque delectus ratione nam molestias obcaecati ipsam, omnis! Consectetur, odio adipisci recusandae libero reprehenderit vitae voluptatibus dolores porro, odit aperiam dolorum.',
                'user_id'=>'1',
                'is_published'=>false,
                'slug'=>'lorem-ipsum5',
            ]);

    }
 
}