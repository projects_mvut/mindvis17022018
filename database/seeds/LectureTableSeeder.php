<?php
use Illuminate\Database\Seeder;
class LectureTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lectures')->insert([
                'title'=>'Nouns',
                'content'=>'Nouns Course',
                'course_id'=>'1',
                'slug'=>'mechanical',
            ]);
        DB::table('lectures')->insert([
                'title'=>'Verb',
                'content'=>'Verb Course',
                'course_id'=>'1',
                'slug'=>'mechanica',
            ]);
        DB::table('lectures')->insert([
                'title'=>'Pronoun',
                'content'=>'Pronoun Course',
                'course_id'=>'1',
                'slug'=>'mechacal',
            ]);
        DB::table('lectures')->insert([
                'title'=>'Adverb',
                'content'=>'Adverb Course',
                'course_id'=>'1',
                'slug'=>'mechacgal',
            ]);
    }
 
}