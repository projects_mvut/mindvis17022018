<?php
use Illuminate\Database\Seeder;
class CourseTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
                'title'=>'English',
                'content'=>'English Course',
                'is_published'=>false,
                'slug'=>'engish',
                'category_id'=>2,
                'price'=>'200',
            ]);
        DB::table('courses')->insert([
                'title'=>'Mechanical',
                'content'=>'Mechanical Course',
                'slug'=>'mechanical',
                'url'=>'http://mindvis.in/images/mechanical.jpg',
                'category_id'=>1,
                'price'=>'400',
            ]);
        DB::table('courses')->insert([
                'title'=>'Electrical',
                'content'=>'Electrical Course',
                'slug'=>'electrical',
                'url'=>'http://mindvis.in/images/electrical.jpg',
                'category_id'=>1,
                'price'=>'100',
            ]);
        DB::table('courses')->insert([
                'title'=>'Production',
                'content'=>'Production Course',
                'is_published'=>false,
                'slug'=>'production',
                'category_id'=>1,
            ]);
        DB::table('courses')->insert([
                'title'=>'Aeronautics',
                'content'=>'Aeronautics Course',
                'is_published'=>false,
                'slug'=>'aeronatical',
                'category_id'=>1,
            ]);
        DB::table('courses')->insert([
                'title'=>'Computer Science',
                'content'=>'Computer Science Course',
                'slug'=>'cse',
                'url'=>'http://mindvis.in/images/cse.jpg',
                'category_id'=>1,
                'price'=>'350',
            ]);
        DB::table('courses')->insert([
                'title'=>'Electronics',
                'content'=>'Electronics Course',
                'slug'=>'ece',
                'url'=>'http://mindvis.in/images/electronics.jpg',
                'category_id'=>1,
            ]);
        DB::table('courses')->insert([
                'title'=>'Metallurgy',
                'content'=>'Metallurgy Course',
                'is_published'=>false,
                'slug'=>'meta',
                'category_id'=>1,
            ]);
        DB::table('courses')->insert([
                'title'=>'Civil',
                'content'=>'Civil Course',
                'slug'=>'civil',
                'url'=>'http://mindvis.in/images/civil.jpg',
                'category_id'=>1,
            ]);
        DB::table('courses')->insert([
                'title'=>'Physics',
                'content'=>'Physics Course',
                'slug'=>'pyhysics',
                'url'=>'http://mindvis.in/images/civil.jpg',
                'category_id'=>3,
            ]);
    }
 
}