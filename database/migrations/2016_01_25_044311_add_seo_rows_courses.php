<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoRowsCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('keywords');
            $table->string('description');
            $table->string('seo_title');
        });
        Schema::table('articles', function (Blueprint $table) {
            $table->string('keywords');
            $table->string('description');
            $table->string('seo_title');
        });
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->timestamps();
            $table->string('title');
            $table->string('keywords');
            $table->string('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('keywords');
            $table->dropColumn('description');
            $table->dropColumn('seo_title');
        });
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('keywords');
            $table->dropColumn('description');
            $table->dropColumn('seo_title');
        });
        Schema::drop('settings');
    }
}
