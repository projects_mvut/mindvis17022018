<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('slug',80)->unique()->index();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->integer('blog_id')->unsigned()->nullable()->index();
            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');

        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign('articles_blog_id_foreign');
            $table->dropColumn('blog_id');
        });
    }
}
