<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDemoDetatilsCourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('demo_video');
            $table->string('no_of_video')->default(0);
            $table->string('no_of_quiz')->default(0);
            $table->string('no_of_live_session')->default(0);
            $table->string('dvds')->default("Yes");
            $table->string('notes')->default("Yes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('demo_video');
            $table->dropColumn('no_of_video');
            $table->dropColumn('no_of_quiz');
            $table->dropColumn('no_of_live_session');
            $table->dropColumn('dvds');
            $table->dropColumn('notes');
        });
    }
}
